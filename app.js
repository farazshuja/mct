var MCTApp = angular.module('mctApp', [ 'ngRoute', 'mctAppServices', 'ngSanitize', 'ngAnimate', 'MCTRoutes', 'ui.bootstrap' ]).config(
    ['$routeProvider', '$httpProvider', mctRouter ]);

function mctRouter($routeProvider, $httpProvider) {
    var httpInterceptor = ['$q', '$window', function($q, $window) {
        return {
            'request': function(config) {
                _MCTUtil.ResetSessionCookie();
                return config;
            }
        };
    }];
    $httpProvider.interceptors.push(httpInterceptor);

}

$(document).ready(function() {
    $('.nav-tabs .dropdown').hover(function() {
        $('select:focus').blur();
        var $dd = $(this).children('.dropdown-menu');
        if($dd.children('li').size() == 0) {
            return; //do nothing
        }
        $dd.show();
    }, function() {
        $(this).children('.dropdown-menu').hide();
    });
});

/*
 Patching jquery.growl.js
 format
 {success: 'success message' } or {error: 'error message'}
 */
$.growl.message = function(res) {
    var isSuccess = res.success ? true : false;
    var options = {
        message: isSuccess ? res.success : res.error
    };
    if(isSuccess) {
        this.notice(options);
    }
    else {
        this.error(options);
    }
};

function getRoute(templateUrl, controller, resolve, controllerAs) {
    var url = templateUrl;
    if(templateUrl.indexOf(':') != -1) {
        templateUrl = templateUrl.split(':')[0];
        url = function(param) {
            return templateUrl + param.id;
        }
    }
    var resolveObj = {};
    if(resolve) {
        angular.forEach(resolve, function(v,k){
            if(k == 'Scripts') { //its an object,
                resolveObj[k] = ['JSLoader', function(JSLoader) {
                    return JSLoader.load(v);
                }];
            }
            else {
                var o = ['$resource', '$route', function($resource, $route) {
                    var id = $route.current.params.id;
                    var r = v.indexOf(':') == -1 ? $resource(v).get().$promise : $resource(v).get({id : id}).$promise;
                    return r;
                }];
                resolveObj[k] = v ? o : function() {return null;};
            }
        });
    }

    var obj = {
        templateUrl: url,
        controller: controller,
        resolve: resolveObj
    };

    if(controllerAs) {
        obj.controllerAs = controllerAs;
    }
    return obj;
}

/**
 * Retrieve nested item from object/array
 * @param {Object|Array} obj
 * @param {String} path dot separated
 * @param {*} def default value ( if result undefined )
 * @returns {*}
 */
Object.resolve = function(path, obj, safe) {
    var i, len;
    path = path.split('.');
    len = path.length;
    for(i = 0; i < len; i++){
        if(!obj || typeof obj !== 'object')
            return def;

        obj = obj[path[i]];
    }

    if(obj === undefined)
        return def;

    return obj;
};

var _MCTUtil = (function() {
    return {
        ResetSessionCookie: function() {
            Cookies.set('MCTSessionCookie', 'true', { path: '/', expires: 1800 }); // Expires in 30 mins
        }
    }
})();

(function() {
    //hook on all type of ajax request initiated by jQuery
    $(document).ajaxSend(function() {
        _MCTUtil.ResetSessionCookie();
    });
    //initial an interval to redirect user to logout page as soon Session Cookie expired
    setInterval(function() {
        if(!Cookies.get('MCTSessionCookie')) {
            var href = $('.logout-link').attr('href');
            if(href)
                window.location.href = href;
        }
    },1000);    //call after each second
})();