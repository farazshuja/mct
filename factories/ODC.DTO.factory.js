//Out Data Connector Data Object
(function() {
    angular.module('mctApp').factory('OutDataConnectorDTO', OutDataConnectorDTO);
    OutDataConnectorDTO.$inject = ['$location', '$http', '$filter'];

    function OutDataConnectorDTO($location, $http, $filter) {
        var  dataSenders, outMessageTypeList, sendingMedium, isSendingMediumXML, DSValid;
        var data = {
            "id": null,
            "name": "",
            "description": "",
            "state": "ENABLED",
            "enabled": true,
            "dataSenders": [],
            "messageTypeMappings": []
        };
        var messageTypeMapping = {
            "id": null,
            "messageType": null,
            "enabled": true,
            "odcMessageConfigDto": {
                "id": null,
                "constructOutMessageInDataSender": false,
                "resendFailedMessagesByDailyJob": true,
                "skipOMH": false,
                "outGoingMessageConfigDto": {
                    "zeroResultAction": "LOG_TO_TABLE",
                    "variableMappings": [],
                    "tagRemovalConfigs": []
                }
            }
        };
        var variableMapping= {
            "transformation": "NO_TRANSFORMATION"
        };

        /* -------------------- Private Methods ---------------------- */
        /* --------------------------------------------------------- */
        //add attribute selectable, to decide what common type of DataSenders can be selected, once user has selected some DS
        function resetSelectedDS() {
            angular.forEach(this.dataSenders, function(v,k) {
                v.selectable = true;
            });
        };

        //allow DS checkboxes selectable only if the medium is XML_API
        function handleDSSelection(chk) {
            var that = this;
            that.sendingMedium = chk.sendingMedium;
            that.isSendingMediumXML = that.sendingMedium == 'XML_API';
            angular.forEach(that.dataSenders, function(v,k) {
                v.selectable = v.sendingMedium == that.sendingMedium;
            });
        };

        // initilaize OutMessageTypeList with correct values
        function initializeOutMessageTypeList(_saveData) {
            var that = this;

            for(var i=0;i<that.outMessageTypeList.length;i++) {
                var obj = that.outMessageTypeList[i];
                obj.selectable=true;

                var mTM = angular.copy(messageTypeMapping);
                mTM.messageType = angular.copy(obj);

                that.data.messageTypeMappings.push(mTM);

                if(_saveData) {
                    var found = $filter('searchInObject')(_saveData.messageTypeMappings, 'messageType.id', obj.id);
                    if (found) {
                        obj.checked = true;
                        obj.selectable = found.enabled;
                        obj.enabled = found.enabled;
                        //reset "zeroResultAction" to "LOG_TO_TABLE" in case it was not save
                        if(found.odcMessageConfigDto.outGoingMessageConfigDto) {
                            if(!found.odcMessageConfigDto.outGoingMessageConfigDto.zeroResultAction) {
                                found.odcMessageConfigDto.outGoingMessageConfigDto.zeroResultAction = "LOG_TO_TABLE";
                            }
                        }
                        else {
                            found.odcMessageConfigDto.outGoingMessageConfigDto = angular.copy(messageTypeMapping.odcMessageConfigDto.outGoingMessageConfigDto)
                        }

                        found.checked = true;
                        that.data.messageTypeMappings[i] = angular.copy(found);
                    }
                }
            }
            if(_saveData) {
                this.data.id = _saveData.id;
                this.data.name = _saveData.name;
                this.data.description = _saveData.description;
            }
        }

        function validateCheckboxes(msgTypeList) {
            var isValid = false;
            for (var i=0; i<msgTypeList.length; i++) {
                var o = msgTypeList[i];
                if (o.checked) {
                    isValid = true;
                }
            }
            return isValid;
        }

        /* -------------------- Public Methods ---------------------- */
        /* --------------------------------------------------------- */

        //initialize Data with default values, and prepare the data object of DTO
        var initData = function(_dataSenders, _outMessageTypeList, _savedData) {
            this.dataSenders = _dataSenders;
            this.outMessageTypeList = _outMessageTypeList;
            this.data = angular.copy(data);

            if(_savedData) {    //if modify page then load data accordingly
                var _lastSaveDS;
                for(var i=0;i<this.dataSenders.length;i++) {
                    var obj = this.dataSenders[i];
                    var found = $filter('searchInObject')(_savedData.dataSenders, 'id', obj.id);
                    if(found) {
                        obj.checked = true;
                        _lastSaveDS = obj;
                    }
                }

                handleDSSelection.call(this, _lastSaveDS);   //handle DS checkboxes based on first checkbox
                this.DSValid = true;    // as on modify page at least on DS is selected on load
                this.OMTValid = true;   // as on modify page at least one Message Type is selected on load
            }
            else {
                resetSelectedDS.call(this);
            }
            initializeOutMessageTypeList.call(this, _savedData);

        };

        //on checking/unchecking DS checkboxes
        //enable/disabel checkboxes base on similar medium type
        var dataSenderSelectionChanged = function(chk, dto) {
            if(chk.checked){
                handleDSSelection.call(this, chk);
                this.DSValid = true;
            }
            else {
                var checkedCount = 0;
                for(var i=0;i<this.dataSenders.length;i++) {
                    var o = this.dataSenders[i];
                    if(o.checked) {
                        handleDSSelection.call(this, o);
                        checkedCount++;
                        break;
                    }
                }
                if(checkedCount == 0) {
                    resetSelectedDS.call(this);
                }
                this.DSValid = checkedCount > 0;
            }

        };

        var outMsgTypeSelectionChanged = function(chk, i) {
            if (chk === false) {
                this.OMTValid = true;
            } else {
                this.OMTValid = validateCheckboxes(this.outMessageTypeList);
                this.data.messageTypeMappings[i].checked = chk.checked;
            }
        };

        var addNewMapping = function(messageTypeIndex) {
            this.data.messageTypeMappings[messageTypeIndex].odcMessageConfigDto.outGoingMessageConfigDto.variableMappings.push({
                'transformation': 'NO_TRANSFORMATION'
            });
        };

        var deleteMappingOEM = function(messageTypeIndex, row) {

            var ifBuilder = this.data.messageTypeMappings[messageTypeIndex].odcMessageConfigDto.outGoingMessageConfigDto.variableMappings;
            for(var i=0;i<ifBuilder.length; i++) {
                var item = ifBuilder[i];
                if(item.$$hashKey == row.$$hashKey) {
                    ifBuilder.splice(i, 1);
                    break;
                }
            }
        };

        var addXPath = function (messageTypeIndex) {
            this.data.messageTypeMappings[messageTypeIndex].odcMessageConfigDto.outGoingMessageConfigDto
                .tagRemovalConfigs.push({xpath: ''});
        };

        var deleteXPath = function(messageTypeIndex, index) {
            console.log(index);
            this.data.messageTypeMappings[messageTypeIndex].odcMessageConfigDto.outGoingMessageConfigDto
                .tagRemovalConfigs.splice(index, 1);
        };

        var hasAtLeastOneAttribute = function (messageTypeIndex) {
            var configs = this.data.messageTypeMappings[messageTypeIndex].odcMessageConfigDto.outGoingMessageConfigDto.tagRemovalConfigs;
            if (configs) {
                for (var i = 0; i < configs.length; i++) {
                    if (configs[i].attribute) {
                        return true;
                    }
                }
            }
            return false;
        };

        var save = function(formSelector, url, listUrl) {
            var dataToPost = $("#createForm").serialize();
            var dataSendersId = [];
            angular.forEach(this.dataSenders, function(val, key) {
                if(val.checked)
                    dataSendersId.push(val.id);
            });
            dataToPost += '&dataSenderIds=[' + dataSendersId.join(',') + ']';
            $http({
                method: 'POST',
                url: url,
                data: dataToPost,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
            .success(function(data) {
                $location.path(listUrl);
                $.growl.message(data);
            })
            .error(function(data) {
                $location.path(listUrl);
                $.growl.message(data);
            });
        };


        return {
            data: data,
            dataSenders: dataSenders,
            outMessageTypeList: outMessageTypeList,
            messageTypeIfBuilder: [],
            sendingMedium: sendingMedium,  //sending medium of the selected DS
            isSendingMediumXML: false,
            DSValid: false,   //all null values will be initialized after initData called
            OMTValid: false,
            initData: initData,
            dataSenderSelectionChanged: dataSenderSelectionChanged,
            outMsgTypeSelectionChanged: outMsgTypeSelectionChanged,
            addNewMapping: addNewMapping,
            deleteMappingOEM: deleteMappingOEM,
            addXPath: addXPath,
            deleteXPath: deleteXPath,
            hasAtLeastOneAttribute: hasAtLeastOneAttribute,
            save: save
        }
    }
})();