(function() {
    angular.module('mctApp').factory('DataSenderDTO', DataSenderDTO);
    DataSenderDTO.$inject = ['$http', '$location', 'MVMConfig'];

    function DataSenderDTO($http, $location, MVMConfig) {
        var addNewMapping = function() {
            this.data.dataSenderOutGoingMessageConfigDto.outGoingMessageConfigDto.variableMappings.push({
                'transformation': 'NO_TRANSFORMATION'
            });
        };

        var deleteMappingOEM = function(row) {
            var ifBuilder = this.data.dataSenderOutGoingMessageConfigDto.outGoingMessageConfigDto.variableMappings;
            for(var i=0;i<ifBuilder.length; i++) {
                var item = ifBuilder[i];
                if(item.$$hashKey == row.$$hashKey) {
                    ifBuilder.splice(i, 1);
                    break;
                }
            }
        };

        var initData = function(mvmTypes) {
            this.data = {
                type: "CHAINED_TRIGGERED",
                destinationType: "CRM",
                sendingMedium: "XML_API",
                mvmTypes: mvmTypes,
                dataSenderApiPropertiesDto: {
                    keystoreType: "NONE",
                    responseTime: 0,
                    maxRetries: 0,
                    firstDelayTime: 0,
                    growthFactor: 0.000,
                    encryptionProtocol: "NONE",
                    headerKeys: [],
                    headerValues: [],
                    httpMethod: "POST"
                },
                enabled: true,
                //custom properties require on view
                fileUploaded: false,
                checkFile: true,
                requestHeader: [{}],
                mvmConfig: angular.copy(MVMConfig.mvmConfig),
                dataSenderOutGoingMessageConfigDto: {
                    "outGoingMessageConfigDto": {
                        "zeroResultAction": "LOG_TO_TABLE",
                        "variableMappings": [],
                        "tagRemovalConfigs": []
                    }
                }
            };

        };

        var addRequestHeader = function() {
            this.data.requestHeader.push({});
        };

        var deleteRequestHeader = function(index) {
            this.data.requestHeader.splice(index, 1);
        };

        var addMVMProperty = function() {
            MVMConfig.addMVMProperty(this.data.mvmConfig);
        };

        var removeMVMProperty = function(index) {
            MVMConfig.removeMVMProperty(index, this.data.mvmConfig);
        };

        var openStartDate = function(mvmProperties) {
            MVMConfig.openStartDate(mvmProperties);
        };

        var openEndDate = function(mvmProperties) {
            MVMConfig.openEndDate(mvmProperties);
        };

        var changeMinEndDate = function (mvmProperties) {
            MVMConfig.changeMinEndDate(mvmProperties);
        };

        var gatherStatistics = function (mvmProperties) {
            MVMConfig.gatherStatistics(mvmProperties, this.data.mvmConfig.id, $http);
        };

        var isFailedValue = function (value) {
            return MVMConfig.isFailedValue(value);
        };

        var addXPath = function() {
            this.data.dataSenderOutGoingMessageConfigDto.outGoingMessageConfigDto.tagRemovalConfigs.push({xpath: ''});
        };

        var deleteXPath = function(index) {
            this.data.dataSenderOutGoingMessageConfigDto.outGoingMessageConfigDto.tagRemovalConfigs.splice(index, 1);
        };

        var hasAtLeastOneAttribute = function () {
            var configs = this.data.dataSenderOutGoingMessageConfigDto.outGoingMessageConfigDto.tagRemovalConfigs;
            if (configs) {
                for (var i = 0; i < configs.length; i++) {
                    if (configs[i].attribute) {
                        return true;
                    }
                }
            }
            return false;
        };

        var loadData = function(data, mvmTypes) {
            this.initData(mvmTypes);

            MVMConfig.parseCoefficientToDays(data.mvmConfig);
            this.data = $.extend(true, this.data, data);

            //handle enable/disable of configuration dropdown
            MVMConfig.handleConfigurationDropDownState(this.data.mvmConfig);

            this.data.requestHeader = [];
            var dto = this.data.dataSenderApiPropertiesDto;
            for(var i=0;i<dto.headerKeys.length;i++) {
                this.data.requestHeader.push({
                    key: dto.headerKeys[i],
                    value: dto.headerValues[i]
                })
            }
            //on load of data check if file was previously uploaded then make file upload field required
            this.data.fileUploaded = this.data.dataSenderApiPropertiesDto.keystoreType != 'NONE';
            this.data.checkFile = !this.data.fileUploaded;
        };

        var save = function(formSelector, modifyUrl) {
            var formData = new FormData($(formSelector)[0]);
            $http.post(modifyUrl, formData, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })
                .success(function(data, status, headers, config) {
                    $location.path("/bidirectional/dataSender/list");
                    $.growl.message(data);
                })
                .error(function(data, status, headers, config) {
                    $location.path("/bidirectional/dataSender/list");
                    $.growl.message(data);
                });
        };

        var composeDayCoefficients = function (mvmProperties) {
            return MVMConfig.composeDayCoefficients(mvmProperties);
        };

        return {
            data: {},
            addNewMapping: addNewMapping,
            deleteMappingOEM: deleteMappingOEM,
            initData: initData,
            addRequestHeader:addRequestHeader,
            deleteRequestHeader: deleteRequestHeader,
            //will add a property and will delete that mvm type from dropdown, so that next time it cannot be selected
            addMVMProperty: addMVMProperty,
            removeMVMProperty: removeMVMProperty,
            addXPath: addXPath,
            deleteXPath: deleteXPath,
            hasAtLeastOneAttribute: hasAtLeastOneAttribute,
            loadData: loadData,
            save: save,
            composeDayCoefficients: composeDayCoefficients,
            openStartDate: openStartDate,
            openEndDate: openEndDate,
            changeMinEndDate: changeMinEndDate,
            gatherStatistics: gatherStatistics,
            isFailedValue: isFailedValue
        };
    }
})();
