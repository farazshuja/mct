//User Data Object
(function() {
    angular.module('mctApp').factory('UserDTO', UserDTO);
    UserDTO.$inject = ['HTTPWrapper'];

    function UserDTO(HTTPWrapper) {


        /* -------------------- Public Methods ---------------------- */
        /* --------------------------------------------------------- */

        //initialize Data with default values, and prepare the data object of DTO
        var initData = function(grpList, _savedData) {
            this.data = {
                groupIds: []
            };

            if (_savedData) {
                this.data = angular.extend(this.data, _savedData);
                this.data.groupIds = _savedData.userGroups.map(function(a) { return a.groupId; });
            }
        };

        var save = function(formSelector, url, listUrl) {
            var dataToPost = $(formSelector).serialize();
            dataToPost += '&groupIds=' + this.data.groupIds;
            HTTPWrapper.postForm(url, dataToPost, listUrl);
        };

        return {
            data: {},
            initData: initData,
            save: save
        }
    }
})();