//Modal Factory to show/hide modals on MCT
// Its based on jquery and bootstrap's modal plugin
(function() {
    angular.module('mctApp').factory('Modal', Modal);
    Modal.$inject = ['HTTPWrapper'];

    function Modal(HTTPWrapper) {

         var showModal = function(sel, callback) {
            var $visibleModals = $('.modal:visible');
            $(sel).off('shown.bs.modal');
            if($visibleModals.size() > 0) {
                //$('.modal:visible').off('hidden.bs.modal');
                $('.modal:visible').one('hidden.bs.modal', function () {
                    $(sel).modal('show');
                });
                $('.modal:visible').modal('hide');
            }
            else {
                $(sel).modal('show');
            }
            if(callback) {
                //$(sel).off('shown.bs.modal');
                $(sel).one('shown.bs.modal', function(e) {
                    callback();
                });
            }
        };

        var hideModal = function() {
            $('.modal:visible').modal('hide');
        };

        var showIndicator = function() {
            $('body').append('<div class="ajax-indicator-overlay"></div><div class="ajax-indicator"></div>');
            $('.ajax-indicator-overlay').css('opacity',.25);
        };

        var hideIndicator = function() {
            $('.ajax-indicator, .ajax-indicator-overlay').remove();
        };


        return {
            showModal: showModal,
            hideModal: hideModal,
            showIndicator: showIndicator,
            hideIndicator: hideIndicator
        }
    };
})();