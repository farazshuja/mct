/**
 * @class GridView service, which will return object to dealer with Grid View
 * @param {String} url
 * @param {Object} params
 * { page: 1, rows: 10, sidx: 'id', sord: 'asc' }
 *
 * @usage:
 * $scope.Grid = GridView.init('url/to/api/of/grid', [optional params]);
 *
 */
(function() {
    angular.module('mctApp').factory('GridView', GridView);
    GridView.$inject = ['$http'];

    function GridView($http) {
        var _grids = {};

        //default params
        var defaultParams = {
            page: 1,
            rows: 10,
            sidx: 'id',
            sord: 'asc'
        };

        /**
         * @constructor
         */
        function Grid(url, params) {
            var that = this;

            that._url = url;
            if(params)
                that.params = angular.extend({}, defaultParams, params);  //params that will set while calling the url
            else {
                that.params = angular.copy(defaultParams);
            }
            that.isGridLoading = false;     //to check whether some request is in progress
            that.data = null;   //after successful API call data will be filled by row data returned by server
            that.hasNextPages = false;  //to check whether there are some next pages
            that.hasPrevPages = false;  //to check whether there are some prev pages

            that.refresh();
        }

        /**
         * refresh the Grid based on current params or passed one
         * use this method in case you want to refresh the grid after operations like Enable/Disable row
         * @param params
         * @returns $promise
         *
         */
        Grid.prototype.refresh = function(params) {
            var that = this;
            if(that.isGridLoading)
                return; //do nothing if Grid is already loading or in progress

            that.params = angular.extend(that.params, params);
            that.isGridLoading = true;
            return $http({
                method: 'GET',
                url: that._url,
                params: that.params
            }).success(function(data) {
                that.data = data;
                that.isGridLoading = false;
            });
        };

        /**
         * sort the grid and reset the page number to one
         * @param {String} name: the attribute on which the Grid must be sorted like id, name, date etc
         * @returns see refresh() method
         */
        Grid.prototype.sort = function(name) {
            var that = this;
            var sordOrder = that.params.sord == 'asc' ? 'desc' : 'asc';
            that.params = angular.extend(that.params, {sidx: name, sord: sordOrder, page: 1});
            return that.refresh();
        };

        /**
         * set the correct class to show the up/down icon of sort
         * @param name
         * @returns {String} class name
         */
        Grid.prototype.setSortClass = function(name) {
            var that = this;
            return that.params.sidx == name ? that.params.sord : '';

        };

        /**
         * Load the next page, if its available
         */
        Grid.prototype.nextPage = function() {
            var that = this;
            if(!that.isGridLoading && that.hasNextPages) {
                that.params.page++;
                that.refresh();
            }
        };

        /**
         * Load the previous page if its available
         */
        Grid.prototype.prevPage = function() {
            var that = this;
            if(!that.isGridLoading && that.hasPrevPages) {
                that.params.page--;
                that.refresh();
            }
        };


        /**
         * show the pager title text like Showing 1 - 10 of 20
         * @returns {string}
         */
        Grid.prototype.pagerText = function() {
            var that = this;
            var label = 'Showing 1 - 1 of 1';
            if(that.data) {
                var start = ((that.data.page - 1) * that.params.rows) + 1;
                var end = that.data.page * that.params.rows;
                var total = parseInt(that.data.records);
                end = end > total ? total : end;
                label = isNaN(total) || total == 0 ? null : 'Showing ' + start + ' - ' + end + ' of ' + total;
                that.hasNextPages = end < total ? true : false;
                that.hasPrevPages = start > 1 ? true : false;
            }

            return label;

        };

        /**
         * a special method to handle enable/disable of delete button
         * @param col: column of the row object like 'id', 'email' etc
         * @returns {boolean}
         */
        Grid.prototype.isAnyCheckBoxChecked = function(col) {
            var that = this;
            var list = [];
            if(that.data && that.data.rows){
                for (var i = 0; i < that.data.rows.length; i++) {
                    if (that.data.rows[i].checked) {
                        var l = that.data.rows[i];
                        list.push(l[col]);
                    }
                }
            }
            that.listStr = list.join(', ');
            return list.length != 0;
        }

        /**
         * Post the form related to Grid on same page, i.e. for operations like enable/disable/delete
         * @param url
         * @param data
         * @param $scope    //$scope is require to hide the modal
         */
        Grid.prototype.postForm = function(url, data, $scope) {
            var that = this;
            $http({
                method: 'POST',
                url: url,
                data: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                $.growl.message(data);
                $scope.hideModal();
                that.refresh();
            })
            .error(function(data, status, headers, config){
                $.growl.message(data);
                $scope.hideModal();
                that.refresh();
            });
        }

        // Initialize the grid using url, params and return new Grid Object
        // return the cached object in case grid was visited already
        var init = function(url, params, msg) {
            if(typeof _grids[url] == 'undefined') {
                _grids[url] = new Grid(url, params);
                return _grids[url];
            }
            var grid = _grids[url];
            grid.refresh();
            return grid;
        };

        return {
            init: init
        }
    }

})();
