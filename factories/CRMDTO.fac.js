/**
 * CRMDTO factory will resolve three dependencies and will return the merged data, it will resolve these APIs
 * oems: metaData/OEM/getOEMsLite
 * data sender: bidirectional/dataSender/getDataSendersLite
 * crm: 'bidirectional/crmDsMapping/getCRM/:id  //in case of modify page
 */
(function() {
    angular.module('mctApp').factory('CRMDTO', CRMDTO);
    CRMDTO.$inject = ['$http', '$location', '$q', '$filter', 'HTTPWrapper'];

    function CRMDTO($http, $location, $q, $filter, HTTPWrapper) {

        //init Data with default values
        var initData = function(oemId, dataSenderLeadId) {
            return {
                id: null,
                oemId: oemId,
                CRMOem: '',
                crmName: '',
                crmCode: '',
                dataSenderLeadId: dataSenderLeadId,
                dataSenderLeadDispositionId: -1,
                dataSenderLargeFileId: -1,
                dataSenderEmailId: -1,
                dataSenderVehicleInventoryId: -1,
                leadDispositionDataProviderId : -1,
                contactEmail: '',
                login: '',
                password: '' 
            }
        };

        var loadAll = function(id) {
            var isUpdate = id ? true : false;
            //return a promise
            //promise resolve must
            var one = $http.get('metaData/OEM/getOEMsLite');
            var two = $http.get('bidirectional/dataSender/getDataSendersLite');
            var three = id ? $http.get('bidirectional/crmDsMapping/getCRM/' + id) : null;
            var four = $http.get('metaData/dataProviders/getDataPrvidersList');


            var save = function(formSelector, url, listUrl) {
                var names = '&dataSenderLeadName=' + $('#select-lead option:selected').text();
                names += '&dataSenderLeadDispositionName=' + $('#select-dispositions option:selected').text();
                names += '&dataSenderLargeFileName=' + $('#select-large-file option:selected').text();
                names += '&dataSenderEmailName=' + $('#select-email option:selected').text();
                names += '&leadDispositionDataProviderName=' + $('#select-ld-dp option:selected').text();

                var dataToPost = $(formSelector).serialize() + names;
                HTTPWrapper.postForm(url, dataToPost, listUrl);
            };

            return $q.all([one, two, three, four]).then(function(response) {
                var oemsLite = response[0].data.oemList.filter(function(o) {
                    if(isUpdate) {   //modify page
                        return true;
                    }
                    return !o.disabled;
                });

                var dataSendersNotEmailLite = response[1].data.dataSenders.filter(function(o) {
                    if(isUpdate) {   //modify page
                        return o.sendingMedium != "EMAIL" && o.destinationType == 'CRM';
                    }
                    return o.sendingMedium != "EMAIL" && o.enabled == true && o.destinationType == 'CRM';
                });

                //create special type which also have None value
                var dataSendersNotEmailLiteWithNone = angular.copy(dataSendersNotEmailLite);
                dataSendersNotEmailLiteWithNone.unshift({
                    destinationType: null,
                    enabled: true,
                    id: -1,
                    name: 'None',
                    sendingMedium: "EMAIL"
                });

                var dataSendersEmailLite = response[1].data.dataSenders.filter(function(o) {
                    if(isUpdate) {   //modify page
                        return o.sendingMedium == "EMAIL" && o.destinationType == 'CRM';
                    }
                    return o.sendingMedium == "EMAIL" && o.enabled == true && o.destinationType == 'CRM';
                });
                dataSendersEmailLite.unshift({
                    destinationType: null,
                    enabled: true,
                    id: -1,
                    name: 'None',
                    sendingMedium: "EMAIL"
                });

                var _data = initData(oemsLite[0].id, dataSendersNotEmailLite[0].id);

                if(isUpdate) {  //extend default data with saved data
                    _data = angular.extend(_data, response[2].data.crm);
                    var oem = $filter('searchInObject')(oemsLite, 'id', _data.oemId);
                    _data.CRMOem = oem ? oem.name : '';
                }
                
                var dataProvidersCRMIntegration = response[3].data.dataProviderList.filter(function(o) {
                    if(isUpdate) {   //modify page
                        return o.type == "CRM_INTEGRATION" &&  o.oems.map(function(a) {return a.id;}).indexOf(_data.oemId) >= 0;
                    }
                    return o.type == "CRM_INTEGRATION" && o.oems.map(function(a) {return a.id;}).indexOf(_data.oemId) >= 0 && o.enabled == true;
                });

                //create special type which also have None value
                var dataProvidersCRMIntegrationWithNone = angular.copy(dataProvidersCRMIntegration);
                dataProvidersCRMIntegrationWithNone.unshift({
                    enabled: true,
                    id: -1,
                    name: 'None',
                    type: "CRM_INTEGRATION"
                });

                // Data and APIs
                return {
                    isUpdate: isUpdate,
                    OEMsLite: oemsLite,
                    DataSendersNotEmailLite: dataSendersNotEmailLite,
                    DataSendersNotEmailLiteWithNone: dataSendersNotEmailLiteWithNone,
                    DataSendersEmailLite: dataSendersEmailLite,
                    DataProvidersForCRMIntegrationWithNone : dataProvidersCRMIntegrationWithNone,
                    data: _data,
                    save: save
                };
            });

        };

        return {
            loadAll: loadAll
        }
    };

})();
