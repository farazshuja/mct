//User Data Object
(function() {
    angular.module('mctApp').factory('OEMDTO', OEMDTO);
    OEMDTO.$inject = ['HTTPWrapper'];

    function OEMDTO(HTTPWrapper) {


        /* -------------------- Public Methods ---------------------- */
        /* --------------------------------------------------------- */

        //initialize Data with default values, and prepare the data object of DTO
        var initData = function(_savedData) {
            this.data = {
            };


            if(_savedData) {
                this.data = angular.extend(this.data, _savedData);
            }


        };


        var save = function(formSelector, url, listUrl) {
            var dataToPost = $(formSelector).serialize();
            HTTPWrapper.postForm(url, dataToPost, listUrl);
        };



        return {
            data: {},
            initData: initData,
            save: save
        }
    };
})();