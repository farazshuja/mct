//A wrapper around angular $http method, to handle form ajax requests for MCT
(function() {
    angular.module('mctApp').factory('HTTPWrapper', HTTPWrapper);
    HTTPWrapper.$inject = ['$location', '$http'];

    function HTTPWrapper($location, $http) {

        /* -------------------- Public Methods ---------------------- */
        /* --------------------------------------------------------- */


        // Method to post the form based on name attributes and show growl notification of success/failure
        var postForm = function(url, dataToPost, listUrl) {
             $http({
                 method: 'POST',
                 url: url,
                 data: dataToPost,
                 headers: {'Content-Type': 'application/x-www-form-urlencoded'}
             })
             .success(function(data) {
                $location.path(listUrl);
                $.growl.message(data);
             })
             .error(function(data) {
                $location.path(listUrl);
                $.growl.message(data);
             });
        };

        // A general ajax wrapper around $http to also show error growl
        var httpAjax = function(type, url, d, success, error) {
            $http({
                method: type,
                url: url,
                data: d,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
            .success(function(data, status, headers, config) {
                if(success){
                    success(data, status, headers, config);
                }
            })
            .error(function(data, status, headers, config){
                if(error){
                    error(data, status, headers, config);
                }
                $.growl.message(data);
            });
        }

        return {
            postForm: postForm,
            httpAjax: httpAjax
        }
    };
})();