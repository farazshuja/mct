MCTApp.factory('MVMConfig', function() {
    var defaultMVMProperty = {
        id: null,
        mvmPropertiesType: null,
        enabled: true,
        average: 0,
        failed: 0,
        positiveTolerance: 0,
        negativeTolerance: 0,
        startTime: 0,
        endTime: 1,
        period: 15,
        days: [1,1,1,1,1,1,1],   // Sun - Sat
        startDateOpened: false,
        startDate: null,
        endDateOpened: false,
        endDate: null,
        startDateOptions: {
            maxDate: new Date(2030, 12, 31),
            minDate: new Date(2010, 1, 1),
            startingDay: 1
        },
        endDateOptions: {
            maxDate: new Date(2030, 12, 31),
            minDate: new Date(2010, 1, 1),
            startingDay: 1
        },
        statistics: null,
        statisticsLoading: false
    };

    var mvmConfig = {
        id: null,
        volumeMonitorType: 'SENDER',
        mvmType: 'Peak',
        mvmTypeEnabled: true,
        mvmProperties: [],
        hours: [
            { key: '12am', val: 0 },
            { key: '1am', val: 1 },
            { key: '2am', val: 2 },
            { key: '3am', val: 3 },
            { key: '4am', val: 4 },
            { key: '5am', val: 5 },
            { key: '6am', val: 6 },
            { key: '7am', val: 7 },
            { key: '8am', val: 8 },
            { key: '9am', val: 9 },
            { key: '10am', val: 10 },
            { key: '11am', val: 11 },
            { key: '12pm', val: 12 },
            { key: '1pm', val: 13 },
            { key: '2pm', val: 14 },
            { key: '3pm', val: 15 },
            { key: '4pm', val: 16 },
            { key: '5pm', val: 17 },
            { key: '6pm', val: 18 },
            { key: '7pm', val: 19 },
            { key: '8pm', val: 20 },
            { key: '9pm', val: 21 },
            { key: '10pm', val: 22 },
            { key: '11pm', val: 23 }
        ],
        periods: [
            { key: '15 mins', val: 15 },
            { key: '30 mins', val: 30 },
            { key: '1 hr', val: 60 },
            { key: '2 hrs',	val: 120 },
            { key: '3 hrs',	val: 180 },
            { key: '4 hrs', val: 240 }
        ],
        constDays: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        defaultDateFormat: 'MM/dd/yyyy'
    };

    var parseCoefficientToDays = function(mvmConfig) {
        if(mvmConfig) {
            for(var i=0;i<mvmConfig.mvmProperties.length;i++) {
                var obj = mvmConfig.mvmProperties[i];
                if(obj.daysCoefficients) {
                    var data = obj.daysCoefficients.split('|');
                    obj.days = data.map(Number);
                }
                else {
                    if(!obj.enabled) {
                        var property = angular.copy(defaultMVMProperty);
                        property.id = obj.id;
                        property.mvmPropertiesType = obj.mvmPropertiesType;
                        property.priority = obj.priority;
                        property.enabled = false;
                        angular.extend(obj, property);
                    }
                }
            }
        }
    };

    var addMVMProperty = function(mvmConfig) {
        var property = angular.copy(defaultMVMProperty);
        property.mvmPropertiesType = mvmConfig.mvmType;
        mvmConfig.mvmProperties.push(property);
        //select other value in mvmType and disable it
        mvmConfig.mvmType = mvmConfig.mvmType == 'Peak' ? 'Off-peak' : 'Peak';
        mvmConfig.mvmTypeEnabled = false;
    };

    var removeMVMProperty = function(index, mvmConfig) {
        mvmConfig.mvmProperties.splice(index, 1);
        if(mvmConfig.mvmProperties.length == 1) {
            var type = mvmConfig.mvmProperties[0].mvmPropertiesType;
            mvmConfig.mvmType = type == 'Peak' ? 'Off-peak' : 'Peak';
            mvmConfig.mvmTypeEnabled = false;
        }
        else {
            mvmConfig.mvmTypeEnabled = true;
        }

    };

    var composeDayCoefficients =  function(mvmProperties) {
        if (mvmProperties.days) {
            return mvmProperties.days.join('|');
        }
    };

    //handle enable/disable of configuration dropdown
    var handleConfigurationDropDownState = function(mvmConfig) {
        if(mvmConfig.mvmProperties.length == 2) {
            mvmConfig.mvmTypeEnabled = false;
        }
        else if(mvmConfig.mvmProperties.length == 1) {
            var type = mvmConfig.mvmProperties[0].mvmPropertiesType;
            mvmConfig.mvmType = type == 'Peak' ? 'Off-peak' : 'Peak';
            mvmConfig.mvmTypeEnabled = false;
        }
    };

    var openStartDate = function (mvmProperties) {
        mvmProperties.startDateOpened = true;
    };

    var openEndDate = function (mvmProperties) {
        mvmProperties.endDateOpened = true;
    };

    var changeMinEndDate = function (mvmProperties) {
        var startDate = new Date(mvmProperties.startDate.getTime());
        var endDate = mvmProperties.endDate;
        if (endDate && startDate.getTime() > endDate.getTime()) {
            mvmProperties.endDate = null;
        }
        startDate.setDate(startDate.getDate() + 1);
        mvmProperties.endDateOptions.minDate = startDate;
    };

    var formatDate = function (date) {
        return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
    };

    var isFailedValue = function (value) {
        return value.indexOf('Low') > -1 || value.indexOf('High') > -1 || value.indexOf('Fail') > -1;
    };

    var gatherStatistics = function (mvmProperties, configId, $http) {
        mvmProperties.statisticsLoading = true;
        mvmProperties.daysCoefficients = this.composeDayCoefficients(mvmProperties);
        $http.post('metaData/MVM/statistics'
                + '?mvmConfigId=' + configId
                + '&startDate=' + this.formatDate(mvmProperties.startDate)
                + '&endDate=' + this.formatDate(mvmProperties.endDate),
            JSON.stringify(mvmProperties))
            .success(function (data) {
                mvmProperties.statistics = data;
                mvmProperties.statisticsLoading = false;
            });
    };

    return {
        mvmConfig: mvmConfig,
        addMVMProperty: addMVMProperty,
        removeMVMProperty: removeMVMProperty,
        composeDayCoefficients: composeDayCoefficients,
        parseCoefficientToDays: parseCoefficientToDays,
        handleConfigurationDropDownState: handleConfigurationDropDownState,
        openStartDate: openStartDate,
        openEndDate: openEndDate,
        changeMinEndDate: changeMinEndDate,
        formatDate: formatDate,
        isFailedValue: isFailedValue,
        gatherStatistics: gatherStatistics
    }
});