MCTApp.factory('GeoUpdateFactory', function() {

    var updators = [ {
        title : 'Update lat/long for all leads where lat/long is not set',
        url : 'leadsLatLongNotSet'
    }, {
        title : 'Update lat/long for all leads where lat/long is set',
        url : 'leadsLatLongSet'
    }, {
        title : 'Update lat/long for all sales where lat/long is not set',
        url : 'salesLatLongNotSet'
    }, {
        title : 'Update lat/long for all sales where lat/long is set',
        url : 'salesLatLongSet'
    } ];

    var dateProperties = {
        defaultDateFormat : 'MM/dd/yyyy',

        startDateOpened : false,
        startDate : null,

        endDateOpened : false,
        endDate : null,

        startDateOptions : {
            maxDate : new Date(2030, 12, 31),
            minDate : new Date(2000, 1, 1),
            startingDay : 1
        },

        endDateOptions : {
            maxDate : new Date(2030, 12, 31),
            minDate : new Date(2000, 1, 1),
            startingDay : 1
        }
    };

    var openStartDate = function() {
        dateProperties.startDateOpened = true;
    };

    var openEndDate = function() {
        dateProperties.endDateOpened = true;
    };

    return {
        updators : updators,
        dateProperties : dateProperties,
        openStartDate : openStartDate,
        openEndDate : openEndDate
    };
});