//Data Provider Data Object
(function() {
    angular.module('mctApp').factory('DPDTO', DPDTO);
    DPDTO.$inject = ['HTTPWrapper', '$http', 'MVMConfig'];

    function DPDTO(HTTPWrapper, $http, MVMConfig) {

        /* -------------------- Private Methods ---------------------- */
        /* --------------------------------------------------------- */

        //Merge Selected OEM Associations and Static Types
        function mergeSelectedOEMs(OEMLeadCategoryMappingList) {
            if(!OEMLeadCategoryMappingList)
                return; // do nothing

            var that = this;
            //Merge Selected OEM Associations and Static Types
            for (var x = 0; x < OEMLeadCategoryMappingList.length; x++) {
                var objX = OEMLeadCategoryMappingList[x];
                //objX.oemDto.checked = true;
                var oemId = objX.oemDto.id;

                if (angular.isDefined(that.data.leadCategoryOemMappings)) {
                    for (var y = 0; y < that.data.leadCategoryOemMappings.length; y++) {
                        var objY = that.data.leadCategoryOemMappings[y];
                        var found = false;
                        for (var z = 0; z < objY.oems.length; z++) {
                            found = objY.oems[z].id == oemId;
                            if (found) {
                                objX.oemDto.checked = true;
                                break;
                            }
                        }
                        if (found) {
                            objX.selectedCat = objY.id;
                            //if selected Cat is disabled set the oemDto disabled too
                            objX.selectedCatEnabled = angular.isDefined(objY.enabled) ? objY.enabled : false;
                            break;
                        }
                    }
                    objX.selectedCatEnabled = angular.isDefined(objX.selectedCatEnabled) ? objX.selectedCatEnabled : true; //just consider it enabled
                    objX.oemDto.usedMapping = objX.leadCategories.length > 0;
                }
                else if (angular.isDefined(that.data.oems)) {
                    for (var y = 0; y < that.data.oems.length; y++) {
                        var objY = that.data.oems[y];
                        if (objY.id == oemId) {
                            objX.oemDto.checked = true;
                            break;
                        }
                    }
                    objX.oemDto.usedMapping = objX.leadCategories.length > 0;
                }
                else {
                    objX.oemDto.usedMapping = false;

                }
            }
        }

        /* -------------------- Public Methods ---------------------- */
        /* --------------------------------------------------------- */

        //initialize Data with default values, and prepare the data object of DTO
        var initData = function(crmCodes, oemLeadCategoryMappingList, _savedData) {
            this.data = {
                userId: 0,
                type: 'LEAD',
                providerMediumType: 'FTP_SFTP',
                providerMediumFtpType: 'FTP',
                providerMediumFileType: 'CSV_PSV',
                providerMediumFtpConnMode: 'PASSIVE',
                providerMediumSecureProtocol: 'SSL',
                dpCRMCodesDtos: [{}]
            };

            this.CrmCodes = crmCodes;
            this.oemLeadCategoryMappingList = oemLeadCategoryMappingList;

            if(_savedData) {
                this.data = angular.extend(this.data, _savedData);
                mergeSelectedOEMs.call(this, this.oemLeadCategoryMappingList);
            }

            //set default values
            if (!angular.isDefined(this.data.providerMediumFtpConnMode)) {
                this.data.providerMediumFtpConnMode = 'PASSIVE';
            }
            if (!angular.isDefined(this.data.providerMediumSecureProtocol)) {
                this.data.providerMediumSecureProtocol = 'SSL';
            }

        };


        var getCRMCode = function(crmCode) {
            var that = this;
            if(crmCode != null)
            {
                for(var x=0; x < that.CrmCodes.length ;x++)
                {
                    if(that.CrmCodes[x].id == crmCode.id)
                    {
                        return that.CrmCodes[x];
                    }
                }
            }
            return that.CrmCodes[0];
        };

        /*
         This method set/initialize different variables based on which form hide/show contents, i.e. content related to Medium Type
         whether its FTP or Local File or XML API etc
         */
        function setFormMediumTypeComponents(newVal) {
            var that = this;
            if (newVal == 'XML_API' || newVal == 'XML API') {
                that.isXMLAPISelected = true;
            }else if(newVal == 'REST_API' || newVal == 'REST API') {
                that.isXMLAPISelected = false;
                that.isRESTAPISelected = true;
            }
            else if (newVal == 'LOCAL_FILE_FOLDER' || newVal == 'Local File/Folder') {
                that.isXMLAPISelected = false;
                that.isRESTAPISelected = false;
                that.isFTPSFTPSelected = false;
                that.isLocalFileSelected = true;
            } else {
                that.isFTPSFTPSelected = true;
                that.isXMLAPISelected = false;
                that.isRESTAPISelected = false;
                that.isLocalFileSelected = false;
            }
        }

        var isCheckboxEnabled = function (item) {

            if (item.oemDto.enabled && item.selectedCatEnabled && item.oemDto.usedMapping && item.leadCategories.length > 0) {
                return true;
            }
            if (item.oemDto.enabled && !angular.isDefined(item.selectedCatEnabled) && !item.oemDto.usedMapping && item.leadCategories.length > 0) {
                return true;
            }
            return false;
        };

        var getName = function(list, key) {
            for(var i=0; i<list.length; i++) {
                var obj = list[i];
                if(obj.key == key) {
                    return obj.val;
                }
            }
            return '';
        };

        // use this method to clone a data provider on create page
        var applyData = function(dp) {
            this.initData(this.CrmCodes, this.oemLeadCategoryMappingList, dp);
            //clear name and key
            this.data.name = '';
            this.data.authKey = '';
        };

        var save = function(formSelector, url, listUrl) {
            var dataToPost = $(formSelector).serialize();
            HTTPWrapper.postForm(url, dataToPost, listUrl);
        };

        var openStartDate = function (mvmProperties) {
            MVMConfig.openStartDate(mvmProperties);
        };

        var openEndDate = function (mvmProperties) {
            MVMConfig.openEndDate(mvmProperties);
        };

        var changeMinEndDate = function (mvmProperties) {
            MVMConfig.changeMinEndDate(mvmProperties);
        };

        var gatherStatistics = function (mvmProperties) {
            MVMConfig.gatherStatistics(mvmProperties, this.data.mvmConfig.id, $http);
        };

        var isFailedValue = function (value) {
            return MVMConfig.isFailedValue(value);
        };

        return {
            data: {},
            mvmConfig: {},
            CrmCodes: null, //will be initiated after initData call
            initData: initData,
            isCheckboxEnabled: isCheckboxEnabled,
            getName: getName,
            getCRMCode: getCRMCode,
            setFormMediumTypeComponents: setFormMediumTypeComponents,
            applyData: applyData,
            save: save,
            openStartDate: openStartDate,
            openEndDate: openEndDate,
            changeMinEndDate: changeMinEndDate,
            gatherStatistics: gatherStatistics,
            isFailedValue: isFailedValue
        }
    }
})();