MCTApp.controller("DevToolController", ["$scope", "$http", "$location", function($scope, $http, $location) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.submitForm = function(angObj) {
        $scope.page.isPageLoading = true;
        var d = $.param({
            'dpAuthToken': $("#dpAuthToken").val(),
            'zohoModuleName': $("#zohoModuleName").val(),
            'zohoAccountAuthToken':  $("#zohoAccountAuthToken").val()
        });
        httpAjax($http, 'POST', 'devtool/pullProspectsFromZoho', d, function(data) {
            $scope.page.isPageLoading = false;
            $location.path("/devtool/pullProspectsFromZoho");
            $.growl.message(data);
        });
    };

    $scope.devToolItemRunModal = function(requestedUrl, isViewRequested, successLocation, isPageLoading, defaultMessage) {
        $scope.devToolItem = {};
        $scope.devToolItem.requestedUrl = requestedUrl;
        $scope.devToolItem.isViewRequested = isViewRequested;
        $scope.devToolItem.successLocation = successLocation;
        $scope.devToolItem.isPageLoading = isPageLoading;
        $scope.devToolItem.defaultMessage = defaultMessage;
        $scope.showModal('#devtool-item-run-modal');
    };

    $scope.devToolItemRunConfirm = function() {
        $scope.hideModal();
       if ($scope.devToolItem.isViewRequested) {
           $location.path($scope.devToolItem.requestedUrl).replace();
           return;
        } 
        if ($scope.devToolItem.isPageLoading) {
            $scope.page.isPageLoading = true;
            httpAjax($http, 'POST', $scope.devToolItem.requestedUrl, null, function(data) {
                         $scope.page.isPageLoading = false;
                         $location.path($scope.devToolItem.successLocation);
                         $.growl.message(data);
            });
        } else {
            httpAjax($http, 'POST', $scope.devToolItem.requestedUrl, null, function() {});
            $.growl.message($scope.devToolItem.defaultMessage);
        }
    };

    $scope.submitProcessingLeadsForm = function() {
        $scope.page.isPageLoading = true;
        var d = $.param({'criteriaQuery': $("#criteriaQuery").val(), 'serviceUrl': $("#serviceUrl").val(),'successResponseXpath':$("#successResponseXpath").val()});
        httpAjax($http, 'POST', 'devtool/processingFailedLeads', d, function(data) {
                    $scope.page.isPageLoading = false;
                    $location.path("/devtool/processingFailedLeads");
                    $.growl.message(data);
        });
    };

    $scope.getProcessPendingLeadsAndLeadDispositionsMessage = function() {
        return {success: 'Processing started. It can take some time, since select for leads and dispositions is slow. Please monitor logs for detailed information.'}
    };

    $scope.submitHHIDForm = function() {
        $.ajax({
            type: "POST",
            url: 'devtool/hhid',
            data: $('form#hhIdRequestForm').serialize(),
            dataType : 'text',
            success : function(result) {
                var text = $('<div/>').text(result).html();// encode
                $("#responseDiv").html(text);
            },
            error: function (result) {
                alert('Request has not been done. Something went wrong');
                $('#responseDiv').html('Error during hhid request!');
            }
        });
    }
}]);

MCTApp.controller("DownloadCallRecordController", ["$scope", "$http", "$location", function($scope, $http, $location) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.gridPageSize = 50;

    $scope.refreshGrid = function(){
        $scope.$broadcast ('gridRefreshEvent');
        $scope.updateGrid();
    };

    $scope.downloadRecords = function(id) {
        if (id != undefined && id != null ) {
            var ids = id;
        } else {
            var inputs = $('input[name=callUid]:checked');
            var ids = $.map(inputs, function(o,k) {
                return $(o).val();
            });
        }
        //post form and show success/error message above grid
        postFormGrid($scope, $http, 'devtool/callRecords/download', 'uids=' + ids);

    }
}]);

MCTApp.controller("MapCallToLeadController", ["$scope", "$http", "$location", function($scope, $http, $location) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.gridPageSize = 1000;

    $scope.refreshGrid = function(){
        $scope.$broadcast ('gridRefreshEvent');
        $scope.updateGrid();
    };

    $scope.mapCallToLead = function(id) {
        if (id != undefined && id != null ) {
            var ids = id;
        } else {
            var inputs = $('input[name=callUid]:checked');
            var ids = $.map(inputs, function(o,k) {
                return $(o).val();
            });
        }

        $("#mapAll").attr("disabled", true);
        $("#mapAll").addClass("disabled");

        //post form and show success/error message above grid
        httpAjax($http, 'POST', 'devtool/mapCallToLead/map', 'uids=' + ids, function(data, status, headers, config) {
            $.growl.message(data);
            $scope.hideModal();
            $scope.refreshGrid();
            $("#mapAll").attr("disabled", false);
            $("#mapAll").removeClass("disabled");
        });

    }
}]);

MCTApp.controller("IAFillFilesController", ["$scope", "$http", "$location", function($scope, $http, $location) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.submitForm = function(angObj) {
        if(angObj.$valid) {
            $scope.page.isPageLoading = true;
            var d = $.param({'path': $("#path").val()});
            httpAjax($http, 'POST', 'devtool/files', d, function(data) {
                $scope.page.isPageLoading = false;
                $location.path("/devtool/files");
                $.growl.message(data);
            });
        }
    }
}]);

MCTApp.controller("UpdateRemoteFileHistory", ["$scope", "$http", "$location", function($scope, $http, $location) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.gridPageSize = 50;

    $scope.refreshGrid = function(){
        $scope.$broadcast ('gridRefreshEvent');
        $scope.updateGrid();
    };

    $scope.updateFileHistory = function(id) {
        var ids;
        if (id) {
            ids = id;
        } else {
            var inputs = $('input[name=uid]:checked');
            ids = $.map(inputs, function(o,k) {
                return $(o).val();
            });
        }
        //post form and show success/error message above grid
        postFormGrid($scope, $http, 'devtool/remoteFileHistory/update', 'uids=' + ids);

    }
}]);

MCTApp.controller("LeadNurturingController", ["$scope", "$http", "$location","NurturingPrograms","DataConsumers", function($scope, $http, $location,NurturingPrograms,DataConsumers) {
    $scope.MenuService.setActiveMenuMod('mct-mod');
    $scope.nurturingProgramList=NurturingPrograms['nurturingProgramList'];
    $scope.dataConsumerList=DataConsumers['dataConsumerList'];

    $scope.submitForm = function(angObj) {
        if(angObj.$valid) {
            $scope.page.isPageLoading = true;
            var d = $.param({'leadUids': $("#leadUid").val(), 'nurturingProgramId': $("#nurturingProgramId").val(),'dataConsumerId':$("#dataConsumerId").val()});
            httpAjax($http, 'POST', 'devtool/leadNurturing', d, function(data) {
                $scope.page.isPageLoading = false;
                $location.path("/devtool/leadNurturing");
                $.growl.message(data);
            });
        }
    }
}]);

MCTApp.controller("MktTemplateController", ["$scope", "$http", "$location","OEMs", function($scope, $http, $location, OEMs) {
    $scope.MenuService.setActiveMenuMod('mct-mod');
    $scope.oemList=OEMs['oemList'];

    $scope.isMktTemplatesValid = true;
    $scope.isCRMsValid = true;

    $scope.submitForm = function(angObj) {
        if(angObj.$valid) {
            $scope.page.isPageLoading = true;
            httpAjax($http, 'POST', 'devtool/sendMktTemplates', $("#mktTemplateForm").serialize(), function(data) {
                $scope.page.isPageLoading = false;
                $location.path("/devtool/sendMktTemplates");
                $.growl.message(data);
            });
        }
    };

    $scope.$watch('oemId', function(newVal) {
        $scope.isDataLoading = true;

        $scope.isOEMValid = true;
        $scope.isFormValid = false;
        var url = 'devtool/sendMktTemplates/templatesAndCrms?oemId=' + newVal;
        httpAjax($http, 'GET', url, null, function(data, status, headers, config) {

            $scope.mktTemplates = data.mktTemplates.length > 0 ? data.mktTemplates : null;
            $scope.crms = data.crms.length > 0 ? data.crms : null;
            $scope.isDataLoading = false;
            $scope.validateForm(true);

        }, function(data, status, headers, config) {
            $scope.isDataLoading = false;
        });
    });

    //validate Marketing Templates and CRMs
    $scope.validateForm = function() {
        var isMktTemplatesValid = $('input:checkbox[name=mktTemplateIds]:checked').size() > 0;
        var isCRMsValid = $('input:checkbox[name=crmIds]:checked').size() > 0;
        $scope.isFormValid = isMktTemplatesValid && isCRMsValid;
    }
}]);

MCTApp.controller("CronExecutionController", ["$scope", "$http", "$location", "$timeout", "CronList", function($scope, $http, $location, $timeout, CronList) {
    $scope.MenuService.setActiveMenuMod('mct-mod');


    $scope.isRunScheduled = true;
    httpAjax($http, 'GET', 'devtool/isRunScheduled', null, function (data) {
        $scope.isRunScheduled = data.isRunScheduled;
    });
    $scope.cronList = CronList.cronList;

    $scope.predicate = 'id';
    $scope.reverse = false;
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.showExecuteCronModal = function (exCron) {
        $scope.exCron = exCron;
        $scope.showModal('#execute-cron-modal');
    };

    $scope.execute = function () {
        $scope.exCron.executing = true;
        var exCron = $scope.exCron;
        $scope.data = null;
        httpAjax($http, 'POST', 'devtool/executeCron?methodName=' + $scope.exCron.methodName, null, function (data) {
            var id = exCron.id;
            $scope.data = data;
            $scope.updateStatus(id, data.error);
        });
        $timeout(function () {
            if ($scope.data == null || !$scope.data.error) {
                var data = {
                    success: 'The execution started. Check logs for details.'
                };
                $.growl.message(data);
            } else {
                $.growl.message($scope.data);
            }
            $scope.hideModal();
        }, 50);
    };

    $scope.updateStatus = function (id, status) {
        for (var i = 0; i < $scope.cronList.length; i++) {
            if ($scope.cronList[i].id == id) {
                $scope.cronList[i].executing = status;
            }
        }
    };

}]);

MCTApp.controller("ConfigKeyValuePropsController", ["$scope", "$http", "ConfigKeyValuePropsList", function($scope, $http, ConfigKeyValuePropsList) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.lpsProps = ConfigKeyValuePropsList.list;

    $scope.changeValue = function (configKeyValueProp) {
        var params = $.param({
            'paramKey': configKeyValueProp.paramKey,
            'paramValue': configKeyValueProp.paramValue,
            'dateValue': configKeyValueProp.dateValue,
            'paramValueDate':  configKeyValueProp.paramValueDate
        });
        httpAjax($http, 'POST', 'devtool/updateConfigKeyValueProps', params, function (data) {
            $.growl.message(data);
        });
    };

}]);

MCTApp.controller("CacheController", ["$scope", "$http", "CacheEntries", function ($scope, $http, CacheEntries) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.cacheEntries = CacheEntries ? CacheEntries.cacheEntries : null;

    $scope.predicate = 'formattedName';
    $scope.reverse = false;
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.clearCache = function (cache) {
        httpAjax($http, 'POST', 'devtool/clearCache?name=' + cache.name + '&type=' + cache.type, null, function (data) {
            $.growl.message(data);
            cache.size = 0;
        });
    };


}]);

MCTApp.controller("MigrateCallRecordsController", ["$scope", "$http", function ($scope, $http) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.calculateValues = function () {
        httpAjax($http, 'GET', 'devtool/leadCallRecords/currentPosition', null, function(data) {
            $scope.currentPosition = data;
        });
        httpAjax($http, 'GET', 'devtool/leadCallRecords/totalAmount', null, function(data) {
            $scope.totalAmount = data;
        });
    };

    $scope.getFileCount = function () {
        $scope.showModal('.ajax-modal');
        httpAjax($http, 'GET', 'devtool/leadCallRecords/s3FileCount', null, function(data) {
            $scope.fileCount = parseInt(data);
            $scope.fileCountText = 'Files on S3: ' + $scope.fileCount;
            $scope.hideModal();
        });
    };

    $scope.calculateValues();

    $scope.uploadRecords = function () {
        $scope.upload(false);
    };

    $scope.showUploadMissingRecordsModal = function () {
        $scope.showModal('#upload-missing-modal');
    };

    $scope.uploadMissingRecords = function () {
        $scope.hideModal();
        $scope.upload(true);
    };

    $scope.upload = function (missing) {
        var msg;
        if (!$scope.validateAmount() && !missing) {
            msg = {
                error: 'Amount must be a positive integer'
            };
            $.growl.message(msg);
            return;
        }
        var amount = missing ? 'missing' : $scope.amount;
        var amountArg = $scope.amount ? '&amount=' + $scope.amount : '';
        httpAjax($http, 'POST', 'devtool/leadCallRecords/upload?missing=' + missing + amountArg, null, function(data) {
            $.growl.message(data);
            $scope.calculateValues();
            if ($scope.fileCount && data.success) {
                var newRecords = data.success.split(" ")[0];
                $scope.fileCount += parseInt(newRecords == 'No' ? 0 : newRecords);
                $scope.fileCountText = 'Files on S3: ' + $scope.fileCount;
            }
        });
        msg = {
            success: 'Uploading of ' + amount + ' call records has started'
        };
        $.growl.message(msg);
    };

    $scope.resetPosition = function () {
        httpAjax($http, 'POST', 'devtool/leadCallRecords/resetPosition', null, function() {
            $scope.currentPosition = 0;
        });
    };

    $scope.validateAmount = function () {
        return $scope.amount % 1 === 0 && $scope.amount > 0;
    };

}]);

MCTApp.controller("ApiHealthCheckController", ["$scope", "$http", "$timeout", "HealthCheckProperties", function($scope, $http, $timeout, HealthCheckProperties) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.predicate = 'crmName';
    $scope.reverse = false;
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.healthCheckProperties = HealthCheckProperties.healthCheckProperties;

    $scope.mapStatus = function (endpoint, status) {
        for (var i = 0; i < $scope.healthCheckProperties.length; i++) {
            if ($scope.healthCheckProperties[i].endpointUrl === endpoint) {
                $scope.healthCheckProperties[i].status = status;
            }
        }
    };

    $scope.check = function (endpoint) {
        $scope.showModal('.ajax-modal');
        endpoint = endpoint ? '?endpoint=' + endpoint : '';
        httpAjax($http, 'GET', 'devtool/performHealthCheck' + endpoint, null, function(data) {
            angular.forEach(data, function(value, key) {
                $scope.mapStatus(key, value);
            });
            $timeout(function () {
                $scope.hideModal();
            }, 200);
        });
    };

    $scope.isErrorCode = function (code) {
        return code >= 400;
    };

}]);

MCTApp.controller("RawXmlLookUpController", ["$scope", "$http", "$location", "$q", function($scope, $http, $location, $q) {
    $scope.MenuService.setActiveMenuMod('mct-mod');
    $scope.entityNameList = ["Lead","LeadDisposition"];
    $scope.isResultShow = 0;
    $scope.isCancelShow = 0;
    $scope.isSearchShow = 1;
    $scope.dchSearchFields = {};
    $scope.rawXml = [];
    var canceller = $q.defer();
    var _path = $location.path();
    var reqResXMLEditor;
    var copyToClip = $('.copy-to-clip');
    $('[data-toggle="tooltip"]').tooltip();
    $scope.$watch('dchSearchFields.entityName', function(val1) {
	    var param = "?dataDestinationName="+$scope.dchSearchFields.entityName;
	    $scope.isResultShow = 0;
	    $scope.rawXml = [];
        httpAjax($http, 'GET', 'devtool/getDataConsumersIdAndName' + param, null, function(data) {
            $scope.dataConsumerList = data;
            $scope.dataProviderList = [];
        });
    });       
    
    //validation for entityId
    $scope.$watch('dchSearchFields.entityId', function(val) {
    	//As entityId is Integer need to validate range of positive Integer.
        if (val < 0 || val > 2147483647) { 
        	$.growl.message({error:'Entity Id value is not Valid.'});
        	$scope.dchSearchFields.entityId = 0;
        }
        if (val) {
        	if (val.replace(/[^0-9.]/g, '').length < val.length) {
        		$.growl.message({error:'Entity Id value should contains number only.'});
            	$scope.dchSearchFields.entityId = 0;
        	}
        }
        
    });
    
    $scope.$watch('dchSearchFields.dataConsumerId', function(val) {
    	var providers = [];
    	$scope.isResultShow = 0;
    	if($scope.dataConsumerList){
    		for(var i=0;i<$scope.dataConsumerList.length;i++) {
          		if($scope.dataConsumerList[i].id == val) {
          			var ids = $scope.dataConsumerList[i].dataProviderIds;
          			var names = $scope.dataConsumerList[i].dataProvidersNames;
          			var k=0;
    	            for(var j=0; j<ids.length; j++){
    	            	var dpname = names.split(',');
    	            	providers.push({
	            		    id: ids[j],
	            		    name: dpname[k++]
	            		 });
    	            }
    	      		$scope.dataProviderList = providers;
    	      		break;
          		}
          	}
    	} 	
    });
    
    $scope.cancelRequest = function(){
    	canceller.resolve("user cancelled"); 
    };
    
    $scope.getXml = function () {
       	var param = $("#rawXmlForm").serialize();
    	$scope.isCancelShow = 1;
    	$scope.isSearchShow = 0;
    	$scope.showIndicator();
    	$http({
            method: 'GET',
            url: 'devtool/getRawXml?'+ param,
            data: null,
            timeout: canceller.promise,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data, status, headers, config) {
		 	$scope.rawXml = data;
		 	$scope.isResultShow = 1;
		    if ($scope.rawXml.length === 0) {
		        $.growl.message({error:'No result found'});
		    	$scope.isResultShow = 0;
		    }
		    $scope.isCancelShow = 0;
		    $scope.isSearchShow = 1;
		})
		.error(function(data, status, headers, config){
	        var ele = angular.element('.error-code');
	        ele.html(status);
	        var scope = angular.element('.error-modal').scope();
	        scope.showModal('.error-modal');
	        $scope.isCancelShow = 0;
	        $scope.isResultShow = 0;
	        $scope.isSearchShow = 1;
	        $scope.rawXml = [];
	        canceller = $q.defer();
		});
    	$scope.hideIndicator();
    };
    
    if(_path.indexOf('devtool/rawXmlLookUp') > 0) {
        reqResXMLEditor = CodeMirror.fromTextArea($('#test-response')[0], {
            lineNumbers: true,
            mode:  "text/html",
            readOnly: true
        });
    }
    
    $scope.showContentModal = function(res, tag) {
        $('#response-modal .modal-title').text(tag);
        $scope.showModal('#response-modal');
        res = vkbeautify.xml(res);
        //if its broken then remove undefined
        res = res.replace(/>undefined</g, '><');    //this is required to patch vkbeautify plugin
        reqResXMLEditor.setValue(res);
        setTimeout(function() {
            reqResXMLEditor.refresh();
        }, 700);
    }; 
    
    if(copyToClip.size() > 0) {
    	copyToClip.tooltip({placement: 'bottom',trigger: 'manual'});
        clipboard = new Clipboard('.copy-to-clip', {
            text: function(trigger) {
            	copyToClip.tooltip('show');
                return reqResXMLEditor.getValue();
            }
        });
        copyToClip.hover(null, function() {
            $(this).tooltip('hide');
        });
    }
    
}]);

MCTApp.controller("RawMessageLookUpController", ["$scope", "$http", "$location", "$timeout", function($scope, $http, $location, $timeout) {
    $scope.MenuService.setActiveMenuMod('mct-mod');
    $scope.isResultShow = 0;
    $scope.omhSearchFields = {};
    $scope.rawMessage = [];
    var _path = $location.path();
    var reqResMessageEditor;
    var copyToClip = $('.copy-to-clip');
    $('[data-toggle="tooltip"]').tooltip();
    $scope.getDataSenderList = function () {
        httpAjax($http, 'GET', 'devtool/getDataSendersIdAndName', null, function(data) {
            $scope.dataSenderList = data;
        }
    )};
    $scope.getDataSenderList();
    $scope.getMessage = function () {
    	var param = $("#rawMessageForm").serialize();
    	$scope.showModal('.ajax-modal');
    	httpAjax($http, 'GET', 'devtool/getRawMessage?' + param, null , function(data) {
	        $scope.rawMessage = data;
	        if($scope.rawMessage.length === 0) {
	            $.growl.message({error:'No result found'});
	        	$scope.isResultShow = 0;
	        } else{
	            $scope.isResultShow = 1;
	        }
	        $timeout(function () {
	            $scope.hideModal();
	        }, 200);
    	});
    };
    
    if(_path.indexOf('devtool/rawMessageLookUp') > 0) {
        reqResMessageEditor = CodeMirror.fromTextArea($('#test-response')[0], {
            lineNumbers: true,
            mode:  "text/html",
            readOnly: true
        });
    }
    
    $scope.showRequestResponseModal = function(row, isReq) {
        var res;
        if(!isReq) {
            res = row.rawResponse ? row.rawResponse : '';
            $('#response-modal .modal-title').text('Response');
        }
        else {
            res = row.rawMessage ? row.rawMessage : '';
            $('#response-modal .modal-title').text('Request');
        }
        $scope.showModal('#response-modal');
        res = vkbeautify.xml(res);
        //if its broken then remove undefined
        res = res.replace(/>undefined</g, '><');    //this is required to patch vkbeautify plugin
        reqResMessageEditor.setValue(res);
        setTimeout(function() {
            reqResMessageEditor.refresh();
        }, 700);
    }; 
    
    if(copyToClip.size() > 0) {
    	copyToClip.tooltip({placement: 'bottom',trigger: 'manual'});
        clipboard = new Clipboard('.copy-to-clip', {
            text: function(trigger) {
            	copyToClip.tooltip('show');
                return reqResMessageEditor.getValue();
            }
        });
        copyToClip.hover(null, function() {
            $(this).tooltip('hide');
        });
    }
    
    $scope.inlineOptions = {
       customClass: getDayClass,
       minDate: new Date(),
       showWeeks: true
    };

    $scope.dateOptions = {
        maxDate: new Date(2030, 12, 31),
        minDate: new Date(),
        startingDay: 1
    };

    $scope.toggleMin = function() {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.openDateFrom = function() {
        $scope.popupDateFrom.opened = true;
    };
      
    $scope.openDateTo = function() {
        $scope.popupDateTo.opened = true;
    };
  
    $scope.popupDateFrom = {
        opened: false
    };
  
    $scope.popupDateTo = {
       opened: false
    };

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date);
        }
        return '';
    }
}]);

var checkQueryTimeController = function($scope, $http) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.check = function() {
        $scope.page.isPageLoading = true;
        httpAjax($http, 'GET', 'devtool/query/check', null, function(data) {
            $scope.page.isPageLoading = false;
            $scope.applications = [ {
                name : 'LMS Query Time',
                data : data.lmsQueryTimes
            }, {
                name : 'MCT Query Time',
                data : data.mctQueryTimes
            } ];
            $scope.times = range(1, data.amount);
        });
    };

    function range(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    }
};
MCTApp.controller('CheckQueryTimeController', checkQueryTimeController);
checkQueryTimeController.$inject = [ '$scope', '$http' ];


var updateGeoController = function($scope, $http, $location, status, GeoUpdateFactory) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    var pageUrl = $location.path().substring(1) + '/';
    $scope.DTO = GeoUpdateFactory;
    $scope.updator = {};

    if (status.busy) {
        $scope.busy = status.busy;
        $scope.total = status.total;
        $scope.finished = status.finished;
    }

    $scope.execute = function(updator) {
        if (!confirm('Confirm updating')) {
            return;
        }

        httpAjax($http, 'POST', pageUrl + updator.url, $.param(updator), function() {
            $location.path(pageUrl);
        });
    };

    $scope.cancel = function() {
        if (confirm('Confirm canceling')) {
            httpAjax($http, 'POST', 'devtool/updateGeo/cancel', null, function() {
                $location.path(pageUrl);
            });
        }
    };

    $scope.invalidDateRange = function() {
        return $scope.updator.endDate < $scope.updator.startDate;
    };

    $scope.$watch('updator.endDate', function(newValue) {
        GeoUpdateFactory.dateProperties.startDateOptions.maxDate = newValue;
    });

    $scope.$watch('updator.startDate', function(newValue) {
        GeoUpdateFactory.dateProperties.endDateOptions.minDate = newValue;
    });

};
MCTApp.controller('UpdateGeoController', updateGeoController);
updateGeoController.$inject = [ '$scope', '$http', '$location', 'status', 'GeoUpdateFactory'];

MCTApp.controller("SuspendedOmhController", ["$scope", "$http", "OutDataConnectors", "DataSenders",
    function ($scope, $http, OutDataConnectors, DataSenders) {
        $scope.MenuService.setActiveMenuMod('mct-mod');

        $scope.outDataConnectors = OutDataConnectors ? OutDataConnectors.odcList : null;
        $scope.dataSenders = DataSenders ? DataSenders.dataSenders : null;
        $scope.days = 14;

        $scope.submitForm = function (angObj) {
            $scope.page.isPageLoading = true;
            if (angObj.$valid) {
                httpAjax($http, 'POST', 'devtool/suspendOmhRecords', $('#suspendedOmhForm').serialize(),
                    function (data) {
                        $scope.page.isPageLoading = false;
                        $.growl.message(data);
                    });
            }
        };
    }]);
