/**
 * Controller for List page of Out Data Connector
 * #/bidirectional/odc/list
 */
(function() {
    angular.module('mctApp').controller("OutDataConnectorController", OutDataConnectorController);
    OutDataConnectorController.$inject = ['$scope', 'GridView'];

    function OutDataConnectorController($scope, GridView){
        $scope.MenuService.setActiveMenuMod('ds-mod', 'Bi-directional');  //set the correct Menu

        //initialize a local variable with GridView Service
        $scope.Grid = GridView.init('bidirectional/odc/grid');

        $scope.enableDisableData = {};
        $scope.showEnableDisableModal = function(row) {
            $scope.enableDisableData = {
                id: row.id,
                name: row.name,
                title: row.enabled ? 'Disable' : 'Enable',
                url:  row.enabled ? 'bidirectional/odc/disable' : 'bidirectional/odc/enable'
            };
            $scope.showModal('#enable-disable-modal');
        };
        $scope.confirmEnableDisable = function(data) {
            $scope.showModal('.ajax-modal', function(){
                $scope.Grid.postForm(data.url, 'id=' + data.id, $scope);
            });
        };
        $scope.showDeleteModal = function(row) {
            $scope.odcNameToDelete = row.name;
            $scope.odcIdToDelete = row.id;
            $scope.showModal('#delete-odc-modal');
        };
        $scope.confirmDeletion = function() {
            $scope.Grid.postForm('bidirectional/odc/delete', 'id=' + $scope.odcIdToDelete, $scope);
        };
    }

})();


/**
 * Controller for create and modify pages of Out Data Connector
 * #/bidirectional/odc/create
 * #/bidirectional/odc/modify/:id
 */
(function() {
    angular.module('mctApp').controller('OutDataConnectorCreateModifyController', OutDataConnectorCreateModifyController);

    OutDataConnectorCreateModifyController.$inject = ['$scope', 'OutDataConnector', 'OutMessageTypes', 'DataSenders', 'StaticTypes', 'OutDataConnectorDTO'];
    function OutDataConnectorCreateModifyController($scope,  OutDataConnector, OutMessageTypes, DataSenders, StaticTypes, OutDataConnectorDTO) {

        $scope.MenuService.setActiveMenuMod('ds-mod', 'Bi-directional');  //set the correct Menu
        if(OutDataConnector) {  //if modify page
            OutDataConnectorDTO.initData(DataSenders.dataSenders, OutMessageTypes.outMessageTypeList, OutDataConnector.odc);
            $scope.isUpdate = true;
        }
        else {
            OutDataConnectorDTO.initData(DataSenders.dataSenders, OutMessageTypes.outMessageTypeList);
            $scope.isUpdate = false;
        }
        $scope.DTO = OutDataConnectorDTO;   //to use methods of DTO object
        $scope.ODC = OutDataConnectorDTO.data;  //data object to bind with html elements
        $scope.pageLabel = $scope.isUpdate ? 'Modify' : 'Create';

        $scope.zeroResultActionTypes = keyValToArray(StaticTypes.zeroResultActionTypes);
        $scope.messageTypeTransformation = keyValToArray(StaticTypes.transformationTypes);

        $scope.submitForm = function(angObj) {
            var url = 'bidirectional/odc/' + ($scope.isUpdate ? 'modify' : 'create');

            if(angObj.$valid) {
                $scope.page.isPageLoading = true;
                $scope.DTO.save('#createForm', url, "/bidirectional/odc/list");
            }
        };

        $scope.showNotification = function(chk) {
            if(chk)
                $scope.showModal('#notification-modal');
        };
        $scope.showResendFailedNotification = function(chk) {
            if(!chk)
                $scope.showModal('#resendFailed-notification-modal');
        };

    }
})();