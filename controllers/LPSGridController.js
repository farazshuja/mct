MCTApp.controller("LPSGridController", ["$scope", "MCTAPI", "$attrs", "$route", function($scope, MCTAPI, $attrs, $route) {
    $scope.$on('gridRefreshEvent', function() {
        $scope.$parent.updateGrid = $scope.updateGrid;
    });

    $scope.grid = {
        search : {
            "_search": '',
            "page": 1,
            "rows": $attrs.rowSize ? $attrs.rowSize : $scope.gridPageSize,
            "sidx": $attrs.rowIndex ? $attrs.rowIndex : $scope.sidx,
            "sord": $scope.sord ? $scope.sord : "asc"
        },
        pager: {
            pagerLabel: null,
            hasNextPages: false,
            hasPrevPages: false
        },
        //message: $scope.operation,
        data: {},
        isGridLoading: false,
        listStr: '',
        loadOnActive: $attrs.loadOnActive,
        gridService: $attrs.gridData ? $attrs.gridData : $scope.GridData
    };
    if($attrs.gridUrlId) {
        $scope.grid.search.id = $route.current.params.id;
    }

    $scope.updateGrid = function(gService) {
        $scope.grid.gridService = angular.isDefined(gService) ? gService : $scope.grid.gridService;
        $scope.grid.isGridLoading = true;

        if($attrs.gridParam) {  //if there is some custom param, parse it and pass to search
            var v = $attrs.gridParam.split(':');
            $scope.grid.search[v[0]] = $scope[v[1]];
        }

        MCTAPI[$scope.grid.gridService].get($scope.grid.search).$promise.then(function(result) {
            //result has zero rows then show prev page
            if(angular.isDefined(result.rows) && result.rows.length == 0 && $scope.grid.search.page > 1){
                $scope.prevPage(true);
                return;
            }

            $scope.grid.data = result;

            $scope.grid.isGridLoading = false;
            updatePager();
            
            //show empty msg if no data
            if(!$scope.grid.pager.pagerLabel){
                angular.element('.empty-row').removeClass('hide');
            }
            else {
                angular.element('.empty-row').addClass('hide');
            }

        }, function(reason) {

        });
    };
    $scope.clearGrid = function() {
        $scope.grid.data = null;
        $scope.grid.isGridLoading = false;
        angular.element('.empty-row').removeClass('hide');
    };

    if($scope.grid.gridService && !$scope.grid.loadOnActive)
        $scope.updateGrid();
    else
        $scope.clearGrid();

    $scope.sortOrder = function(event, name) {
        var $elem = $(event.currentTarget);
        var c = $elem.hasClass('desc') ? 'asc' : 'desc';
        $('.sortable').removeClass('asc desc');
        $elem.addClass(c);
        $scope.grid.search.page = 1;
        $scope.grid.search.sidx = name;
        $scope.grid.search.sord = c;
        $scope.updateGrid();
    }

    $scope.isAnyCheckBoxChecked = function(col) {
        var list = [];
        if($scope.grid.data && $scope.grid.data.rows){
            for (var i = 0; i < $scope.grid.data.rows.length; i++) {
                if ($scope.grid.data.rows[i].checked) {
                    var l = $scope.grid.data.rows[i];
                    list.push(l[col]);
                }
            }
        }
        $scope.grid.listStr = list.join(', ');
        return list.length == 0 ? false : true;
    }

    function updatePager() {
        var start = (($scope.grid.search.page - 1) * $scope.grid.search.rows) + 1;
        var end = $scope.grid.search.page * $scope.grid.search.rows;
        var total = parseInt($scope.grid.data.records);
        end = end > total ? total : end;
        $scope.grid.pager.pagerLabel = isNaN(total) || total == 0 ? null : 'Showing ' + start + ' - ' + end + ' of ' + total;
        $scope.grid.pager.hasNextPages = end < total ? true : false;
        $scope.grid.pager.hasPrevPages = start > 1 ? true : false;
    }


    $scope.nextPage = function() {
        if(!$scope.grid.isGridLoading) {
            $scope.grid.search.page++;
            $scope.updateGrid();
        }
    }
    $scope.prevPage = function(forcePrev) {
        if(!$scope.grid.isGridLoading || forcePrev) {
            $scope.grid.search.page--;
            $scope.updateGrid();
        }
    }

}]);
