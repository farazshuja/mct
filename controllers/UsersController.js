/**
 * Controller for List page of User
 * #/users/list
 */
(function() {
    angular.module('mctApp').controller("UsersController", UsersController);
    UsersController.$inject = ["GridView", "HTTPWrapper", "MenuService", "Modal"];
    function UsersController(GridView, HTTPWrapper, MenuService, Modal) {

        var vm = this;  //view model
        MenuService.setActiveMenuMod('mct-mod', 'ACL');

        //initialize a local variable with GridView Service
        vm.Grid = GridView.init('system/users/grid', {sidx: 'loginId'});

        //if the modal requires to load some data from ajax then use this method to
        //show Modal
        vm.deleteUsers = function (listStr) {
            Modal.showModal('.ajax-modal', function(){
                var d = $.param({'emails': listStr});
                HTTPWrapper.httpAjax('POST', "system/users/verify", d, function(data) {
                    if(data.isCurrentUser){
                        Modal.showModal('#cannot-delete-modal');
                    }
                    else {
                        $('#delete-users-modal strong').html(listStr);
                        Modal.showModal('#delete-users-modal');
                    }
                });
            });

        };

        vm.confirmDeleteUsers = function() {
            var inputs = $('input[name=loginId]:checked');
            var ids = $.map(inputs, function(o,k) {
                return $(o).val();
            });
            Modal.showModal('.ajax-modal', function(){
                vm.Grid.postForm('system/users/delete', 'loginIds=' + ids, Modal);
            });
        }

    };

})();

/**
 * Controller for create and modify pages of Users
 * #/users/create
 * #/users/modify/:id
 */
(function() {
    angular.module('mctApp').controller("UserEditController", UserEditController);
    UserEditController.$inject = ["grpList", "User", "UserDTO", "$templateCache", "MenuService"];

    function UserEditController(grpList, User, UserDTO, $templateCache, MenuService) {
        var vm = this;
        MenuService.setActiveMenuMod('mct-mod', 'ACL');
        $templateCache.removeAll();

        if (User) {
            UserDTO.initData(grpList.groupList, User.userList);
            vm.isUpdate = true;
        }
        else {
            UserDTO.initData(grpList.groupList);
        }

        vm.User = UserDTO.data;
        vm.groups = grpList.groupList;
        vm.selectedGroups = User ? User.userList.userGroups : [];

        vm.submitForm = function(angObj) {
            if(angObj.$valid) {
                var url = 'system/users/' + (vm.isUpdate ? 'modify' : 'create');
                UserDTO.save('#createForm', url, "/users/list");
            }
        }
    }
})();
