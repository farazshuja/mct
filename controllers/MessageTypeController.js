MCTApp.controller("MessageTypeController", ['$scope', '$resource', '$http', function($scope, $resource, $http){
    $scope.MenuService.setActiveMenuMod('ds-mod', 'Bi-directional');  //set the correct Menu

    $scope.refreshGrid = function(){
        $scope.$broadcast ('gridRefreshEvent');
        $scope.updateGrid();
    };

    $scope.enableDisableData = {};
    $scope.showEnableDisableModal = function(row) {
        $scope.enableDisableData = {
            id: row.id,
            name: row.name,
            title: row.enabled ? 'Disable' : 'Enable',
            url:  row.enabled ? 'bidirectional/messageTypes/disable' : 'bidirectional/messageTypes/enable'
        }
        $scope.showModal('.ajax-modal', function() {
            $scope.showModal('#enable-disable-modal');
        });
    };
    $scope.confirmEnableDisable = function(row) {
        $scope.showModal('.ajax-modal', function(){
            postFormGrid($scope, $http, row.url, 'id=' + row.id);
        });
    };

}]);

MCTApp.controller("MessageTypeCreateController", ['$scope', '$resource', '$http', '$location', '$filter', 'MessageType', function($scope, $resource, $http, $location, $filter, MessageType) {
    $scope.MenuService.setActiveMenuMod('ds-mod', 'Bi-directional');  //set the correct Menu

    $scope.MT = MessageType ? MessageType.messageType : null;

    $scope.submitForm = function(angObj) {
        var url = 'bidirectional/messageTypes/' + ($scope.MT ? 'modify' : 'create');

        if(angObj.$valid) {
            $scope.page.isPageLoading = true;
            $('input[disabled=disabled]').removeAttr('disabled');

            httpAjax($http, 'POST', url, $("#createForm").serialize(), function(data) {
                $location.path("/bidirectional/messageTypes/list");
                $.growl.message(data);
            });
        }
    }

}]);