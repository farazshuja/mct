MCTApp.controller("CrmDsMappingController", ['$scope', '$rootScope', '$resource', '$http', '$filter', 'OEMsLite', 'GridView', function($scope, $rootScope, $resource, $http, $filter, OEMsLite, GridView){
    $scope.MenuService.setActiveMenuMod('ds-mod', 'Bi-directional');  //set the correct Menu

    $scope.OEMsLite = OEMsLite.oemList;

    if($rootScope.CRMMapping) { //if its already set then use it
        $scope.oemList = $rootScope.CRMMapping.selectedOEM;
    }
    else {
        $scope.oemList = $scope.OEMsLite[0].id;
    }

    $scope.$watch('oemList', function(n,o) {
        $rootScope.CRMMapping = {
            selectedOEM: n
        }
    });


    //initialize a local variable with GridView Service
    $scope.Grid = GridView.init('bidirectional/crmDsMapping/grid', {
        oemId: $scope.oemList
    });

    $scope.$watch('oemList', function(newVal, oldVal) {
        $scope.Grid.refresh({
            oemId: $scope.oemList
        });
    });




    $scope.deleteRow = function (row) {
        $scope.showModal('.ajax-modal', function(){
            httpAjax($http, 'GET', 'bidirectional/crmDsMapping/deleteAllowed?id=' + row.id, null, function(data) {
                data = JSON.parse(data);
                if(data) {
                    $('#delete-modal strong').html(row.crmName);
                    $('#toDeleteId').val(row.id);
                    $scope.showModal('#delete-modal');
                }
                else {
                    $('#cannot-delete-modal strong').html(row.crmName);
                    $scope.showModal('#cannot-delete-modal');
                }
            });
        });

    };

    $scope.confirmDelete = function() {
        var id = $('#toDeleteId').val();
        //post form and show success/error message above grid
        $scope.Grid.postForm('bidirectional/crmDsMapping/delete', 'id=' + id, $scope);

    }
}]);
MCTApp.controller("CrmDsMappingEditController", ['$scope', '$rootScope', '$location', '$filter', 'CRM', function($scope, $rootScope, $location, $filter, CRM) {
    $scope.MenuService.setActiveMenuMod('ds-mod', 'Bi-directional');  //set the correct Menu

    $scope.isUpdate = CRM.isUpdate;
    // CD -> Crm Data
    $scope.CD = CRM.data;

    if(!$scope.isUpdate && $rootScope.CRMMapping) {  //if create page, load oemId from list page
        //set it only if its not disable
        var id = $rootScope.CRMMapping.selectedOEM;
        var found = $filter('searchInObject')(CRM.OEMsLite, 'id', id);
        if(found && found.enabled) {
            $scope.CD.oemId = id;
        }
    }

    $scope.OEMsLite = CRM.OEMsLite;
    $scope.DataSendersNotEmailLite = CRM.DataSendersNotEmailLite;
    $scope.DataSendersNotEmailLiteWithNone = CRM.DataSendersNotEmailLiteWithNone;
    $scope.DataSendersEmailLite = CRM.DataSendersEmailLite;
    $scope.DataProvidersForCRMIntegrationWithNone = CRM.DataProvidersForCRMIntegrationWithNone;

    $scope.$watch('CD.oemId', function(n,o) {
        $rootScope.CRMMapping = {
            selectedOEM: n
        }
    });

    $scope.submitForm = function(angObj) {
        if(angObj.$valid) {
            var url = $scope.isUpdate ? 'bidirectional/crmDsMapping/modify' : 'bidirectional/crmDsMapping/create';
            $scope.page.isPageLoading = true;
            CRM.save('#createForm', url, "/bidirectional/crmDsMapping/list");
        }
    }
}]);