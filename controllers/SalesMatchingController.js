MCTApp.controller("ScriptExecutionCreateEditController", ["$scope", "script", "$http", "$location", function($scope, script, $http, $location) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Status & Execution Control');
    $scope.editor = CodeMirror.fromTextArea(document.getElementById('executionScript'), {
        mode: 'text/x-sql',
        indentWithTabs: true,
        smartIndent: true,
        lineNumbers: true,
        matchBrackets : true
    });

    var v = '--Type your script here';
    if(script){
        $scope.form = script.scriptConfig;
        v = $scope.form.executionScript;
    }
    else {
        $scope.executionScript = v;
    }

    $scope.editor.getDoc().setValue(v);

    $scope.editor.on('change', function(){
        var text = $scope.editor.getValue();
        if(script){
            $scope.form.executionScript = text;
        }
        else {
            $scope.executionScript = text;
        }
        $scope.$apply();
        $('#executionScript').val(text);
    });

    $scope.submitForm = function(form) {
        if(form.$valid) {
            var url = script ? 'scriptExecution/sales/edit/' + $scope.form.id : 'scriptExecution/sales/create';
            $scope.page.isPageLoading = true;
            httpAjax($http, 'POST', url, $("#" + form.$name).serialize(), function(data) {
                $location.path("/scriptExecution/sales");
                $.growl.message(data);
            });
        }
    }

}]);

MCTApp.controller("ScriptExecutionCreateController", ["$scope", "$http", "$location", function($scope, $http, $location) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Status & Execution Control');
    $scope.editor = CodeMirror.fromTextArea(document.getElementById('executionScript'), {
        mode: 'text/x-sql',
        indentWithTabs: true,
        smartIndent: true,
        lineNumbers: true,
        matchBrackets : true
    });
    var v = '--Type your script here';
    $scope.executionScript = v;
    $scope.editor.getDoc().setValue(v);
    $scope.editor.on('change', function(){
        var text = $scope.editor.getValue();
        $scope.executionScript = text;
        $scope.$apply();
        $('#executionScript').val($scope.editor.getValue());
    });

    $scope.submitForm = function(form) {
        if(form.$valid) {
            $scope.page.isPageLoading = true;
            httpAjax($http, 'POST', 'scriptExecution/sales/create', $("#" + form.$name).serialize(), function(data) {
                $location.path("/scriptExecution/sales");
                $.growl.message(data);
            });
        }
    }
}]);

MCTApp.controller("ScriptExecutionController", ["$scope", "$http", "$location", function($scope, $http, $location) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Status & Execution Control');
    $scope.refreshGrid = function(){
        $scope.$broadcast ('gridRefreshEvent');
        $scope.updateGrid();
    }

    $scope.rowData = {
        title: 'Disable',
        id: '',
        name: ''
    }
    $scope.handlePopups = function(row, showDel) {
        $scope.rowData = {
            id: row.id,
            name: row.name,
            title: showDel ? 'Delete' : row.disabled ? 'Enable': 'Disable'
        }
        $scope.showModal('#enable-disable-modal');
    }
    $scope.confirmPopup = function() {
        var url = 'scriptExecution/sales/';
        url += $scope.rowData.title == 'Delete' ? 'delete/' : $scope.rowData.title == 'Enable' ? 'enable/' : 'disable/';
        url += $scope.rowData.id;
        $scope.showModal('.ajax-modal', function(){
            postFormGrid($scope, $http, url, null);
        });
    }

    $scope.startExecution = function(row){
        row.inProgress = true;
        httpAjax($http, 'POST', 'scriptExecution/sales/execute/' + row.id, null, function(data) {
            $.growl.message(data);
            if(data.success){
                $scope.checkExecutionStatus(row);
            }
            else {
                row.inProgress = false;
            }
        });
    };

    $scope.checkExecutionStatus = function(row) {
        setTimeout(function(){
            httpAjax($http, 'GET', 'scriptExecution/sales/checkStatus/' + row.id, null, function(data) {
                if(data.inProgress){
                    $scope.checkExecutionStatus(row);
                }
                else {//else if process completed
                    $.growl.message({'success': 'Script executed successfully'});
                    $scope.refreshGrid();
                }
            });
        }, 3000);
    }

}]);
