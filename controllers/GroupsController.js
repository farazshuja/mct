/**
 * Controller for List page of Groups
 * #/groups/list
 */
(function() {
    angular.module('mctApp').controller("GroupsController", GroupsController);
    GroupsController.$inject = [ "GridView", "MenuService", "Modal" ];

    function GroupsController(GridView, MenuService, Modal) {
        var vm = this; // to bind objects to view page
        MenuService.setActiveMenuMod('mct-mod', 'ACL');

        // initialize a local variable with GridView Service
        vm.Grid = GridView.init('system/groups/grid', {
            sidx : 'groupId'
        });

        vm.deleteGroups = function(list) {
            var groupNames = list.split(', ');
            var $chks = $('input:checkbox[name=groupId]:checked');
            var hasAnyMember = false;
            $chks.each(function(i, o) {
                var v = $(o).val();
                if (v != "") {
                    hasAnyMember = true;
                    return false;
                }
            });

            if (hasAnyMember) {
                Modal.showModal('#cannot-delete-modal');
            } else {
                $('#delete-confirmation-modal strong').html(list);
                Modal.showModal('#delete-confirmation-modal');
            }
        };

        vm.confirmDeleteGroups = function() {
            var ids = [];
            for (var i = 0; i < vm.Grid.data.rows.length; i++) {
                var row = vm.Grid.data.rows[i];
                if (row.checked) {
                    ids.push(row.groupId);
                }
            }
            Modal.showModal('.ajax-modal', function() {
                vm.Grid.postForm('system/groups/delete', 'groupIds=' + ids, Modal);
            });

        };

    }

})();

/**
 * Controller for create and modify pages of Group
 * #/groups/create
 * #/groups/modify/:id
 */
(function() {
    angular.module('mctApp').controller("GroupEditController", GroupEditController);
    GroupEditController.$inject = [ "GroupDTO", "Group", "SelectedUsers", "Users", "MenuService" ];
    function GroupEditController(GroupDTO, Group, SelectedUsers, Users, MenuService) {
        var vm = this; // to bind view page
        MenuService.setActiveMenuMod('mct-mod', 'ACL');

        if (Group) { // modify page
            GroupDTO.initData(Group.groupList);
            vm.isUpdate = true;
        } else {
            GroupDTO.initData();
        }

        vm.GD = GroupDTO.data;
        vm.Users = Users.userList;
        vm.SelectedUsers = SelectedUsers ? SelectedUsers.userList : [];

        vm.submitForm = function(angObj) {
            if (angObj.$valid) {
                var url = vm.isUpdate ? 'system/groups/modify' : 'system/groups/create'
                GroupDTO.save('#createGroupForm', url, "/groups/list");
            }
        }
    }

})();