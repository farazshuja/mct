MCTApp.controller("TestingRequestCreateModifyController", ['$scope', '$http', '$location', '$routeParams', 'CommonRequestForm', 'ContentTypes', 'Environments', function ($scope, $http, $location, $routeParams, CommonRequestForm, ContentTypes, Environments) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.contentTypes = ContentTypes.contentTypes;
    $scope.environments = Environments.environments;
    $scope.firstContentType = $scope.contentTypes[0].name;

    $scope.crf = CommonRequestForm ? CommonRequestForm.commonRequestForm : null;
    if ($scope.crf == null) {
        $scope.crf = {};
        $scope.action = 'create';
        $scope.requestMethod = 'POST';
        $scope.environment = null;
        $scope.headers = [{}];
        $scope.specificRequestForms = [{}];
        $scope.specificRequestForms[0].requestType = $scope.firstContentType;
        $scope.specificRequestForms[0].responseType = $scope.firstContentType;
        $scope.specificRequestForms[0].testingVariables = [{}];
        $scope.specificRequestForms[0].dbValidationEntries = [{}];
        if(Environments.environments && Environments.environments.length > 0) {
            $scope.crf.environmentId = Environments.environments[0].id;
        }

    } else {
        $scope.action = 'modify';
        $scope.serviceName = $scope.crf.serviceName;
        $scope.requestMethod = $scope.crf.requestMethod;
        $scope.url = $scope.crf.url;
        $scope.headers = ($scope.crf.headers && $scope.crf.headers.length != 0) ? $scope.crf.headers : [{}];
        $scope.specificRequestForms = $scope.crf.specificRequestForms ? $scope.crf.specificRequestForms : [{}];
        for (var i = 0; i < $scope.specificRequestForms.length; i++) {
            $scope.specificRequestForms[i].testingVariables = ($scope.specificRequestForms[i].testingVariables && $scope.specificRequestForms[i].testingVariables.length != 0)
                ? $scope.specificRequestForms[i].testingVariables : [{}];
            $scope.specificRequestForms[i].dbValidationEntries = ($scope.specificRequestForms[i].dbValidationEntries && $scope.specificRequestForms[i].dbValidationEntries.length != 0)
                ? $scope.specificRequestForms[i].dbValidationEntries : [{}];
        }
    }

    $scope.$watch('crf.environmentId', function(newVal) {
        httpAjax($http, 'GET', 'devtool/functionalTesting/environments/getEnvironment/' + newVal, null, function (data) {
            $scope.environment = data.environment;
        });
    });

    if ($routeParams.specificRequestFormId) {
        for (var i = 0; i < $scope.specificRequestForms.length; i++) {
            if ($scope.specificRequestForms[i].id == $routeParams.specificRequestFormId) {
                $scope.activeTab = i;
                break;
            }
        }
    } else {
        $scope.activeTab = 0;
    }

    $scope.addRow = function (rows) {
        rows.push({})
    };

    $scope.deleteRow = function (index, rows) {
        rows.splice(index, 1);
        if (rows.length == 0) {
            rows.push({});
        }
    };

    $scope.deleteAllRows = function (rows) {
        rows.splice(0, rows.length);
        rows.push({});
    };

    $scope.addTab = function (tabs) {
        if(isAnimating)
            return; //do nothing

        tabs.push({
            disabled: false,
            requestType: $scope.firstContentType,
            responseType: $scope.firstContentType,
            testingVariables: [{}],
            dbValidationEntries: [{}]
        });
        $scope.activeTab = tabs.length - 1;

        setTimeout(function() {
            $scope.slideBack(true);
        },300);

    };

    $scope.changeTabStatus = function(index) {
        $scope.specificRequestForms[index].disabled = !$scope.specificRequestForms[index].disabled;
    };

    $scope.deleteTab = function (index, tabs) {
        if ($scope.activeTab > 0) {
            if (index == $scope.activeTab && index == tabs.length - 1 || index < $scope.activeTab) {
                $scope.activeTab = $scope.activeTab - 1;
            }
        }
        tabs.splice(index, 1);
        if (tabs.length == 0) {
            tabs.push({
                disabled: false,
                requestType: $scope.firstContentType,
                responseType: $scope.firstContentType,
                testingVariables: [{}],
                dbValidationEntries: [{}]
            });
        }
    };

    $scope.showTab = function (index) {
        $scope.activeTab = index;
    };

    $scope.rowsValid = function (rows) {
        var count = 0;
        for (var i = 0; i < rows.length; i++) {
            if (!rows[i].name && !rows[i].query || !rows[i].value) {
                count++;
            }
        }
        return count == 0;
    };

    $scope.showDeleteRowsModal = function (rows) {
        if (rows.length > 1) {
            $scope.rows = rows;
            $scope.showModal('#delete-all-rows-modal');
        } else {
            $scope.deleteAllRows(rows);
        }
    };

    $scope.confirmAllRowsDeletion = function () {
        $scope.deleteAllRows($scope.rows);
        $scope.hideModal();
    };

    $scope.submitForm = function () {
        $scope.page.isPageLoading = true;
        var url = 'devtool/functionalTesting/requests/' + $scope.action;
        httpAjax($http, 'POST', url, $("#createRequest").serialize(), function (data) {
            $scope.page.isPageLoading = false;
            $location.path("/devtool/functionalTesting");
            $.growl.message(data);
        })
    };

    //make the active tab into view
    var isAnimating = false;
    $scope.slideBack = function(isNewTabAdded) {
        if(isAnimating)
            return; //do nothing

        isAnimating = true;
        var conWidth = $('.tabs-wrapper').width() - 90;    //remove width of margin-left
        var cLeft = $('.tabs-have-nav').position().left;
        var tWidth = $('.mct-tabs li:eq(0)').width() + 10;  //including margin-right
        var tabsWidth = ($('.mct-tabs li').size() * tWidth) + cLeft;

        var noOfMoves = 1;
        if(tabsWidth > conWidth) {

            //scroll the new tab into view
            if(isNewTabAdded) {
                var $lastTab = $('.mct-tabs li:last');
                var index = $('.mct-tabs li').index($lastTab);
                var lastTabLeft = index * tWidth;
                var extraRight = lastTabLeft + cLeft;
                if(0 < extraRight && extraRight < conWidth) {
                    noOfMoves = 1;
                }
                else {
                    noOfMoves = Math.ceil((extraRight-conWidth)/tWidth);
                }


            }
            $('.tabs-have-nav').animate({
                'left': '-=' + (tWidth*noOfMoves) + 'px'
            }, 700, function() {
                isAnimating = false;
            });
        }
        else {
            isAnimating = false;
        }


    };

    $scope.slideForward = function() {
        if(isAnimating)
            return; //do nothing

        isAnimating = true;
        var cLeft = $('.tabs-have-nav').position().left;
        var tWidth = $('.mct-tabs li:eq(0)').width() + 10;  //including margin-right
        if(cLeft < 0) {
            $('.tabs-have-nav').animate({
                'left': '+=' + tWidth + 'px'
            }, 700, function() {
                isAnimating = false;
            });
        }
        else {
            isAnimating = false;
        }
    };

    $scope.bundle = {};
    $scope.showSaveVariableBundleModal = function (index) {
        $scope.tabId = index;
        $scope.bundle = null;
        $scope.showModal('#save-variable-bundle-modal');
        httpAjax($http, 'GET', 'devtool/functionalTesting/requests/variableBundles/list/', null, function (data) {
            $scope.variableBundles = data.variableBundles;
        });
    };

    $scope.confirmSaveVariableBundle = function () {
        var testingVariables = $scope.specificRequestForms[$scope.tabId].testingVariables;
        var variables = [];
        for (var i = 0; i < testingVariables.length; i++) {
            var params = $.param({
                name: testingVariables[i].name,
                value: testingVariables[i].value,
                sql: testingVariables[i].sql ? true : false
            });
            var mapping = '&testingVariables[' + i + '].';
            params = mapping + params.replace(/\&/g, mapping);
            variables.push(params);
        }
        var url = 'devtool/functionalTesting/requests/variableBundles/save';
        var variableParams = variables.join('&');
        var d = '';
        if ($scope.bundle.id) {
            d += 'id=' + $scope.bundle.id;
        }
        d += '&name=' + $scope.bundle.name + '&' + variableParams;
        $scope.showModal('.ajax-modal', function () {
            httpAjax($http, 'POST', url, d, function (data) {
                $scope.hideModal();
                $.growl.message(data);
            });
        });
    };

    $scope.showLoadBundleModal = function (index) {
        $scope.tabId = index;
        $scope.bundle = null;
        $scope.showModal('#load-variable-bundle-modal');
        httpAjax($http, 'GET', 'devtool/functionalTesting/requests/variableBundles/list/', null, function (data) {
            $scope.variableBundles = data.variableBundles;
        });
    };

    $scope.confirmLoadVariableBundle = function () {
        httpAjax($http, 'GET', 'devtool/functionalTesting/requests/variableBundles/getBundle/' + $scope.bundle.id, null, function (data) {
            var msg = "Bundle " + $scope.bundle.name + " was loaded successfully!";
            $.growl.message({success: msg});
            if (data.variableBundle.testingVariables && data.variableBundle.testingVariables.length > 0) {
                $scope.specificRequestForms[$scope.tabId].testingVariables = data.variableBundle.testingVariables;
            }
            $scope.hideModal();
        });
    };

    $scope.replicateTab = function (index) {
        if(isAnimating)
            return; //do nothing

        var tab = $scope.specificRequestForms[index];
        $scope.resetIds(tab.testingVariables);
        $scope.resetIds(tab.dbValidationEntries);
        $scope.specificRequestForms.push({
            disabled: tab.disabled,
            description: tab.description,
            requestType: tab.requestType,
            requestBody: tab.requestBody,
            responseType: tab.responseType,
            expectedResponseBody: tab.expectedResponseBody,
            timeout: tab.timeout,
            callCount: tab.callCount,
            testingVariables: tab.testingVariables,
            dbValidationEntries: tab.dbValidationEntries
        });
        $scope.activeTab = $scope.specificRequestForms.length - 1;

        setTimeout(function() {
            $scope.slideBack(true);
        },300);
    };

    $scope.resetIds = function(array) {
        for (var i = 0; i < array.length; i++) {
            array[i].id = null;
        }
    }
}]);