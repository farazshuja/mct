/**
 * Controller for List page of Data Sender
 * #/bidirectional/dataSender/list
 */
(function() {
    angular.module('mctApp').controller("DataSenderController", DataSenderController);
    DataSenderController.$inject = ['$scope', 'GridView'];

    function DataSenderController($scope, GridView){
        $scope.MenuService.setActiveMenuMod('ds-mod', 'Bi-directional');  //set the correct Menu

        //initialize a local variable with GridView Service
        $scope.Grid = GridView.init('bidirectional/dataSender/grid');

        $scope.enableDisableData = {};
        $scope.showEnableDisableModal = function(row) {
            $scope.enableDisableData = {
                id: row.id,
                name: row.name,
                title: row.enabled ? 'Disable' : 'Enable',
                url:  row.enabled ? 'bidirectional/dataSender/disable' : 'bidirectional/dataSender/enable'
            };
            $scope.showModal('.ajax-modal', function() {
                $scope.showModal('#enable-disable-modal');
            });
        };
        $scope.confirmEnableDisable = function(row) {
            $scope.showModal('.ajax-modal', function(){
                $scope.Grid.postForm(row.url, 'id=' + row.id, $scope);
            });
        };

    }

})();

/**
 * Controller for Create and Modify pages of Data Sender
 * #/bidirectional/dataSender/create
 * #/bidirectional/dataSender/modify/:id
 */
(function() {

    angular.module('mctApp').controller('DataSenderCreateController', DataSenderCreateController);
    DataSenderCreateController.$inject = ['$scope', 'DataSenderDTO', 'DataSender', 'StaticTypes', 'MVMTypes'];

    function DataSenderCreateController($scope, DataSenderDTO, DataSender, StaticTypes, MVMTypes) {

        $scope.MenuService.setActiveMenuMod('ds-mod', 'Bi-directional');  //set the correct Menu

        $scope.isUpdate = DataSender;
        if($scope.isUpdate) {    //modify page
            DataSenderDTO.loadData(DataSender.dataSender, MVMTypes.mvmTypes);
            $scope.mvmConfig = DataSenderDTO.data.mvmConfig;
        }
        else {
            DataSenderDTO.initData(MVMTypes.mvmTypes);
        }

        $scope.DTO = DataSenderDTO;
        $scope.DS = DataSenderDTO.data;
        $scope.StaticTypes = StaticTypes;
        $scope.MVMTypes = $scope.DS.mvmTypes;

        //fix keystoretypes to make None as top
        $scope.StaticTypes.keyStoreTypes = ArrangeKeyStoreTypes($scope.StaticTypes.keyStoreTypes);
        $scope.StaticTypes.secureProtocolTypes = keyValToArray($scope.StaticTypes.secureProtocolTypes);
        $scope.StaticTypes.httpMethods = ['POST', 'GET'];

        $scope.title = $scope.isUpdate ? 'Modify' : 'Create';


        $scope.zeroResultActionTypes = keyValToArray(StaticTypes.zeroResultActionTypes);
        $scope.messageTypeTransformation = keyValToArray(StaticTypes.transformationTypes);


        $scope.submitForm = function(angObj) {
            if(angObj.$valid) {
                $scope.page.isPageLoading = true;
                var modifyUrl = 'bidirectional/dataSender/' + ($scope.isUpdate ? 'modify' : 'create');
                DataSenderDTO.save('#createForm', modifyUrl);
            }
        };

        $scope.showNotification = function(chk) {
            if(chk)
                $scope.showModal('#notification-modal');
        };

        $scope.mediumChanged = function() {
            if($scope.DS.sendingMedium != 'XML_API') {
                $scope.DS.dataSenderOutGoingMessageConfigDto.configureOutgoingMessage = false;
            }
        };

        // arrange keystoretypes arrage, so that NONE is at top
        function ArrangeKeyStoreTypes(types) {
            delete types['NONE'];
            var keystoreTypes = [{'key': 'NONE', val: 'None'}];
            for(var i in types) {
                var obj = {
                    key: i,
                    val: types[i]
                };
                keystoreTypes.push(obj);
            }
            return keystoreTypes;
        }

    }

})();
