MCTApp.controller("InDataConnectorController", ['$scope', '$http', '$location', function($scope, $http, $location){
    $scope.MenuService.setActiveMenuMod('ds-mod', 'Bi-directional');  //set the correct Menu

    $scope.refreshGrid = function(){
        $scope.$broadcast ('gridRefreshEvent');
        $scope.updateGrid();
    };

    $scope.enableDisableData = {};
    $scope.showEnableDisableModal = function(row) {
        $scope.enableDisableData = {
            id: row.id,
            name: row.name,
            title: row.enabled ? 'Disable' : 'Enable',
            url:  row.enabled ? 'bidirectional/idc/disable' : 'bidirectional/idc/enable'
        }
        $scope.showModal('#enable-disable-modal');
    };
    $scope.confirmEnableDisable = function(row) {
        $scope.showModal('.ajax-modal', function(){
            postFormGrid($scope, $http, row.url, 'id=' + row.id);
        });
    };
    $scope.showDeleteModal = function(row) {
        $scope.idcNameToDelete = row.name;
        $scope.idcIdToDelete = row.id;
        $scope.showModal('#delete-idc-modal');
    };
    $scope.confirmDeletion = function() {
        postFormGrid($scope, $http, 'bidirectional/idc/delete', 'id=' + $scope.idcIdToDelete);
    };

    $scope.OEM = {};
    $scope.submitForm = function(angObj) {
        if(angObj.$valid) {
            $scope.page.isPageLoading = true;
            httpAjax($http, 'POST', 'metaData/dataConsumers/create', $("#createForm").serialize(), function(data) {
                $location.path("/metaData/dataSources/dataConsumers/list");
                $.growl.message(data);
            });
        }
    }
}]);

MCTApp.controller("InDataConnectorCreateModifyController", ['$scope', '$resource', '$http', '$location', '$filter', 'InDataConnector', 'InMessageTypes', 'StaticTypes', 'DataConsumers', 'OutDataConnectors', function($scope, $resource, $http, $location, $filter, InDataConnector, InMessageTypes, StaticTypes, DataConsumers, OutDataConnectors) {
    $scope.MenuService.setActiveMenuMod('ds-mod', 'Bi-directional');  //set the correct Menu

    $scope.IDC = InDataConnector ? InDataConnector.idc : null;

    var serviceURLstatic = window.location.origin + window.location.pathname + 'services/';
    serviceURLstatic = serviceURLstatic.indexOf('trilogy') != -1 ? serviceURLstatic.replace(window.location.pathname, '/') : serviceURLstatic;
    $scope.serviceURLStatic = serviceURLstatic;


    $scope.selectODC = function(odcId, index) {
        if (odcId != null && odcId != -1) {
            var url = 'bidirectional/odc/getOutMsgTypeList?odcId=' + odcId;

            httpAjax($http, 'GET', url, null, function(data, status, headers, config) {
                $scope.messageTypeObjects[index].chainedMessage = data.outMsgTypeList;

            });
        }
    }

    var fakeODC = {id:-1, name:"None", enabled:true};
    var tempOdcList = [fakeODC];
    $scope.inMessageTypeList = InMessageTypes.inMessageTypeList;
    //a specif object to deal with generated content from Message Type List
    $scope.messageTypeObjects = [];

    for(var i=0;i<$scope.inMessageTypeList.length;i++) {
        $scope.messageTypeObjects.push({});
        $scope.messageTypeObjects[i].actionTypeList = createActionTypeArray(StaticTypes.actionTypes);
        $scope.messageTypeObjects[i].dataConsumerList = angular.copy(DataConsumers.dataConsumerList);
        var odc = tempOdcList.concat(OutDataConnectors.odcList);
        $scope.messageTypeObjects[i].odcList = angular.copy(odc);
        $scope.messageTypeObjects[i].chainedMessage = [];

        //prepare selected items
        if($scope.IDC) {
            var foundIndex = $filter('searchInObject')($scope.IDC.messageTypeMapping, 'inMessageTypeId', $scope.inMessageTypeList[i].id, true);
            if(typeof foundIndex != 'object') {
                var found = $scope.IDC.messageTypeMapping[foundIndex];
                $scope.selectODC(found.outDataConnectorId,i);
                //re-arrange the order correctly as in inMessageTypeList
                var ele = $scope.IDC.messageTypeMapping.splice(foundIndex,1);
                $scope.IDC.messageTypeMapping.splice(i,0, ele[0]);
            }
            else {
                $scope.IDC.messageTypeMapping.splice(i,0, {});
            }
        }

    }

    $scope.some = {firsts: [], seconds: []};

    $scope.checkTheCorrectBox = function(outerArray, innerArray, $filter, isMapped) {
        angular.forEach(outerArray, function(value, key) {
            var found = $filter('searchInObject')(innerArray, 'inMessageTypeId', value.id);
            if(found) {
                if(isMapped)
                    value.checked = found.checked ? found.checked : false;
                else
                    value.checked = true;
            }
        });
    }

    if ($scope.IDC) { //if edit mode
        $scope.originalMsg = $scope.IDC.inMessageTypeDetermExpression;
        $scope.checkTheCorrectBox($scope.inMessageTypeList, $scope.IDC.messageTypeMapping, $filter);
        $scope.IMTValid = validateCheckboxes($scope.inMessageTypeList);
        //initialize odc here
        for(var i=0;i<$scope.IDC.messageTypeMapping.length;i++) {
            var mtm = $scope.IDC.messageTypeMapping[i];
            $scope.some.firsts[i] = mtm.outDataConnectorId || -1;
        }


    } else {
        $scope.IMTValid = false;
    }

    $scope.inMsgTypeSelectionChanged = function(chk) {
        if (chk === false) {
            $scope.IMTValid = true;
        } else {
            $scope.IMTValid = validateCheckboxes($scope.inMessageTypeList);
        }
    };

    $scope.restoreMsg = function() {
        $scope.IDC.inMessageTypeDetermExpression = $scope.originalMsg;
        $scope.createForm.inMessageTypeDetermExpression.$setPristine(true);
    }

    $scope.submitForm = function(angObj) {
        var url = 'bidirectional/idc/' + ($scope.IDC ? 'modify' : 'create');

        if(angObj.$valid) {
            $scope.page.isPageLoading = true;
            //convert disabled options to enabled
            $('select option[disabled=disabled]').removeAttr('disabled');
            $('.panel.ng-hide').remove();   //remove all hidden panels, to avoid submission
            httpAjax($http, 'POST', url, $("#createForm").serialize(), function(data) {
                $location.path("/bidirectional/idc/list");
                $.growl.message(data);
            });
        }
    }

    function createActionTypeArray(obj) {
        var at = [];
        for(o in obj) {
            var c = {};
            c.key = o;
            c.val = obj[o];
            at.push(c);
        }
        return at;
    }

    function validateCheckboxes(msgTypeList) {
        var isValid = false;
        for (var i=0; i<msgTypeList.length; i++) {
            var o = msgTypeList[i];
            if (o.checked) {
                isValid = true;
            }
        }
        return isValid;
    }

}]);
