/**
 * Controller for List page of Data Privder
 * #/metaData/dataProviders/list
 */
(function() {
    angular.module('mctApp').controller('DataProvidersController', DataProvidersController);
    DataProvidersController.$inject = ["$scope", "$http", "GridView"];
    function DataProvidersController($scope, $http, GridView) {
        $scope.MenuService.setActiveMenuMod('ds-mod', 'Data Providers');  //set the correct Menu

        //initialize a local variable with GridView Service
        $scope.Grid = GridView.init('metaData/dataProviders/grid');

        $scope.enableDisableData = {};
        $scope.showEnableDisableModal = function(row) {
            $scope.enableDisableData = {
                id: row.id,
                name: row.name,
                title: row.enabled ? 'Disable' : 'Enable',
                url:  row.enabled ? 'metaData/dataProviders/disable' : 'metaData/dataProviders/enable'
            };
            $scope.showModal('.ajax-modal', function() {
                if(row.enabled) {
                    httpAjax($http, 'GET', 'metaData/dataProviders/getMappedConsumerNames/' + row.id, null, function(data) {
                        $scope.enableDisableData.hasDisabledProviders = data.mappedConsumerNames.join(', ');
                        $scope.showModal('#enable-disable-modal');
                    });
                }
                else {
                    $scope.enableDisableData.hasDisabledProviders = null;
                    $scope.showModal('#enable-disable-modal');
                }
            });

        };
        $scope.confirmEnableDisable = function(row) {
            $scope.showModal('.ajax-modal', function(){
                $scope.Grid.postForm(row.url, 'id=' + row.id, $scope);
            });
        };

    }
})();


/**
 * Controller for create and modify pages of Data Providers
 * #/metaData/dataProviders/create
 * #/metaData/dataProviders/modify/:id
 */
(function() {
    angular.module('mctApp').controller('DataProvidersEditController', DataProvidersEditController);
    DataProvidersEditController.$inject = ['$scope', '$http', '$filter', 'StaticTypes', 'OEMLeadCategoryMappingList', 'DataProvider', 'CrmCodes', 'eventCategories', 'CRMUsers', 'MVMTypes', 'MVMConfig', 'DPDTO'];

    function DataProvidersEditController($scope, $http, $filter, StaticTypes, OEMLeadCategoryMappingList, DataProvider, CrmCodes, eventCategories, CRMUsers, MVMTypes, MVMConfig, DPDTO) {
        $scope.MenuService.setActiveMenuMod('ds-mod', 'Data Providers');  //set the correct Menu

        $scope.DTO = DPDTO;
        if(DataProvider) {
            DPDTO.initData(CrmCodes.crmCodesList, OEMLeadCategoryMappingList.oemLeadCategoryMappingList, DataProvider);
            $scope.isUpdate = true;
        }
        else {
            DPDTO.initData(CrmCodes.crmCodesList, OEMLeadCategoryMappingList.oemLeadCategoryMappingList);
        }

        $scope.DP = DPDTO.data;


        CRMUsers.userList.unshift({loginId: 0, loginName: "-- N/A --"});
        $scope.CRMUsers = CRMUsers.userList;

        //convert to array and sort in ASC order
        StaticTypes.dataProviderTypes = keyValToArray(StaticTypes.dataProviderTypes);
        StaticTypes.dataProviderMediumTypes = keyValToArray(StaticTypes.dataProviderMediumTypes);
        StaticTypes.ftpTypes = keyValToArray(StaticTypes.ftpTypes);
        StaticTypes.fileTypes = keyValToArray(StaticTypes.fileTypes);
        StaticTypes.ftpConnectionModes = keyValToArray(StaticTypes.ftpConnectionModes);
        StaticTypes.ftpSecureProtocols = keyValToArray(StaticTypes.ftpSecureProtocols);

        $scope.StaticTypes = StaticTypes;
        $scope.OEMLeadCategoryMappingList = OEMLeadCategoryMappingList.oemLeadCategoryMappingList;

        $scope.CrmCodes = CrmCodes.crmCodesList;

        $scope.eventCategories = eventCategories;


        //MVM related properties
        if($scope.isUpdate) {
            var defaultConfig = angular.copy(MVMConfig.mvmConfig);
            $scope.mvmConfig = $.extend(true, defaultConfig, $scope.DP.mvmConfig);
            MVMConfig.parseCoefficientToDays($scope.mvmConfig);
            //handle enable/disable of configuration dropdown
            MVMConfig.handleConfigurationDropDownState($scope.mvmConfig);
        }
        else {
            $scope.mvmConfig = angular.copy(MVMConfig.mvmConfig);
        }

        $scope.MVMConfig = MVMConfig;
        $scope.mvmTypes = MVMTypes.mvmTypes;

        $scope.addNewMapping = function() {
            $scope.DP.dpCRMCodesDtos.push({});
        };

        //show hide host section based on ProviderMediums
        $scope.$watch('DP.providerMediumType', function(newVal) {
            $scope.DTO.setFormMediumTypeComponents(newVal);
        });

        $scope.$watch('DP.type', function(newVal, oldVal) {
            $scope.DTO.providerTypeIsLead = newVal == 'LEAD';
            if($scope.isUpdate) {
                //reset all checkboxes
                if(newVal == 'LEAD') {
                    angular.forEach($scope.OEMLeadCategoryMappingList, function(o,k) {
                        if(o.leadCategories.length == 0) {
                            o.checked = false;
                        }
                    });
                }
            }
        });

        $scope.submitForm = function(angObj) {
            if(angObj.$valid) {
                var url = 'metaData/dataProviders/' + ($scope.isUpdate ? 'modify' : 'create');
                $scope.page.isPageLoading = true;
                $scope.DTO.save('#createForm', url, "/metaData/dataProviders/list");
            }
        };


        $scope.isDefined = function (obj) {
            return angular.isDefined(obj);
        }

        $scope.loadDP = function() {
            //load only first time, then cache it
            if(!$scope.dpList) {
                //load template via ajax call
                var url = 'metaData/dataProviders/getDataPrvidersList';
                $scope.showModal('.ajax-modal', function(){
                    $http.get(url).then(function success(response) {
                        $scope.dpList = response.data.dataProviderList;
                        $scope.dataProviderID = $scope.dpList[0].id;
                        $scope.showModal('#load-dp-modal');
                    }, function error(response) {
                        $.growl.message(response.data);
                        $scope.hideModal();
                    });
                });
            }
            else {
                $scope.showModal('#load-dp-modal');
            }

        };

        $scope.confirmLoadDP = function() {
            var found = $filter('searchInObject')($scope.dpList, 'id', $scope.dataProviderID);
            if(found) {
                DPDTO.applyData(found);
                $scope.DP = DPDTO.data;

                //reset MVM properties
                var defaultConfig = angular.copy(MVMConfig.mvmConfig);
                $scope.mvmConfig = $.extend(true, defaultConfig, $scope.DP.mvmConfig);
                MVMConfig.parseCoefficientToDays($scope.mvmConfig);
                //handle enable/disable of configuration dropdown
                MVMConfig.handleConfigurationDropDownState($scope.mvmConfig);

                $scope.hideModal();
            }
        }
    }
})();