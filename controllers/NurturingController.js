MCTApp.controller("NurturingController", ['$scope', '$resource', '$http', function($scope, $resource, $http){
    $scope.MenuService.setActiveMenuMod('ds-mod', 'Nurturing Programs');  //set the correct Menu

    $scope.refreshGrid = function(){
        $scope.$broadcast ('gridRefreshEvent');
        $scope.updateGrid();
    };

    $scope.enableDisableData = {};
    $scope.showEnableDisableModal = function(row) {
        $scope.enableDisableData = {
            id: row.id,
            name: row.name,
            title: row.enabled ? 'Disable' : 'Enable',
            url:  row.enabled ? 'metaData/nurturing/disable' : 'metaData/nurturing/enable'
        }
        $scope.showModal('.ajax-modal', function() {
            if(row.enabled) {
                httpAjax($http, 'GET', 'metaData/dataProviders/getMappedConsumerNames/' + row.id, null, function(data) {
                    $scope.enableDisableData.hasDisabledProviders = data.mappedConsumerNames.join(', ');
                    $scope.showModal('#enable-disable-modal');
                });
            }
            else {
                $scope.enableDisableData.hasDisabledProviders = null;
                $scope.showModal('#enable-disable-modal');
            }
        });

    };
    $scope.confirmEnableDisable = function(row) {
        $scope.showModal('.ajax-modal', function(){
            postFormGrid($scope, $http, row.url, 'id=' + row.id);
        });
    };

    $scope.getCommaSeparated = function(row, obj) {
        var list = [];
        for(var i=0;i<row[obj].length;i++) {
            var o = row[obj][i];
            list.push(o.name);
        }
        return list.join(', ');
    }
}]);

MCTApp.controller("NurturingCreateController", ['$scope', '$resource', '$http', '$location', '$filter', 'StaticTypes', 'NurturingProgram', function($scope, $resource, $http, $location, $filter, StaticTypes, NurturingProgram) {
    $scope.MenuService.setActiveMenuMod('ds-mod', 'Nurturing Programs');  //set the correct Menu

    $scope.StaticTypes = StaticTypes;
    $scope.NP = NurturingProgram ? NurturingProgram.nurturingProgram : null;

    //track the nurture dealers and there ppids
    $scope.nDealers = [];
    $scope.nurturingCriteria = [];    // to show the program criteria in popup window
    $scope.programCriteriaPID = 1;  //t show the PPID in popup

    //initialize the validation variables for OEM, Make, Lead Category and Type, Dealers
    $scope.isOEMValid = true;
    $scope.isMakeValid = true;
    $scope.isLCValid = true;
    $scope.isLTValid = true;
    $scope.isDealersValid = true;
    $scope.isLeadnTypeLoading = false;

    var dealers = [];
    //if edit mode
    if($scope.NP) {

        checkTheCorrectBox($scope.StaticTypes.oems, $scope.NP.oems, $filter);
        checkTheCorrectBox($scope.StaticTypes.makes, $scope.NP.makes, $filter);

        $scope.leadCategories = $scope.NP.leadCategories;
        $scope.leadTypes = $scope.NP.leadTypes;

        //go through makes and if it has value -1 select Any
        if($scope.NP.makes[0].id == -1) {
            $scope.makeAny = true;
        }

        $scope.NurtureDealers = [];
        var tempDealers = [];
        //Nurture For Dealers Obj
        for(var i=0;i<$scope.NP.dealers.length;i++) {
            var obj = $scope.NP.dealers[i];
            tempDealers[obj.dlCode] = tempDealers[obj.dlCode] ? tempDealers[obj.dlCode] : [];
            tempDealers[obj.dlCode].push({
                id: obj.productProgramId,
                name: obj.productProgramId,
                __id: obj.id    //to be used in case of modify
            });
        }

        var preData = [];
        for(var i in tempDealers) {
            $scope.nDealers.push({
                dlCode: i,
                productProgramIds: tempDealers[i]
            });
            preData.push({'id': i, 'name': i}); //prepare dealers
        };

        onOEMChanged($scope, $http, dealers, true, $filter, preData);
        resetTokenInput($scope, $http, dealers, $filter, preData);
        //check select all if all of the makes are selected
        if ($scope.StaticTypes.makes.length == $scope.NP.makes.length) {
            $scope.selectAllMake = true;
        }
    }
    else {
        $scope.deliveryMethod = 'API';
        $scope.inventoryOption = 'ANY';
        $scope.apiDeliveryDetails = {
            type: 'GATED'
        };
        $scope.leadCategories = null;
        $scope.leadTypes = null;
        resetTokenInput($scope, $http, dealers, $filter);
    }

    $scope.OEMChanged = function() {
        onOEMChanged($scope, $http, dealers, false, $filter);
    };

    $scope.submitForm = function(angObj) {
        //var dealers = $('#nurturing-dealers').tokenInput('get');
        var dealers = $scope.nDealers;
        var dlCodesStr = '';
        if(dealers.length > 0) {
            var index = 0;
            for(var i = 0;i<dealers.length;i++){
                var obj = dealers[i];
                for(var j=0;j<obj.productProgramIds.length;j++){
                    if(obj.productProgramIds[j].__id) {
                        dlCodesStr += '&dealers[' + index + '].id=' + obj.productProgramIds[j].__id;
                    }
                    dlCodesStr += '&dealers[' + index + '].dlCode=' + obj.dlCode;
                    dlCodesStr += '&dealers[' + index + '].productProgramId=' + obj.productProgramIds[j].name;
                    index++;
                }
            }
        }
        var url = 'metaData/nurturing/' + ($scope.NP ? 'modify' : 'create');

        if(angObj.$valid) {
            $scope.page.isPageLoading = true;
            httpAjax($http, 'POST', url, $("#createForm").serialize() + dlCodesStr, function(data) {
                $location.path("/metaData/nurturing/list");
                $.growl.message(data);
            });
        }
    }

    $scope.onMakeChanged = function(that) {
        if(that){
            if(that.makeAny) {
                for(var i=0;i<$scope.StaticTypes.makes.length;i++) {
                    $scope.StaticTypes.makes[i].checked = false;
                }
                $scope.selectAllMake = false;
            }
            else if(that.checked) {
                $scope.makeAny = false;
            }
        }
        $scope.isMakeValid = $('input:checkbox[name=makeIds]:checked').size() > 0;
    }
    $scope.onSelectAllMake = function(that) {
        if(!that)
            return;

        $scope.makeAny = false;
        for(var i=0;i<$scope.StaticTypes.makes.length;i++) {
            $scope.StaticTypes.makes[i].checked = that.selectAllMake;
        }
    }
    $scope.onLCChanged = function () {
        $scope.isLCValid = $('input:checkbox[name=leadCategoryIds]:checked').size() > 0;
    }
    $scope.onLTChanged = function()  {
        $scope.isLTValid = $('input:checkbox[name=leadTypeIds]:checked').size() > 0;
    }

    //validate OEM, Makes, Lead Categories, Types and Dealers
    $scope.isFormValid = function() {
        $scope.isOEMValid = $('input:checkbox[name=oemIds]:checked').size() > 0;
        $scope.onMakeChanged();
        $scope.onLCChanged();
        $scope.onLTChanged();
        if(($(".token-input-list").size())){
            checkDealersValidation($scope);
        }
        else {
            $scope.isDealersValid = true;
        }
        return $scope.isOEMValid && $scope.isMakeValid && $scope.isLCValid && $scope.isLTValid && $scope.isDealersValid;
    }

    $scope.getPossibleVariables = function(msg,isJson) {
        var messages = [];
        angular.forEach($scope.StaticTypes.variableList, function(v, k) {
            if(v.messageVariable[msg]){
            	var displayVariable = isJson ? (v.messageVariable.variable).replace(/\$|{|}/g,'') : v.messageVariable.variable ;
                if(isJson)
                {
                	if(displayVariable.indexOf("lead.")>=0)
                	{
                		messages.push(displayVariable);
                	}
                }
                else
                {
                	messages.push(displayVariable);
                }
            }
        });
        return messages.join(', ');
    };

}]);

function resetTokenInput($scope, $http, d, $filter, preData) {
    //if the list has already some data then merge it to preData
    var currentData = [];
    var $tokenInput = $('#nurturing-dealers');
    var $list = $tokenInput.parent().children(".token-input-list");
    //preserve the list of dealers only if its in data
    if($list.size() > 0) {
        currentData = $tokenInput.tokenInput('get');
        preData = $.extend(currentData, preData);
        for(var i=0;i<preData.length;i++) {
            var found = $filter('searchInObject')(d, 'id', preData[i].id);
            if(!found) {
                preData.splice(i,1);
            }
        }

    }

    $list.remove();
    $tokenInput.tokenInput([],{
        prePopulate: preData ? preData : [],
        preventDuplicates: true,
        disableKeyboard: true,
        onResult: function(item) {
            if($.isEmptyObject(item)){
                var v = $("tester").text();
                var pat = /^[a-zA-Z0-9]*$/;
                if(pat.test(v)) {
                    return [{id:v,name: v}];
                }
                return null;
            }else{
                return item;
            };
        },
        onAdd: function(item) {
            $scope.nDealers.push({
                dlCode: item.id
            });
            checkDealersValidation($scope);
            $scope.$apply();
        },
        onSelect: function(obj) {
            resetPPIDTokens($scope, $http, obj);
        },
        onDeselect: function() {
            clearOutPPIDTokens();
        },
        beforeDelete: function(token, delMethod) {
            var that = this;
            $('#delete-dlCode-modal strong').html('Dealer Code: ' + token.name);
            $scope.showModal('#delete-dlCode-modal');
            $('#delete-dlCode-modal #okButton').unbind('click')
            $('#delete-dlCode-modal #okButton').bind('click', function() {
                delMethod();
                $scope.hideModal();
                clearOutPPIDTokens(token, $scope);
                checkDealersValidation($scope);
                $scope.$apply();
            });

        }
    });
    //show placeholder and msg to select some dealer first
    clearOutPPIDTokens();
}
function resetPPIDTokens($scope, $http, byDealer) {
    var $ppidInput = $('#nurturing-dealers-ppid');
    var $ppidList = $ppidInput.parent().children(".token-input-list");
    $ppidList.remove();
    var preData = getPPIDs($scope, byDealer);
    $ppidInput.tokenInput([],{
        prePopulate: preData ? preData : [],
        hintText: "Type in some ppid",
        disableKeyboard: true,
        onResult: function(item) {
            if($.isEmptyObject(item)){
                var v = $("tester").text();
                if(isNaN(v)) {
                    return null;
                }
                return [{id:v,name: v}];
            }else{
                return item;
            };
        },
        onAdd: function(token) {
            fillInDealersData($scope, byDealer, token, true);
            $scope.$apply();
        },
        onSelect: function(obj) {
            var dlCode = $('.col-xs-6-first .token-input-selected-token p').text();
            var pId = obj.id;
            var nurtureId = $('input:hidden[name=id]').val();
            if(!nurtureId){
                return; //do not show the popup on create page
            }
            var url = 'metaData/nurturing/getNurturingCriteriaForDealerProgram/' + nurtureId + '/' + dlCode + '/' + pId;
            //show tooltip
            $scope.showModal('.ajax-modal', function() {
                httpAjax($http, 'GET', url, null, function(data, status, headers, config) {
                    $scope.nurturingCriteria = data.nurturingCriteria;
                    $scope.programCriteriaPID = pId;
                    $scope.showModal('#nurturing-criteria-modal');
                });
            });

        },
        beforeDelete: function(token, delMethod) {
            var that = this;
            $('#delete-ppid-modal strong').html('Program Id: ' + token.name);
            $scope.showModal('#delete-ppid-modal');
            $('#delete-ppid-modal #okButton1').unbind('click')
            $('#delete-ppid-modal #okButton1').bind('click', function() {
                delMethod();
                $scope.hideModal();
                //var data = $(that).tokenInput('get');
                fillInDealersData($scope, byDealer, token);
                $scope.$apply();
            });

        }
    });
}
//loop through nDealers add data or update data if required
function fillInDealersData($scope, byDealer, data, isAdd) {
    var isUpdated = false;
    for(var i=0;i<$scope.nDealers.length;i++) {
        var obj = $scope.nDealers[i];
        if(obj.dlCode == byDealer.id) {
            //remove the token
            if(isAdd) {
                obj.productProgramIds = obj.productProgramIds ? obj.productProgramIds : [];
                obj.productProgramIds.push(data);
            }
            else {
                for(var x=0;x<obj.productProgramIds.length;x++) {
                    if(obj.productProgramIds[x].id == data.id) {
                        obj.productProgramIds.splice(x,1);
                        break;
                    }

                }
            }
            isUpdated = true;
            break;
        }
    }
    if(!isUpdated) {
        $scope.nDealers.push({
            dlCode: byDealer.id,
            productProgramIds: data
        })
    }
    checkDealersValidation($scope);

}
function getPPIDs($scope, byDealer) {
    var preData = [];
    for(var i=0;i<$scope.nDealers.length;i++) {
        var obj = $scope.nDealers[i];
        if(obj.dlCode == byDealer.id) {
            preData = obj.productProgramIds;
            break;
        }
    }
    return preData;
}
function checkDealersValidation($scope) {
    var isValid = true;
    for(var i=0;i<$scope.nDealers.length;i++) {
        var obj = $scope.nDealers[i];
        if(!obj.productProgramIds || obj.productProgramIds.length == 0) {
            $scope.isDealersValid = false;
            return;
        }
    }
    
    if($scope.isDealersValid == false){
        $scope.isDealersValid = true;
    }
}
//clear out the PPID Token and save it for the current dealer if any was selected before
function clearOutPPIDTokens(item, $scope) {
    var $ppidInput = $('#nurturing-dealers-ppid');
    var $ppidList = $ppidInput.parent().children(".token-input-list");
    $ppidList.remove();
    $ppidInput.tokenInput([],{
        hintText: "Please select a dealer first"
    });
    if(item) {
        for(var i=0;i<$scope.nDealers.length;i++) {
            var obj = $scope.nDealers[i];
            if(obj.dlCode == item.id) {
                $scope.nDealers.splice(i,1);
                break;
            }
        }
    }
}

function onOEMChanged($scope, $http, dealers, applyLeadnTypes, $filter, preData) {
    $scope.isLeadnTypeLoading = true;
    var d = [];
    angular.forEach($scope.StaticTypes.oems, function(value, key) {
        if(value.checked) {
            d.push(value.id);
        }
    });

    if(d.length == 0) {
        $scope.isOEMValid = false;
        $scope.isLeadnTypeLoading = false;
        $scope.leadCategories = null;
        $scope.leadTypes = null;
        $(".token-input-list").remove();
        return;
    }
    $scope.isOEMValid = true;


    d = '?oemIds=' + d.join('&oemIds=');
    var url = 'metaData/nurturing/categoryAndTypes' + d;
    httpAjax($http, 'GET', url, null, function(data, status, headers, config) {
        if(applyLeadnTypes) {
            checkTheCorrectBox(data.leadCategories, $scope.NP.leadCategories, $filter);
            checkTheCorrectBox(data.leadTypes, $scope.NP.leadTypes, $filter);
        }
        else if($scope.leadCategories && $scope.leadTypes) {
            checkTheCorrectBox(data.leadCategories, $scope.leadCategories, $filter, true);
            checkTheCorrectBox(data.leadTypes, $scope.leadTypes, $filter, true);   //persist the selection of checkboxes
        }

        $scope.leadCategories = data.leadCategories.length > 0 ? data.leadCategories : null;
        $scope.leadTypes = data.leadTypes.length > 0 ? data.leadTypes : null;
        $scope.isLeadnTypeLoading = false;


    }, function(data, status, headers, config) {
        $scope.isLeadnTypeLoading = false;
    });
}
function checkTheCorrectBox(outerArray, innerArray, $filter, isMapped) {
    angular.forEach(outerArray, function(value, key) {
        var found = $filter('searchInObject')(innerArray, 'id', value.id);
        if(found) {
            if(isMapped)
                value.checked = found.checked ? found.checked : false;
            else
                value.checked = true;
        }
    });
}