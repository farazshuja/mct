MCTApp.controller("MetaDataController", ['$scope', '$resource', '$http', function($scope, $resource, $http){
    $scope.MenuService.setActiveMenuMod('ds-mod', 'Meta Data');  //set the correct Menu

    // Related to Enable/Disable Modal
    $scope.enableDisableData = {};
    $scope.showEnableDisableModal = function(row, toMethod) {
        $scope.enableDisableData = {
            id: row.id,
            name: row.name,
            title: row.state == 'ENABLED' ? 'Disable' : 'Enable',
            url:  'metaData/dataSources/metaDataConfig/' + toMethod,
            formData: toMethod == 'leadCategory' ? 'leadCategoryId=' : 'leadTypeId=',
            gridToRefresh: toMethod == 'leadCategory' ? '#leadCategoriesGrid' : '#leadTypesGrid'
        }
        $scope.showModal('#enable-disable-modal');

    };
    $scope.confirmEnableDisable = function(row) {
        row.url += row.title == 'Disable' ? '/disable' : '/enable';
        $scope.showModal('.ajax-modal', function(){
            postFormGrid($scope, $http, row.url, row.formData + row.id, row.gridToRefresh);
        });
    };

    //load list of OEMs in modal
    $scope.oemList = null;
    $resource('metaData/OEM/getOemList').get().$promise.then(function(data) {
        $scope.oemList = data.oemList;
    });

    $scope.categoryUrl = 'checkLeadCategoryName';

    $scope.showTab = function($event, selTabPane) {
        var $elem = angular.element($event.currentTarget);
        $elem.parent().parent().find('a').removeClass('active');
        $elem.addClass('active');
        var $tab = angular.element(selTabPane);
        $tab.parent().children().removeClass('active');
        $tab.addClass('active');
        //refresh the current grid
        if(selTabPane == '#leadCategoriesTab'){
            angular.element('#leadCategoriesGrid').scope().updateGrid();
            $scope.categoryUrl = 'checkLeadCategoryName';

        }
        else if(selTabPane == '#leadTypesTab') {
            angular.element('#leadTypesGrid').scope().updateGrid();
            $scope.categoryUrl = 'checkLeadTypeName';
        }
        clearModalForm();
    };

    $scope.confirmCreateUpdate = function() {
        var str = $('#createUpdateForm').serialize();
        var toMethod =  'createLeadCategory';
        var gridToRefresh = '#leadCategoriesGrid';
        if($scope.Modal.title != 'Category') {
            toMethod = 'createLeadType';
            gridToRefresh = '#leadTypesGrid';
        }
        if($scope.Modal.state != 'Create') {
            toMethod = toMethod.replace('create', 'modify');
        }

        postFormGrid($scope, $http, 'metaData/dataSources/metaDataConfig/' + toMethod, str, gridToRefresh);
    };

    //track modal title i.e. Cateogry or Lead Type
    $scope.Modal = {};
    $scope.showCreateUpdateModal = function(selector, title, state) {

        //clear form and reset it
        if(state == 'Create') {
           clearModalForm();
        }

        $scope.Modal.title = title;
        $scope.Modal.state = state; // create or update
        $scope.showModal(selector);
    }
    $scope.showModifyRowModal = function(row, title, state) {
        clearModalForm();
        //update create/update category modal
        $scope.categoryName = row.name;
        $scope.categoryNameOrg = row.name;      //re-set original value
        $scope.categoryId = row.id;
        var oemNames = row.oemNames.split(', ');
        for(var i=0;i<$scope.oemList.length;i++) {
            var oem = $scope.oemList[i];
            if(angular.isDefined(row.oems)){
                for(var y=0;y<row.oems.length;y++){
                    var rowOEM = row.oems[y];
                    if(oem.checked = oem.name == rowOEM.name){
                        oem.usedMapping = rowOEM.usedMapping;
                        break;
                    }

                }
            }
        }
        $scope.showCreateUpdateModal('#create-update-modal', title, state);
    };

    function clearModalForm() {
        $scope.createUpdateForm.$setPristine();
        angular.forEach($scope.oemList, function(value, key) {
            value.checked = false;
            value.usedMapping = false;
        });
        $scope.categoryName = '';
        $scope.categoryNameOrg = '';
        $scope.categoryId = '';

    }
    
    $scope.cancelModal = function() {
    	$scope.hideModal();
    	$scope.categoryName = '';
    };
    
}]);