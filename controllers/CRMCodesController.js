MCTApp.controller('CRMCodesController', [ "$scope", "$http", 'eventCategories', function($scope, $http, eventCategories) {
        $scope.MenuService.setActiveMenuMod('ds-mod', 'CRM Codes'); // set the correct Menu
        $scope.eventCategories = eventCategories;
        $scope.refreshGrid = function() {
            $scope.$broadcast('gridRefreshEvent');
            $scope.updateGrid();
        };
    }
]);

MCTApp.controller('CRMCodesCreateController', [ "$scope", "$http", "$location", 'eventCategories',
    function($scope, $http, $location, eventCategories) {
        $scope.MenuService.setActiveMenuMod('ds-mod', 'CRM Codes'); // set the correct Menu
        $scope.eventCategories = eventCategories;

        // set default values
        $scope.eventCategory = 'DISPOSITION';

        $scope.submitForm = function(angObj) {
            if (angObj.$valid) {
                $scope.page.isPageLoading = true;
                validateAndSave();
            }
        };

        function validateAndSave() {
            $http({
                method : 'GET',
                url : 'metaData/crmCodes/isCRMCodeUnique/',
                params : {'messageType' : $scope.messageType, 'code' : $scope.code, 'eventCategory' : $scope.eventCategory}
            }).then(function(response) {
                if (response.data) {
                    save();
                } else {
                    notUnique();
                }
            }, function() {
                notUnique();
            });
        }

        function save() {
            httpAjax($http, 'POST', 'metaData/crmCodes/create', $("#createForm").serialize(), function(data) {
                $location.path("/metaData/crmCodes/list");
                $.growl.message(data);
            });
        }

        function notUnique() {
            $scope.page.isPageLoading = false;
            $.growl.message({error: 'CRM codes is not unique'});
        }

    }
]);

MCTApp.controller('CRMCodesModifyController', [ "$scope", "$http", "$location", 'eventCategories', 'crmCode',
    function($scope, $http, $location, eventCategories, crmCode) {
        $scope.MenuService.setActiveMenuMod('ds-mod', 'CRM Codes'); // set the correct Menu
        $scope.eventCategories = eventCategories;
        $scope.CRMCODE = crmCode;

        $scope.submitForm = function(angObj) {
            if (angObj.$valid) {
                $scope.page.isPageLoading = true;
                $('input[disabled=disabled], select[disabled=disabled]').removeAttr('disabled');
                validateAndSave();
            }
        };

        function validateAndSave() {
            $http({
                method : 'GET',
                url : 'metaData/crmCodes/isCRMCodeUnique/',
                params : {'messageType' : $scope.CRMCODE.messageType,
                          'code' : $scope.CRMCODE.code,
                          'eventCategory' : $scope.CRMCODE.eventCategory}
            }).then(function(response) {
                if (response.data) {
                    save();
                } else {
                    notUnique();
                }
            }, function() {
                notUnique();
            });
        }

        function save() {
            httpAjax($http, 'POST', 'metaData/crmCodes/modify', $("#createForm").serialize(), function(data) {
                $location.path("/metaData/crmCodes/list");
                $.growl.message(data);
            });
        }

        function notUnique() {
            $scope.page.isPageLoading = false;
            $.growl.message({error: 'CRM codes is not unique'});
        }
    }
]);