MCTApp.controller("MCTController", ["$scope", "MenuService",  function($scope, MenuService) {
    $scope.MenuService = MenuService;

    $scope.gridPageSize = 10;
    $scope.gridMessage = {
        success: null,
        error: null
    };

    $scope.page = {
        isPageLoading: false
    };

    // variable to store current testing results data (just after request executing)
    $scope.currentTestingResultsData = {
        testingResults: null
    };

    $scope.$on('$locationChangeStart', function(event, absNewUrl, absOldUrl) {

        if(absNewUrl.indexOf('devKit/track') > 0) {
            var CRMConfigured = $('#devKitConfigured').val();
            if(CRMConfigured == 'false') {
                event.preventDefault();
            }
        }

    });

    $scope.$on('$routeChangeStart', function(evt, absNewUrl, absOldUrl) {
        if(absNewUrl.$$route && absNewUrl.$$route.templateUrl)
            $scope.page.isPageLoading = true;
    });
    $scope.$on('$routeChangeSuccess', function(evt, absNewUrl, absOldUrl) {
        if(absNewUrl.locals.$template && absNewUrl.locals.$template.indexOf('login.js') != -1){
            $scope.showModal('.session-modal');
            return;
        }

        if(absNewUrl.$$route && absNewUrl.$$route.templateUrl)
            $scope.page.isPageLoading = false;
        
    });

    //Utilities
    $scope.showModal = function(sel, callback) {
        var $visibleModals = $('.modal:visible');
        $(sel).off('shown.bs.modal');
        if($visibleModals.size() > 0) {
            //$('.modal:visible').off('hidden.bs.modal');
            $('.modal:visible').one('hidden.bs.modal', function () {
                $(sel).modal('show');
            });
            $('.modal:visible').modal('hide');
        }
        else {
            $(sel).modal('show');
        }
        if(callback) {
            //$(sel).off('shown.bs.modal');
            $(sel).one('shown.bs.modal', function(e) {
                callback();
            });
        }
    };
    $scope.hideModal = function() {
        $('.modal:visible').modal('hide');
    };
    $scope.showIndicator = function() {
        $('body').append('<div class="ajax-indicator-overlay"></div><div class="ajax-indicator"></div>');
        $('.ajax-indicator-overlay').css('opacity',.25);
    };
    $scope.hideIndicator = function() {
        $('.ajax-indicator, .ajax-indicator-overlay').remove();
    };

}]);

/* Utility Ajax Methods */
//post the current form, reload it and show updated message
function postForm($scope, $http, $route, url, d) {
    $scope.page.isPageLoading = true;
    httpAjax($http, 'POST', url, d, function(data, status, headers, config) {
        $scope.page.isPageLoading = false;
        $route.reload();
        $.growl.message(data);
    });
}

// Post form and show the message above grid
function postFormGrid($scope, $http, url, d, forceUpdateGrid, callback) {
    httpAjax($http, 'POST', url, d, function(data, status, headers, config) {
        $scope.hideModal();
        $.growl.message(data);
        if(forceUpdateGrid)
            angular.element(forceUpdateGrid).scope().updateGrid();
        else
            $scope.refreshGrid();

        if(callback) {
            callback();
        }
    });
}

function httpAjax($http, type, url, d, success, error) {
    $http({
        method: type,
        url: url,
        data: d,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(data, status, headers, config) {
            if(success){
                success(data, status, headers, config);
            }
        })
        .error(function(data, status, headers, config){
            if(error){
                error(data, status, headers, config);
            }
            else{
                var ele = angular.element('.error-code');
                ele.html(status);

                var scope = angular.element('.error-modal').scope();
                scope.showModal('.error-modal');
            }
        });
}

function keyValToArray(obj, desc) {
    var arr = [];
    for(i in obj) {
        arr.push({
            key: i,
            val: obj[i]
        });
    }
    arr.sort(function(a,b) {
        return a.key.localeCompare( b.key );
    });
    return arr;
}