MCTApp.controller("TestingResultController", ['$scope', '$http', '$location', 'TestingResult', 'ContentTypes', function ($scope, $http, $location, TestingResult, ContentTypes) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.tr = TestingResult.testingResult;
    $scope.contentTypes = ContentTypes.contentTypes;

    $scope.getMode = function (type) {
        for (var i = 0; i < $scope.contentTypes.length; i++) {
            if (type == $scope.contentTypes[i].value) {
                return $scope.contentTypes[i].mode;
            }
        }
    };

    $scope.editor = CodeMirror.MergeView(document.getElementById('view'), {
        value: $scope.tr.expectedResponse ? $scope.tr.expectedResponse : 'There was no expected response.',
        origRight: $scope.tr.actualResponse ? $scope.tr.actualResponse : 'There was no actual response.',
        lineNumbers: true,
        mode: $scope.getMode($scope.tr.responseType),
        highlightDifferences: true,
        connect: null,
        collapseIdentical: false,
        allowEditingOriginals: true
    });
    $scope.editor.setShowDifferences($scope.tr.expectedResponse && $scope.tr.actualResponse ? true : false);

    $scope.findFormIds = function (id) {
        httpAjax($http, 'GET', 'devtool/functionalTesting/results/formIdsForResult/' + id, null, function (data) {
            $location.path('devtool/functionalTesting/requests/modify/' + data.commonRequestFormId).search({specificRequestFormId: data.specificRequestFormId});
        })
    };

    $scope.findCommonRequestFormId = function (id) {
        httpAjax($http, 'GET', 'devtool/functionalTesting/results/formIdsForResult/' + id, null, function (data) {
            $location.path('devtool/functionalTesting/results/forRequest/' + data.commonRequestFormId);
        })
    };

    angular.element(document).ready(function () {
        setTimeout(function() {
            var expanded = [];
            $('textarea').each(function (index) {
                expanded[index] = false;
                $(this).on('click', function () {
                    this.style.height = 'auto';
                    this.style.height = (expanded[index] ? '70' : (this.scrollHeight + 5)) + 'px';
                    expanded[index] = !expanded[index];
                });
            });
        },700);
    });

}]);

MCTApp.controller("ResultsForRequestController", ['$scope', '$http', '$location', '$route', 'TestingResults', function ($scope, $http, $location, $route, TestingResults) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.title = 'There Are No Results For This Request';
    if ($scope.currentTestingResultsData.testingResults && $scope.currentTestingResultsData.testingResults.length > 0) {
        $scope.trs = $scope.currentTestingResultsData.testingResults;
        $scope.title = 'Current Testing Results For Request: ';
    } else {
        $scope.trs = TestingResults.testingResults;
        if ($scope.trs) {
            $scope.title = 'Testing Results History For Request: ';
        }
    }
    $scope.currentTestingResultsData.testingResults = null;

    $scope.predicate = 'timeOfRequesting';
    $scope.reverse = true;
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.getFirstLine = function (text) {
        if (!text) {
            return text;
        }
        return text.split('\n')[0];
    };

    $scope.findFormIds = function (id) {
        httpAjax($http, 'GET', 'devtool/functionalTesting/results/formIdsForResult/' + id, null, function (data) {
            $location.path('devtool/functionalTesting/requests/modify/' + data.commonRequestFormId).search({specificRequestFormId: data.specificRequestFormId});
        })
    };

    $scope.showDeleteResultModal = function (row, index) {
        $scope.indexToDelete = index;
        $scope.resultDeleteData = {
            id: row.id,
            description: row.description,
            timeOfRunning: row.timeOfRequesting,
            status: row.status
        };
        $scope.showModal('#delete-result-modal');
    };

    $scope.confirmResultDeletion = function () {
        $scope.trs.splice($scope.indexToDelete, 1);
        httpAjax($http, 'POST', 'devtool/functionalTesting/results/delete', 'resultIds=' + $scope.resultDeleteData.id, function (data) {
            $route.reload();
            $scope.hideModal();
        });
    };

    $scope.showDeleteResultsModal = function (name, index) {
        $scope.indexToDelete = index;
        $scope.checkboxName = name;
        $scope.showModal('#delete-results-modal');
    };

    $scope.confirmResultsDeletion = function () {
        var ids = $scope.getIdsFromInputs();
        httpAjax($http, 'POST', 'devtool/functionalTesting/results/delete', 'resultIds=' + ids, function (data) {
            $scope.hideModal();
            $route.reload();

        });
    };

    $scope.getIdsFromInputs = function () {
        var inputs = $('input[name=' + $scope.checkboxName + ']:checked');
        return $.map(inputs, function (o) {
            return $(o).val();
        });
    };

    $scope.isAnyCheckBoxChecked = function (columnName) {
        var list = [];
        if ($scope.trs && $scope.trs.length > 0) {
            for (var i = 0; i < $scope.trs.length; i++) {
                if ($scope.trs[i].checked) {
                    var l = $scope.trs[i];
                    list.push(l[columnName]);
                }
            }
        }
        return list.length != 0;
    }

}]);