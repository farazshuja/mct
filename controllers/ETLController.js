MCTApp.controller("ALEWSController", ["$scope", "$http", "$route", 'jsExpression', function($scope, $http, $route, jsExpression) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Status & Execution Control');
    $scope.dateGreaterThanError = false;
    $scope.wrongInterval = false;
    $scope.dateRangeHours = 0;
    $scope.dateRange = null;
    $scope.jsExpression = jsExpression.jsExpression;
    // If true - skip duplicated leads by LeadSellerExternalId and DealerID criteria
    $scope.skipDulpicatedLeads = (jsExpression.skipDulpicatedByLeadSellerExternalIDDealerCode == 'true');

    //validation for end date is greater than start
    $scope.$watch('start+end', function (v,c,$scope) {

        if($scope.alewsForm.start.$valid && $scope.alewsForm.end.$valid) {
            var v1 = $scope.alewsForm.start.$viewValue;
            var v2 = $scope.alewsForm.end.$viewValue;

            if(typeof v1 != 'string' || typeof v2 != 'string') {
                return;
            }
            var startDate = new Date(v1.substr(6,4) + '-' + v1.substr(0,2) + '-' + v1.substr(3,2) + 'T' + v1.substr(11));
            var endDate = new Date(v2.substr(6,4) + '-' + v2.substr(0,2) + '-' + v2.substr(3,2) + 'T' + v2.substr(11));

            if(endDate > startDate) {
                $scope.dateGreaterThanError = false;
                $scope.alewsForm.$setValidity('greater-date', true);
                //show the range msg
                var hours = (endDate - startDate) / 3600000;
                var mins = 0;
                var secs = 0;
                var remainder1 = (endDate - startDate) % 3600000;
                if( remainder1 > 0)
                {
                	mins = remainder1 / 60000;
                }
                var remainder2 = remainder1 % 60000;
                if( remainder2 > 0)
                {
                  secs = remainder2 / 1000;
                }
                $scope.dateRangeHours = parseInt(hours);
                $scope.dateRange = 'Range has ' + parseInt(hours) + ' hours, ' + parseInt(mins) + ' mins, ' + parseInt(secs) + ' secs.';
                validateInterval($scope);
            }
            else {
                $scope.dateGreaterThanError = true;
                $scope.alewsForm.$setValidity('greater-date', false);
                $scope.dateRange = null;
            }
        }
    });
    $scope.$watch('runInterval', function (v,c,$scope) {
        validateInterval($scope);
    });

    $scope.submitForm = function(form) {
        if(form.$valid) {
            var d = $('#' + form.$name).serialize();
            postForm($scope, $http, $route, 'jobs/alews', d);
        }
    };

    function validateInterval($scope) {
        var interval = $scope.alewsForm.runInterval.$viewValue;

        if(interval < 1 || interval > 360) {
            $scope.alewsForm.$setValidity('interval-time', false);
            $scope.wrongInterval = true;
        }
        else if(interval > $scope.dateRangeHours){
            $scope.alewsForm.$setValidity('interval-time', false);
            $scope.wrongInterval = true;
        }
        else {
            $scope.alewsForm.$setValidity('interval-time', true);
            $scope.wrongInterval = false;
        }
    }
}]);

MCTApp.controller("IAStartSingleProcessingController", ["$scope", "$http", "$route", function($scope, $http, $route) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Status & Execution Control');
    $scope.viewModel = {
        selectAll: false,
        isAnyChecked: false
    };

    $scope.selectAllChange = function() {

        var rows = this.grid.data.rows;
        for(var i=0;i<rows.length;i++){
            rows[i].checked = $scope.viewModel.selectAll;
        }
        $scope.viewModel.isAnyChecked = $scope.viewModel.selectAll;
        $('#selectAll').prop('indeterminate', false);

    }
    $scope.submitForm = function(form) {
        if(form.$valid) {
            var d = $('#' + form.$name).serialize();
            postForm($scope, $http, $route, 'jobs/IAStartSingleProcessing', d);
        }
    };
    $scope.updateSelectAllChkBox = function() {
        var rows = this.grid.data.rows;
        var tickCount = 0;
        for(var i=0;i<rows.length;i++){
            if(rows[i].checked) {
                tickCount++;
            }
        }

        $scope.viewModel.selectAll = tickCount == rows.length ? true : false;
        $scope.viewModel.isAnyChecked = tickCount > 0;
        $('#selectAll').prop('indeterminate', tickCount != 0 && tickCount != rows.length);
    }
}]);

MCTApp.controller("IAExecutionControlController", ["$scope", "$http", "$route", function($scope, $http, $route) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Status & Execution Control');
    $scope.viewModel = {
        selectAll: false,
        isAnyChecked: false
    };

    $scope.selectAllChange = function() {
        var rows = this.grid.data.rows;
        for(var i=0;i<rows.length;i++){
            rows[i].checked = $scope.viewModel.selectAll;
        }
        $scope.viewModel.isAnyChecked = $scope.viewModel.selectAll;
        $('#selectAll').prop('indeterminate', false);
    }
    $scope.submitForm = function(form) {
        if(form.$valid) {
            var d = $('#' + form.$name).serialize();
            postForm($scope, $http, $route, 'jobs/IAExecutionControl', d);
        }
    };
    $scope.updateSelectAllChkBox = function() {
        var rows = this.grid.data.rows;
        var tickCount = 0;
        for(var i=0;i<rows.length;i++){
            if(rows[i].checked) {
                tickCount++;
            }
        }

        $scope.viewModel.selectAll = tickCount == rows.length ? true : false;
        $scope.viewModel.isAnyChecked = tickCount > 0;
        $('#selectAll').prop('indeterminate', tickCount != 0 && tickCount != rows.length);
    }
}]);

MCTApp.controller("CommonJobsSingleRunController", ["$scope", "$http", "$route", function($scope, $http, $route) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Status & Execution Control');
    $scope.viewModel = {
        isAnyChecked: false
    };

    $scope.submitForm = function(form) {
        if(form.$valid) {
            var d = $('#' + form.$name).serialize();
            postForm($scope, $http, $route, 'jobs/commonJobsSingleRun', d);
        }
    };

    $scope.updateSelectAllChkBox = function() {
        var rows = this.grid.data.rows;
        var tickCount = 0;
        for(var i=0;i<rows.length;i++){
            if(rows[i].checked) {
                tickCount++;
            }
        }

        $scope.viewModel.isAnyChecked = tickCount > 0;
    }
}]);

MCTApp.controller("CommonJobsControlRunController", ["$scope", "$http", "$route", function($scope, $http, $route) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Status & Execution Control');
    $scope.viewModel = {
        isAnyChecked: false
    };

    $scope.submitForm = function(form) {
        if(form.$valid) {
            var d = $('#' + form.$name).serialize();
            postForm($scope, $http, $route, 'jobs/commonJobsControlRun', d);
        }
    };

    $scope.updateSelectAllChkBox = function() {
        var rows = this.grid.data.rows;
        var tickCount = 0;
        for(var i=0;i<rows.length;i++){
            if(rows[i].checked) {
                tickCount++;
            }
        }

        $scope.viewModel.isAnyChecked = tickCount > 0;
    }
}]);

MCTApp.controller("LPSingleFeedGenerationController", ["$scope", "$http", "$route", function($scope, $http, $route) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Status & Execution Control');
    $scope.viewModel = {
        selectAll: false,
        isAnyChecked: false
    };

    $scope.selectAllChange = function() {

        var rows = this.grid.data.rows;
        for(var i=0;i<rows.length;i++){
            rows[i].checked = $scope.viewModel.selectAll;
        }
        $scope.viewModel.isAnyChecked = $scope.viewModel.selectAll;
        $('#selectAll').prop('indeterminate', false);

    }

    $scope.submitForm = function(form) {
        if(form.$valid) {
            var d = $('#' + form.$name).serialize();
            postForm($scope, $http, $route, 'jobs/LPSingleFeedGeneration', d);
        }
    };

    $scope.updateSelectAllChkBox = function() {
        var rows = this.grid.data.rows;
        var tickCount = 0;
        for(var i=0;i<rows.length;i++){
            if(rows[i].checked) {
                tickCount++;
            }
        }

        $scope.viewModel.selectAll = tickCount == rows.length ? true : false;
        $scope.viewModel.isAnyChecked = tickCount > 0;
        $('#selectAll').prop('indeterminate', tickCount != 0 && tickCount != rows.length);
    }

}]);

/*
    Handling click events in jQuery style as there are a lot of row and angularJS is slow to read check/uncheck of each
 */
MCTApp.controller("LPFeedGenerationControlController", ["$scope", "$http", "$route", function($scope, $http, $route) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Status & Execution Control');
    $scope.viewModel = {
        selectAll: true,
        isAnyChecked: true
    };

    $scope.selectAllChange = function() {
        var selectAll = $('#selectAll').is(':checked');
        $('.grid-con-body input:checkbox').prop('checked', selectAll)

        $scope.viewModel.isAnyChecked = selectAll;
        $('#selectAll').prop('indeterminate', false);
    }

    $scope.submitForm = function(form) {
        if(form.$valid) {
            var d = $('#' + form.$name).serialize();
            postForm($scope, $http, $route, 'jobs/LPFeedGenerationControl', d);
        }
    };

    $scope.updateSelectAllChkBox = function(index) {
        var checked = $('.grid-con-body input:checkbox:checked').length;
        var rows = this.grid.data.rows.length;

        $('#selectAll').prop('checked', checked == rows );
        $scope.viewModel.isAnyChecked = checked > 0;
        $('#selectAll').prop('indeterminate', checked != 0 && checked != rows);
    }

    $scope.repeatDone = function() {
        var rows = this.grid.data.rows;
        var checked = 0;
        for(var i=0;i<rows.length;i++) {
            if(rows[i].allowProcessing) {
                checked++;
            }
        }
        var rowsLength = this.grid.data.rows.length;
        if(checked == rowsLength) {
            $('#selectAll').prop('checked', true);
        }
        else if(checked > 0) {
            $('#selectAll').prop('indeterminate', true);
        }
        else {
            $('#selectAll').prop('checked', false);
        }
    };
}]);