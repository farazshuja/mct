/**
 * Controller for List page of Setup OEM
 * #/metaData/OEM/list
 */
(function() {
    angular.module('mctApp').controller("OEMController", OEMController);
    OEMController.$inject = ['GridView', 'MenuService', 'Modal'];

    function OEMController(GridView, MenuService, Modal){
        var vm = this;
        MenuService.setActiveMenuMod('mct-mod', 'OEMs');

        //initialize a local variable with GridView Service
        vm.Grid = GridView.init('metaData/OEM/grid');

        vm.enableDisableData = {};
        vm.showEnableDisableModal = function(row) {
            vm.enableDisableData = {
                id: row.id,
                name: row.name,
                title: row.enabled ? 'Disable' : 'Enable',
                url:  row.enabled ? 'metaData/OEM/disable' : 'metaData/OEM/enable'
            }
            Modal.showModal('#enable-disable-modal');

        };
        vm.confirmEnableDisable = function(row) {
            Modal.showModal('.ajax-modal', function(){
                vm.Grid.postForm(row.url, 'id=' + row.id, Modal);
            });
        };

    }

})();

/**
 * Controller for create and modify pages of Setup OEM
 * #/metaData/OEM/create
 * #/metaData/OEM/modify/:id
 */
(function() {
    angular.module('mctApp').controller("OEMEditController", OEMEditController);
    OEMEditController.$inject = ['StaticTypes', 'OemData', 'OEMDTO', 'MenuService'];

    function OEMEditController(StaticTypes, OemData, OEMDTO, MenuService){
        var vm = this;
        MenuService.setActiveMenuMod('mct-mod', 'OEMs');

        if(OemData) {
            OEMDTO.initData(OemData);
            vm.isUpdate = true;
        }
        else {
            OEMDTO.initData();
        }

        vm.StaticTypes = StaticTypes;
        vm.OEM = OEMDTO.data;

        vm.submitForm = function(angObj) {
            if(angObj.$valid) {
                var url = vm.isUpdate ? 'metaData/OEM/modify' : 'metaData/OEM/create';
                OEMDTO.save('#createForm', url, "/metaData/OEM/list");
            }
        }
    }
})();