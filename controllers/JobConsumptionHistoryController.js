MCTApp.controller("JobConsumptionHistoryController", ["$scope", "$http", "StaticTypes", function($scope, $http, StaticTypes) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Data Consumption History');

    $scope.mediumTypes = StaticTypes.dataProviderMediumTypes;

    $scope.getMediumValue = function(type) {
        return $scope.mediumTypes[type];
    }

}]);

MCTApp.controller("JobConsumptionHistoryDetailedController", ["$scope", "$http", function($scope, $http) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Data Consumption History');
    $scope.DCName = '';
    $scope.isXMLAPI = false;
    $scope.request = '';
    $scope.response = '';

    $scope.showReqRes = function(id) {
        $scope.showModal('.ajax-modal', function(){

            httpAjax($http, 'GET', "metaData/processes/dataConsumerHistory/getAPIRequestResponse/" + id, null, function(data) {
                if(data) {
                    $scope.request = vkbeautify.xml(data.requestContent);
                    $scope.response = vkbeautify.xml(data.responseContent);
                    $scope.showModal('#req-res-modal');
                }
                else {
                    $scope.hideModal();
                }
            });
        });
    };

    $scope.gridLoaded = function(rows) {
        if(rows && rows.length > 0){
            $scope.DCName = rows[0].dataConsumerName;
            $scope.isXMLAPI = rows[0].mediumType === 'XML_API';
        }
    };
}]);