MCTApp.controller("JobSchedulingController", ["$scope", "$http", 'DataConsumers', function($scope, $http, DataConsumers) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Job Scheduling');
    $scope.DataConsumers = DataConsumers.jobDataConsumerList;
    $scope.hasDataConsumers = DataConsumers.jobDataConsumerList.length > 0;

    $scope.refreshGrid = function(){
        $scope.$broadcast ('gridRefreshEvent');
        $scope.updateGrid();
    };

    $scope.enableDisableData = {};
    $scope.showEnableDisableModal = function(row) {
        $scope.enableDisableData = {
            id: row.id,
            name: row.jobName,
            title: row.enabled ? 'Disable' : 'Enable',
            url:  row.enabled ? 'metaData/processes/jobScheduling/disable' : 'metaData/processes/jobScheduling/enable'
        }
        $scope.showModal('.ajax-modal', function() {
            $scope.showModal('#enable-disable-modal');
        });

    };
    $scope.confirmEnableDisable = function(row) {
        $scope.showModal('.ajax-modal', function(){
            postFormGrid($scope, $http, row.url, 'id=' + row.id);
        });
    };

    /* Delete */
    $scope.showDeleteModal = function(row) {
        $scope.jobNameToDelete = row.jobName;
        $scope.jobIdToDelete = row.id;
        $scope.showModal('#delete-consumer-modal');
    };
    $scope.confirmDeletion = function() {
        postFormGrid($scope, $http, 'metaData/processes/jobScheduling/delete', 'id=' + $scope.jobIdToDelete);
    };

    /* Handle Job Executions */
    $scope.executeJob = function(rows){
        var exeRows = [];
        var exeRowIds = [];
        angular.forEach(rows, function(rowObj, index){
            if(rowObj.checked) {
                rowObj.running = true;
                rowObj.checked = false;
                exeRowIds.push(rowObj.id);
                exeRows.push(rowObj);
            }
            else if(rowObj.running) {
                $scope.checkExecutionStatus(rowObj);
            }
        });
        if(exeRowIds.length > 0){
            (function(rowsToExe, ids){
                httpAjax($http, 'POST', 'metaData/processes/jobScheduling/execute?ids=' + ids.join('&ids='), null, function(data) {
                    for(var i=0;i<rowsToExe.length;i++){
                        $scope.checkExecutionStatus(rowsToExe[i]);
                    }
                    $.growl.message(data);
                });
            })(exeRows, exeRowIds);
        }
    };

    $scope.checkExecutionStatus = function(row) {
        setTimeout(function(){
            httpAjax($http, 'GET', 'metaData/processes/jobScheduling/getDataConsumerJob/' + row.id, null, function(data) {
                if(data.dataConsumerJob.running){
                    $scope.checkExecutionStatus(row);
                }
                else {
                    $.growl.message({'success': data.dataConsumerJob.jobName + ' executed successfully'});
                    row.running = false;
                    row.processedFiles = data.dataConsumerJob.processedFiles;
                    row.lastRun = data.dataConsumerJob.lastRun;
                }
            });
        }, 1000);
    };



}]);

MCTApp.controller("JobSchedulingCreateController", ["$scope", "$http", '$location', "DataConsumers", function($scope, $http, $location, DataConsumers) {
    JobSchedulingCommon($scope, $http, $location, DataConsumers, 'metaData/processes/jobScheduling/create');
}]);

MCTApp.controller("JobSchedulingModifyController", ["$scope", "$http", '$location', "DataConsumers", 'ScheduledJob', function($scope, $http, $location, DataConsumers, ScheduledJob) {
    $scope.SJ = ScheduledJob.dataConsumerJob;
    JobSchedulingCommon($scope, $http, $location, DataConsumers, 'metaData/processes/jobScheduling/modify');
}]);
function JobSchedulingCommon($scope, $http, $location, DataConsumers, url) {
    $scope.MenuService.setActiveMenuMod('proc-mod', 'Job Scheduling');
    $scope.DataConsumers = DataConsumers.jobDataConsumerList;
    $scope.submitForm = function(angObj) {
        if(angObj.$valid) {
            $scope.page.isPageLoading = true;
            httpAjax($http, 'POST', url, $("#createForm").serialize(), function(data) {
                if(data.error) {
                    $scope.page.isPageLoading = false;
                }
                else {
                    $location.path("/metaData/processes/jobScheduling/list");
                    $.growl.message(data);
                }
            });
        }
    };
}