MCTApp.controller("EnvironmentConfigController", ['$scope', '$http', '$location',  'Environment', function ($scope, $http, $location, Environment) {
    $scope.MenuService.setActiveMenuMod('mct-mod');

    $scope.environment = Environment ? Environment.environment : null;
    if ($scope.environment == null) {
        $scope.action = 'create';
    } else {
        $scope.action = 'modify';
    }

    $scope.submitForm = function () {
        $scope.page.isPageLoading = true;
        var url = 'devtool/functionalTesting/environments/' + $scope.action;
        httpAjax($http, 'POST', url, $("#createEnvironment").serialize(), function (data) {
            $scope.page.isPageLoading = false;
            $location.path("/devtool/functionalTesting");
            $.growl.message(data);
        })
    };

}]);
