MCTApp.controller("DataConsumerController", ['$scope', '$http', '$location', function($scope, $http, $location){
    $scope.MenuService.setActiveMenuMod('ds-mod', 'Data Consumers');  //set the correct Menu

    $scope.refreshGrid = function(){
        $scope.$broadcast ('gridRefreshEvent');
        $scope.updateGrid();
    };

    $scope.enableDisableData = {};
    $scope.showEnableDisableModal = function(row) {
        $scope.enableDisableData = {
            id: row.id,
            name: row.name,
            title: row.enabled ? 'Disable' : 'Enable',
            url:  row.enabled ? 'metaData/dataConsumers/disable' : 'metaData/dataConsumers/enable'
        }
        $scope.showModal('#enable-disable-modal');
    };
    $scope.confirmEnableDisable = function(row) {
        $scope.showModal('.ajax-modal', function(){
            postFormGrid($scope, $http, row.url, 'id=' + row.id);
        });
    };
    $scope.showDeleteModal = function(row) {
        $scope.dcNameToDelete = row.name;
        $scope.dcIdToDelete = row.id;
        $scope.showModal('#delete-consumer-modal');
    };
    $scope.confirmDeletion = function() {
        postFormGrid($scope, $http, 'metaData/dataConsumers/delete', 'id=' + $scope.dcIdToDelete);
    };

    $scope.OEM = {};
    $scope.submitForm = function(angObj) {
        if(angObj.$valid) {
            $scope.page.isPageLoading = true;
            httpAjax($http, 'POST', 'metaData/dataConsumers/create', $("#createForm").serialize(), function(data) {
                $location.path("/metaData/dataSources/dataConsumers/list");
                $.growl.message(data);
            });
        }
    }

}]);
MCTApp.controller("DataConsumerCreateModifyController", ['$scope', '$http', '$location', '$filter', '$timeout', 'OEMList', 'DataConsumer', 'DataProviders', 'Types', 'lcList','ltList', 'NurturingPrograms',
    function($scope, $http, $location, $filter, $timeout, OEMList, DataConsumer, DataProviders, Types, lcList,ltList,NurturingPrograms){
        $scope.MenuService.setActiveMenuMod('ds-mod', 'Data Consumers');  //set the correct Menu
        $scope.DC = DataConsumer ? DataConsumer.dataConsumer : null;
        var serviceURLstatic = window.location.origin + window.location.pathname + 'services/';
        serviceURLstatic = serviceURLstatic.indexOf('trilogy') != -1 ? serviceURLstatic.replace(window.location.pathname, '/') : serviceURLstatic;
        $scope.serviceURLStatic = serviceURLstatic;


        $scope.oemList = OEMList.oemList;
        $scope.lcList = lcList.lcList;
        $scope.ltList = ltList.ltList;
        $scope.NurturingPrograms = NurturingPrograms.nurturingProgramList;
        $scope.NPValid = true;
        $scope.dataProviderList = DataProviders.dataProviderList;
        $scope.Types = Types;
        $scope.actionTypes = angular.copy(Types.disallowedActionType);

        $scope.DCodeXPathIfBuilder = [{}];
        $scope.XPathorCSVIfBuilder = [{}];

        $scope.dataConsumerOEMDto = {
            oemDeterminationTypeEnum: findAndReturnProperty($scope, 'DC.dataConsumerOEMDto.oemDeterminationTypeEnum', 'DLCODE'),
            ifBuilderDtoListOEM: [],
            productProgramIdentifierDeterminationEnum: findAndReturnProperty($scope, 'DC.leadProperty.productProgramDto.productProgramIdentifierDeterminationEnum' ,'NO_PROGRAM'),
            programIfBuilder: [{}],
            leadPriceDeterminationEnum: findAndReturnProperty($scope, 'DC.leadProperty.leadPriceDto.leadPriceDeterminationEnum', 'FIXED'),
            leadPriceIfBuilder: [{}],
            leadDuplicationIfBuilder: [{}],
            dataRejectionIfBuilder: [{}],
            inventoryLeadIfBuilder: [{}],
            leadMediumIfBuilder: [{}],
            leadModelIfBuilder: [{}]
        };
        function findAndReturnProperty(obj, strObjName, defaultValue) {
            var objs = strObjName.split('.');
            var o = obj;
            for(var i=0;i<objs.length;i++) {
                if(o[objs[i]]){
                    o = o[objs[i]];
                }
                else {
                    o = null;   //chain must never broken
                    break;
                }
            }
            if(o)
                return o;

            return defaultValue;
        }

        $scope.leadProperty = {enableDdsaCheck: false};
        $scope.leadDispositionProperty = {
            enableDdsaCheck: false,
            saveMergedInfo: false
        };

        $scope.dataDestinationIsLead = false;
        $scope.dataDestinationIsSales = false;
        $scope.dataDestinationIsDealerMaster = false;
        $scope.dataDestinationIsLeadDisposition = false;
        $scope.dataDestinationIsInventory = false;
        $scope.dataDestinationIsVehicleInventory = false;
        $scope.dataDestinationValue;
        $scope.mappedRows = []; //variable to generate mapping rows on jsp page
        $scope.mappedList = [];
        $scope.transList = Types.transformationsList;
        var calledManually = false;
        $scope.dataDestinationsSel = $scope.DC ? $scope.DC.dataDestinationId : Types.dataDestinations[0].id;

        var currentVal = $scope.dataDestinationsSel;
        var lastVal = currentVal;
        $scope.dataDestinationsM = $scope.dataDestinationsSel;
        $scope.$watch('dataDestinationsM', function(newVal) {
            if(calledManually) {
                currentVal = newVal;
                $scope.showModal('#destination-change-modal');
            }
            calledManually = true;
        });

        $scope.confirmDestinationChange = function(newVal) {
            $scope.mappedRowsLoading = true;
            lastVal = currentVal;
            $scope.hideModal();

            $scope.dataDestinationIsLead = false;
            $scope.dataDestinationIsSales = false;
            $scope.dataDestinationIsDealerMaster = false;
            $scope.dataDestinationIsLeadDisposition = false;
            $scope.dataDestinationIsInventory = false;
            $scope.dataDestinationIsVehicleInventory = false;
            $scope.dataDestinationsSel = currentVal;
            for(var i=0;i<Types.dataDestinations.length;i++){
                var obj = Types.dataDestinations[i];
                //fill the name in Mapped to $column Name in mapped grid
                if(currentVal == obj.id) {
                    $scope.dataDestinationValue = obj.name;
                    $http.get('metaData/dataConsumers/getDataDestination/' + obj.id).success(function(data) {
                        $scope.dataDestinationIsLead = data.lead;
                        $scope.mappedList = [];
                        angular.forEach(data.dataDestinationColumns, function(v,k){
                            $scope.mappedList.push({
                                id: v.name,
                                name: v.name
                            });
                        });
                    });
                    break;
                }
            }

            var savedMapping = findAndReturnProperty($scope, 'DC.destinationMappings' ,false);
            if(savedMapping){
                $scope.mappedRows = savedMapping;
            }
            else {
                setTimeout(function(){
                    resetMappedRows();
                    $scope.mappedRowsLoading = false;
                    $scope.$apply();
                },700);

            }
            $scope.dataDestinationIsSales = $scope.dataDestinationValue == 'Sales';
            
            $scope.dataDestinationIsDealerMaster = $scope.dataDestinationValue == 'DealerMaster';
            
            $scope.dataDestinationIsLeadDisposition = $scope.dataDestinationValue == 'LeadDisposition';
            
            $scope.dataDestinationIsInventory = $scope.dataDestinationValue == 'CertifiedInventory';
            
            $scope.dataDestinationIsVehicleInventory = $scope.dataDestinationValue == 'VehicleInventory';
        }

        $scope.confirmDestinationChange();

        $scope.cancelDestinationChange = function() {
            calledManually = false; //as next time its automatically calling watch so to avoid popup set it to false
            $scope.dataDestinationsM = lastVal;
        }

        $scope.$watch('dataConsumerOEMDto.oemDeterminationTypeEnum', function(newVal) {
            if(newVal == 'DLCODE_OR_XPATH') {
                $scope.dataConsumerOEMDto.ifBuilderDtoListOEM = $scope.DCodeXPathIfBuilder;
                validateIfBuilderRow($scope.DCodeXPathIfBuilder, 'IfBuilderOEMDetValid');
            }
            else if(newVal == 'XPATH') {
                $scope.dataConsumerOEMDto.ifBuilderDtoListOEM = $scope.XPathorCSVIfBuilder;
                validateIfBuilderRow($scope.XPathorCSVIfBuilder, 'IfBuilderOEMDetValid');
            }
            disableUnknownDealerSelect();

        });
        $scope.$watch('providerHasMoreOEMs', function(newVal) {
            disableUnknownDealerSelect();
        });
        $scope.$watch('providerMediumType', function(newVal) {
            $scope.actionTypes = angular.copy(Types.disallowedActionType);
            if(newVal && newVal != 'XML_API') {
                //also check if it was selected revert back to some other value
                var currentAction = $scope.DC ? $scope.DC.unknownDealerAction : $scope.unknownDealerAction;
                if($scope.DC) {
                    if($scope.DC.unknownDealerAction == 'REJECT_WITH_REASON') {
                        $scope.DC.unknownDealerAction = 'ACCEPT';
                    }
                }
                else {
                    if($scope.unknownDealerAction == 'REJECT_WITH_REASON') {
                        $scope.unknownDealerAction = 'ACCEPT';
                    }
                }
                delete $scope.actionTypes.REJECT_WITH_REASON;
            }

        });
        /*
            A utility method to disable Unknown Dealer Action Drop Down in case radio button
            "Determine based on dealer code ONLY" is selected
         */
        function disableUnknownDealerSelect() {
            $scope.disableUnknownDealerAction = false;
            if($scope.providerHasMoreOEMs && $scope.dataConsumerOEMDto.oemDeterminationTypeEnum == 'DLCODE'){
                if($scope.DC)
                    $scope.DC.unknownDealerAction = 'REJECT_WITH_REASON';
                else
                    $scope.unknownDealerAction = 'REJECT_WITH_REASON';
                $scope.disableUnknownDealerAction = true;
            }
        }

        $scope.addNewMappingOEM = function(ifBuilder) {
            ifBuilder = ifBuilder ? ifBuilder : 'ifBuilderDtoListOEM';
            $scope.dataConsumerOEMDto[ifBuilder].push({});

        };
        $scope.deleteMappingOEM = function(row, ifBuilder) {

            ifBuilder = ifBuilder ? ifBuilder : 'ifBuilderDtoListOEM';
            for(var i=0;i<$scope.dataConsumerOEMDto[ifBuilder].length; i++) {
                var item = $scope.dataConsumerOEMDto[ifBuilder][i];
                if(item.$$hashKey == row.$$hashKey) {
                    $scope.dataConsumerOEMDto[ifBuilder].splice(i, 1);
                    break;
                }
            }
        };

        $scope.IfBuilderOEMDetValid = false;    //to enable/disable Add Mapping button in IfBuilder Determine OEM section
        $scope.ProgramIfBuilderOEMDetValid = false;    //to enable/disable Add Mapping button in IfBuilder of Program identifier section
        $scope.LeadPriceIfBuilderOEMDetValid = false;    //to enable/disable Add Mapping button in IfBuilder of Lead Price section
        $scope.LeadDuplicationIfBuilderValid = false;    //to enable/disable Add Mapping button in IfBuilder of Lead duplication section
        $scope.DataRejectionIfBuilderValid = false;     //to enable/disable Add Mapping button in IfBuilder of Data Rejection section
        $scope.InventoryLeadIfBuilderValid = false;     //to enable/disable Inventry Lead Determination IfBuilder
        $scope.LeadMediumIfBuilderValid = false;     //to enable/disable Lead Medium Determination IfBuilder
        $scope.LeadModelIfBuilderValid = false;     //to enable/disable Lead Model Determination IfBuilder
        $scope.$watch('DCodeXPathIfBuilder', function(newVal, oldVal) {
            if (newVal != oldVal) {
                validateIfBuilderRow(newVal, 'IfBuilderOEMDetValid');
            }
        }, true);
        $scope.$watch('XPathorCSVIfBuilder', function(newVal, oldVal) {
            if (newVal != oldVal) {
                validateIfBuilderRow(newVal, 'IfBuilderOEMDetValid');
            }
        }, true);
        $scope.$watch('dataConsumerOEMDto.programIfBuilder', function(newVal) {
            validateIfBuilderRow(newVal, 'ProgramIfBuilderOEMDetValid');
        }, true);
        $scope.$watch('dataConsumerOEMDto.leadPriceIfBuilder', function(newVal) {
            validateIfBuilderRow(newVal, 'LeadPriceIfBuilderOEMDetValid');
        }, true);
        $scope.$watch('dataConsumerOEMDto.leadDuplicationIfBuilder', function(newVal) {
            validateIfBuilderRow(newVal, 'LeadDuplicationIfBuilderValid');
        }, true);
        $scope.$watch('dataConsumerOEMDto.dataRejectionIfBuilder', function(newVal) {
            validateIfBuilderRow(newVal, 'DataRejectionIfBuilderValid');
        }, true);
        $scope.$watch('dataConsumerOEMDto.inventoryLeadIfBuilder', function(newVal) {
            validateIfBuilderRow(newVal, 'InventoryLeadIfBuilderValid');
        }, true);
        $scope.$watch('dataConsumerOEMDto.leadMediumIfBuilder', function(newVal) {
            validateIfBuilderRow(newVal, 'LeadMediumIfBuilderValid');
        }, true);
        $scope.$watch('dataConsumerOEMDto.leadModelIfBuilder', function(newVal) {
            validateIfBuilderRow(newVal, 'LeadModelIfBuilderValid');
        }, true);

        function validateIfBuilderRow(newVal, addBtn) {
            $scope[addBtn] = true;     //reset btn state to enabled
            if(!newVal)
                return;

            for(var i=0;i<newVal.length;i++) {
                var row = newVal[i];
                var r = angular.isDefined(row.result) ? row.result.toString() : '';
                var tV = angular.isDefined(row.daysThreshold) ? row.daysThreshold.toString() : '';
                var e = angular.isDefined(row.expression) ? row.expression : '';

                //if any row is not fille disable add-mappin btn
                if(tV.length > 0 || (e.length > 0 && r.length > 0)){ // both filled
                    $scope[addBtn] = !$scope[addBtn] ? false : true;  //do not turn the value to true if in looping found some false value
                    row.IfBuilderOEMDetValid = true;
                }
                else if(tV.length == 0 || (e.length ==0 && r.length ==0)) { //both empty
                    $scope[addBtn] = false ;
                    row.IfBuilderOEMDetValid = true;
                }
                else {  //partially filled i.e. row is invalid
                    $scope[addBtn] = false;
                    row.IfBuilderOEMDetValid = false;
                }
            }
        };

        $scope.submitForm = function(angObj) {
            var url = $scope.DC ? 'metaData/dataConsumers/modify' : 'metaData/dataConsumers/create';
            if(angObj.$valid) {
                $scope.page.isPageLoading = true;
                //generate postable data for mapped rows
                var mappedData = getPostAbleMappedData();
                httpAjax($http, 'POST', url, $("#createForm").serialize() + mappedData, function(data) {
                    if(data.error) {
                        $scope.errorList = data.fields;
                        $scope.page.isPageLoading = false;
                    }
                    else {
                        $location.path("/metaData/dataConsumers/list");
                        $.growl.message(data);
                    }
                });
            }
        };

        $scope.providerType = null;
        $scope.providerMediumType = null;
        $scope.providerFileType = null;
        $scope.providerHasMoreOEMs = false;
        $scope.dataProviderListSel = [];
        $scope.chainConsumersList = [];
        $scope.chainConsumerSel = null;

        $scope.dataProviderSelectionChanged = function(chk) {

            if(chk.checked){
                handleDPSelection(chk);
            }
            else {
                var checkedCount = 0;
                for(var i=0;i<$scope.dataProviderList.length;i++) {
                    var o = $scope.dataProviderList[i];
                    if(o.checked) {
                        handleDPSelection(o);
                        checkedCount++;
                        break;
                    }
                }
                if(checkedCount == 0) {
                    $scope.resetSelectedProviders();
                }
            }
            //handle form element for sending list of provider Ids and OEM Determination section
            $scope.dataProviderListSel = [];
            var commonOEMList = [];

            for(var i=0; i<$scope.dataProviderList.length;i++) {
                var item = $scope.dataProviderList[i];
                if(item.checked) {
                    if(item.oems){
                        updateOEMList(commonOEMList, item.oems);
                    }
                    else if(item.leadCategoryOemMappings) {
                        for(var j=0;j<item.leadCategoryOemMappings.length;j++)
                            updateOEMList(commonOEMList, item.leadCategoryOemMappings[j].oems);
                    }
                }
                if(item.checked) {
                    $scope.dataProviderListSel.push(item.id);
                }
            }
            $scope.providerHasMoreOEMs = commonOEMList.length > 1 ? true : false;
            $scope.DPValid = $scope.dataProviderListSel.length > 0;

            //call ajax to load the chain consumer select box
            //chainConsumer/list?consumerId=1&fileType=CSV_PSV
            var fileType = $scope.providerFileType;
            if(fileType) {
                var url = 'metaData/dataConsumers/chainConsumer/list?fileType=' + fileType;
                if($scope.DC) {
                    url += '&consumerId=' + $scope.DC.id;
                }
                httpAjax($http, 'GET', url, null, function(data, status, headers, config) {
                    $scope.chainConsumerList = data.chainConsumerList;
                    if($scope.DC && $scope.DC.chainConsumer) {
                        for(var i=0;i<data.chainConsumerList.length;i++) {
                            if(data.chainConsumerList[i].id == $scope.DC.chainedConsumerId) {
                                $scope.chainConsumerSel = data.chainConsumerList[i];
                                break;
                            }
                        }
                        //if chainConsumerSel is still null, select first value
                        if(!$scope.chainConsumerSel) {
                            $scope.chainConsumerSel = data.chainConsumerList[0];
                        }
                    }
                    else {
                        $scope.chainConsumerSel = data.chainConsumerList[0];
                    }

                });
            }
            else {
                $scope.chainConsumerList = null;
                $scope.chainConsumerSel = null;
            }
        }

        $scope.nurturingProgramsSelectionChanged = function(chk) {
            var count = 0;
            if(chk === false) {
                $scope.NPValid = true;
            }
            else {
                var isValid = false;
                for(var i=0;i<$scope.NurturingPrograms.length;i++){
                    var o = $scope.NurturingPrograms[i];
                    if(o.checked) {
                        isValid = true;
                        count++;
                    }
                }
                $scope.NPValid = isValid;
                $scope.multipleProgramsSelected = count > 1;
            }

        }


        function updateOEMList(list, oems) {
            if(oems) {
                for(var i=0;i<oems.length;i++) {
                    var o = oems[i];
                    if(list.indexOf(o.name) == -1)  //insert only unique records
                        list.push(o.name);
                }
            }
        }

        function handleDPSelection(chk) {
            $scope.providerType = chk.type;
            $scope.providerMediumType = chk.providerMediumType;
            $scope.providerFileType = chk.providerMediumFileType;

            angular.forEach($scope.dataProviderList, function(v,k) {
                var isSelectable =  v.type == $scope.providerType;
                isSelectable = isSelectable && (v.providerMediumType == $scope.providerMediumType);
                v.selectable = isSelectable
                if($scope.providerMediumType != 'XML_API')
                    v.selectable = isSelectable && (v.providerMediumFileType == $scope.providerFileType)

            });
        };
        $scope.resetSelectedProviders = function() {
            //add attribute selectable to dataProviderList to take decisions for enable similar type providers
            angular.forEach($scope.dataProviderList, function(v,k) {
                v.selectable = true;
            });
            $scope.providerType = null;
            $scope.providerMediumType = null;
            $scope.providerFileType = null;
            $scope.providerHasMoreOEMs = false;
        };

        //in case of modify
        if($scope.DC) {
            //intialize oem determination if builder from DC object
            if($scope.dataConsumerOEMDto.oemDeterminationTypeEnum == 'DLCODE_OR_XPATH')
                $scope.DCodeXPathIfBuilder = $scope.DC.dataConsumerOEMDto.ifBuilderDtoList;
            else if($scope.dataConsumerOEMDto.oemDeterminationTypeEnum == 'XPATH')
                $scope.XPathorCSVIfBuilder = $scope.DC.dataConsumerOEMDto.ifBuilderDtoList;

            //intialize program if builder from DC object
            if($scope.DC.leadProperty.productProgramDto && $scope.DC.leadProperty.productProgramDto.ifBuilderDtoList)
                $scope.dataConsumerOEMDto.programIfBuilder =  $scope.DC.leadProperty.productProgramDto.ifBuilderDtoList;

            //intialize program if builder for lead property
            if($scope.DC.leadProperty.leadPriceDto && $scope.DC.leadProperty.leadPriceDto.ifBuilderDtoList)
                $scope.dataConsumerOEMDto.leadPriceIfBuilder =  $scope.DC.leadProperty.leadPriceDto.ifBuilderDtoList;

            //intialize program if builder for lead duplication
            if($scope.DC.leadProperty.leadDeduplication && $scope.DC.leadProperty.leadDeduplication.leadDeduplcationConfigDtoList)
                $scope.dataConsumerOEMDto.leadDuplicationIfBuilder =  $scope.DC.leadProperty.leadDeduplication.leadDeduplcationConfigDtoList;

            //intialize program if builder for lead discarding
            if($scope.DC.dataDiscardingDto && $scope.DC.dataDiscardingDto.ifBuilderDtoList)
                $scope.dataConsumerOEMDto.dataRejectionIfBuilder =  $scope.DC.dataDiscardingDto.ifBuilderDtoList;

            //intialize program if builder for inventory lead determination
            if($scope.DC.leadProperty.inventoryLeadDto && $scope.DC.leadProperty.inventoryLeadDto.ifBuilderDtoList)
                $scope.dataConsumerOEMDto.inventoryLeadIfBuilder =  $scope.DC.leadProperty.inventoryLeadDto.ifBuilderDtoList;

            //intialize program if builder for lead medium determination
            if($scope.DC.leadProperty.leadMediumDto && $scope.DC.leadProperty.leadMediumDto.ifBuilderDtoList)
                $scope.dataConsumerOEMDto.leadMediumIfBuilder =  $scope.DC.leadProperty.leadMediumDto.ifBuilderDtoList;
            
          //intialize program if builder for lead model determination
            if($scope.DC.leadProperty.leadModelDto && $scope.DC.leadProperty.leadModelDto.ifBuilderDtoList)
                $scope.dataConsumerOEMDto.leadModelIfBuilder =  $scope.DC.leadProperty.leadModelDto.ifBuilderDtoList;

            angular.forEach($scope.dataProviderList, function(v,k) {
                var found = $filter('searchInObject')($scope.DC.dataProvider, 'id', v.id);
                var changed = false;
                if(found) {
                    v.checked = true;
                    if(!changed)
                        $scope.dataProviderSelectionChanged(v);
                    changed = true;
                }
            });
            var count = 0;
            angular.forEach($scope.NurturingPrograms, function(v,k) {

                var found = $filter('searchInObject')($scope.DC.leadProperty.nurturingPrograms, 'id', v.id);
                if(found) {
                    v.checked = true;
                    count++;
                }
            });
            $scope.multipleProgramsSelected = count > 1;

            // parse to string to check the radio buttons dynamically
            $scope.DC.leadProperty.useProgramIdDetermination = $scope.DC.leadProperty.useProgramIdDetermination + '';
        }
        else {
            $scope.resetSelectedProviders();

        }

        //this code section handles the mapping columns grid
        //rows to be generated, on create page based on mappedList generate rows
        function resetMappedRows() {
            $scope.mappedRows = [];
            var tV = $scope.transList[0].inventoryTransformation.value;
            var tN = $scope.transList[0].inventoryTransformation.transformationRepresentation;
            angular.forEach($scope.mappedList, function(v,k){
                $scope.mappedRows.push({
                    id: '',
                    dataDestinationColumnId: v.id,
                    dataDestinationColumnName: v.name,
                    dataSource: "",
                    transformation: tV,
                    transformationName: tN
                });
            });
        }

        $scope.confirmRefresh = function() {
            var header = $scope.createForm['sourceFileProperty.fileHeader'].$viewValue
            var separator = $scope.createForm['sourceFileProperty.columnSeparator'].$viewValue;
            var headerArr = header.split(separator);
            $scope.mappedRows = [];
            var tV = $scope.transList[0].inventoryTransformation.value;
            var tN = $scope.transList[0].inventoryTransformation.transformationRepresentation;

            for(var i=0;i<headerArr.length;i++) {
                var o = $scope.mappedList[i];
                var found = $filter('searchInObject')($scope.mappedList, 'name', headerArr[i]);
                var id,name;
                if(found) {
                    id = found.id;
                    name = found.name;
                }
                else if(o) {
                    id = o.id;
                    name = o.name
                }
                else {
                    id = $scope.mappedList[0].id;
                    name = $scope.mappedList[0].name;
                }

                $scope.mappedRows.push({
                    id: '',
                    dataDestinationColumnId: id,
                    dataDestinationColumnName: name,
                    dataSource: headerArr[i],
                    transformation: tV,
                    transformationName: tN
                });
            }
            $scope.hideModal();
        };
        //loop through all mapped rows and get a list of postable data with proper indexes
        function getPostAbleMappedData() {
            var obj = [];
            $('#mappingGrid .grid-con-body  .row').each(function(i, o){
                var $o = $(o);
                var id = $o.find('.mappingId').val();
                var dS = $.trim($o.find('.dataSource').val());
                var exp = $.trim($o.find('.rejectionReasonMsg').val());
                var desHidden = $o.find('.dataDestinationColumnId');
                var desSelect = desHidden.siblings('select');
                var dC = desSelect.size() > 0 ? desSelect.val() : desHidden.val();
                var transHidden = $o.find('.transHidden');
                var transSelect = transHidden.siblings('select');
                var tS = transSelect.size() > 0 ? transSelect.val() : transHidden.val();
                var rowLevel = $o.find('.rowLevel:checked').size() > 0 ? 'INNER' : 'OUTER';
                var params = $.param({
                    id: id,
                    dataDestinationColumnName: dC,
                    dataSource: dS,
                    jsExpression: $o.find('.jsExpression').is(':checked'),
                    transformation: tS,
                    required:  $o.find('.required').is(':checked'),
                    rejectionReasonMsg: exp,
                    level: rowLevel
                });
                var mapping = '&destinationMappings[' + i + '].';
                params =  mapping + params.replace(/\&/g, mapping);
                obj.push(params);
            });
            return obj.join('');
        }

        $scope.deleteMapping = function(row) {
            for(var i=0;i<$scope.mappedRows.length; i++) {
                var item = $scope.mappedRows[i];
                if(item.$$hashKey == row.$$hashKey) {
                    $scope.mappedRows.splice(i, 1);
                    break;
                }
            }
        };
        $scope.addMapping = function() {
            $scope.mappedRows.push({
                id: '',
                dataDestinationColumnId: $scope.mappedList[0].id,
                dataDestinationColumnName: $scope.mappedList[0].name,
                dataSource: "",
                transformation: $scope.transList[0].inventoryTransformation.value,
                transformationName: $scope.transList[0].inventoryTransformation.transformationRepresentation
            });
        };

        $scope.saveCurrentMapping = function() {
            $scope.mappingName = '';
            $scope.showModal('#save-mapping-modal');
        };
        $scope.confirmSaveMapping = function() {
            var url = 'metaData/dataConsumers/mappingTemplate/save';
            var d =   'name=' + $scope.mappingName + '&destinationDto.id=' + $scope.dataDestinationsSel + '&';
            var mappings = getPostAbleMappedData();
            d += mappings;
            if(mappings != '') {
                $scope.showModal('.ajax-modal', function(){
                    httpAjax($http, 'POST', url, d, function(data, status, headers, config) {
                        $scope.hideModal();
                        $.growl.message(data);
                    });
                });
            }
            else {
                $scope.hideModal();
                $.growl.message({error: 'You cannot save empty mappings!'});
            }
        };
        $scope.templateList = {};
        $scope.mappingTemplate = {};
        $scope.loadMapping = function() {
            //load template via ajax call
            var url = 'metaData/dataConsumers/mappingTemplate/list/' + $scope.dataDestinationsSel;
            $scope.showModal('.ajax-modal', function(){
                httpAjax($http, 'GET', url, null, function(data, status, headers, config) {
                    $scope.templateList = data.mappingTemplateList;
                    $scope.showModal('#load-mapping-modal');
                });
            });

        };
        $scope.confirmLoadMapping = function() {
            var url = 'metaData/dataConsumers/mappingTemplate/get/' + $scope.loadMappingForm.mappingTemplate.$viewValue;
            $scope.showModal('.ajax-modal', function(){
                httpAjax($http, 'GET', url, null, function(data, status, headers, config) {
                    $scope.mappedRows = data.mappingTemplate.destinationMappings;
                    $scope.hideModal();
                    $.growl.message({success: 'Template loaded successfully!'});
                });
            });
        };

        $scope.getPossibleVariables = function(msg) {
            var messages = [];
            angular.forEach($scope.Types.variableList, function(v, k) {
                if(v.messageVariable[msg]){
                    if((!v.messageVariable.leadOnly && !v.messageVariable.leadDispositionOnly) 
                            || ($scope.dataDestinationIsLead && v.messageVariable.leadOnly == $scope.dataDestinationIsLead) 
                            || ($scope.dataDestinationIsLeadDisposition && v.messageVariable.leadDispositionOnly == $scope.dataDestinationIsLeadDisposition))
                    {
                        messages.push(v.messageVariable.variable);
                    }
                }
            });
            if (msg != 'hardRejectMsg') {
                messages.push("{{sourceIdentifier}}");
            }
            return messages.join(', ');
        };
        $scope.validateExpression = function(item) {
            var isValid = true;
            if(item.jsExpression) {
                isValid = false;
                try{
                    var v = item.dataSource;
                    v = v.replace(/(['"])({{)(.*?)(}})(['"])/g, "''");
                    v = v.replace(/({{)(.*?)(}})/g, 0);
                    eval(v);
                    isValid = true;
                }
                catch(e){
                    isValid = false;
                }

            }
            item.error = !isValid;
            var $this = this.createForm.expValidation;
            $timeout(function() {
                $this.$setValidity('expValidation', $('.invalid-expression:visible').size() == 0);
            });


        }

    }]);
