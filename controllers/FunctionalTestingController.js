MCTApp.controller("FunctionalTestingController", ['$scope', '$http', '$location', '$timeout', function ($scope, $http, $location, $timeout) {
    $scope.MenuService.setActiveMenuMod('mct-mod');
    $scope.hideModal();
    $scope.requests = [];
    $scope.enabledRequests = false;

    $scope.defineCheckBoxes = function (rows) {
        for (var i = 0; i < rows.length; i++) {
            rows[i].checked = false;
        }
    };

    $scope.findFormIds = function (id) {
        httpAjax($http, 'GET', 'devtool/functionalTesting/results/formIdsForResult/' + id, null, function (data) {
            $location.path('devtool/functionalTesting/requests/modify/' + data.commonRequestFormId).search({specificRequestFormId: data.specificRequestFormId});
        })
    };

    $scope.refreshGrid = function () {
        $scope.$broadcast('gridRefreshEvent');
        $scope.updateGrid();
    };

    $scope.runRequest = function (id) {
        $scope.showModal('.ajax-modal');
        $timeout(function () {
            httpAjax($http, 'POST', 'devtool/functionalTesting/requests/run/' + id, null, function (data) {
                $scope.currentTestingResultsData.testingResults = data.testingResults;
                $location.path('devtool/functionalTesting/results/forRequest/' + id);
                $scope.hideModal();
            })
        }, 200);
    };

    $scope.runRequests = function (name) {
        $scope.showModal('.ajax-modal');
        $scope.checkboxName = name;
        var ids = $scope.getIdsFromInputs();
        $timeout(function () {
            postFormGrid($scope, $http, 'devtool/functionalTesting/requests/run', 'requestIds=' + ids, '#resultsGrid')
        }, 200);
    };

    $scope.runAllRequests = function () {
        $scope.showModal('.ajax-modal');
        $timeout(function () {
            postFormGrid($scope, $http, 'devtool/functionalTesting/requests/runAll', null, '#resultsGrid');
        }, 200);
    };

    $scope.showDeleteRequestModal = function (row) {
        $scope.requestDeleteData = {
            id: row.id,
            serviceName: row.serviceName,
            requestMethod: row.requestMethod,
            url: row.url
        };
        $scope.showModal('#delete-request-modal');
    };

    $scope.confirmRequestDeletion = function () {
        $scope.showModal('.ajax-modal', function () {
            postFormGrid($scope, $http, 'devtool/functionalTesting/requests/delete', 'requestIds=' + $scope.requestDeleteData.id, '#requestsGrid', function (data, status, headers, config) {
                angular.element('#resultsGrid').scope().updateGrid();
                $scope.hideModal();
            });
        });
    };

    $scope.showDeleteRequestsModal = function (name) {
        $scope.checkboxName = name;
        $scope.showModal('#delete-requests-modal');
    };

    $scope.confirmRequestsDeletion = function () {
        var ids = $scope.getIdsFromInputs();
        postFormGrid($scope, $http, 'devtool/functionalTesting/requests/delete', 'requestIds=' + ids, '#requestsGrid');
    };

    $scope.showDeleteResultModal = function (row) {
        $scope.resultDeleteData = {
            id: row.id,
            description: row.description,
            timeOfRunning: row.timeOfRequesting,
            status: row.status
        };
        $scope.showModal('#delete-result-modal');
    };

    $scope.confirmResultDeletion = function () {
        postFormGrid($scope, $http, 'devtool/functionalTesting/results/delete', 'resultIds=' + $scope.resultDeleteData.id, '#resultsGrid');
    };

    $scope.showDeleteResultsModal = function (name) {
        $scope.checkboxName = name;
        $scope.showModal('#delete-results-modal');
    };

    $scope.confirmResultsDeletion = function () {
        var ids = $scope.getIdsFromInputs();
        postFormGrid($scope, $http, 'devtool/functionalTesting/results/delete', 'resultIds=' + ids, '#resultsGrid');
    };

    $scope.showDeleteResultsForRequestModal = function (row) {
        $scope.requestDeleteData = {
            id: row.id,
            serviceName: row.serviceName,
            requestMethod: row.requestMethod,
            url: row.url
        };
        $scope.showModal('#delete-results-for-request-modal');
    };

    $scope.confirmResultsForRequestDeletion = function () {
        postFormGrid($scope, $http, 'devtool/functionalTesting/results/deleteForRequests', 'requestIds=' + $scope.requestDeleteData.id, '#resultsGrid');
    };

    $scope.showDeleteResultsForRequestsModal = function (name) {
        $scope.checkboxName = name;
        $scope.showModal('#delete-results-for-requests-modal');
    };

    $scope.confirmResultsForRequestsDeletion = function () {
        var ids = $scope.getIdsFromInputs();
        postFormGrid($scope, $http, 'devtool/functionalTesting/results/deleteForRequests', 'requestIds=' + ids, '#resultsGrid');
    };

    $scope.showDeleteAllResultsModal = function () {
        $scope.showModal('#delete-all-rows-modal');
    };

    $scope.confirmAllRowsDeletion = function () {
        postFormGrid($scope, $http, 'devtool/functionalTesting/results/deleteAll', null, '#resultsGrid');
    };

    $scope.getIdsFromInputs = function () {
        var inputs = $('input[name=' + $scope.checkboxName + ']:checked');
        return $.map(inputs, function (o) {
            return $(o).val();
        });
    };

    $scope.getFirstLine = function (text) {
        if (!text) {
            return text;
        }
        return text.split('\n')[0];
    };

    $scope.checkEnabledRequests = function (cRow, rows) {
        var noOfEnabledReqs = 0;
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            if(row.checked && !row.disabled) {
                for(var j=0; j<row.specificRequestForms.length; j++) {
                    var req = row.specificRequestForms[j];
                    if(!req.disabled) {
                        noOfEnabledReqs++;
                    }
                }
            }

        }
        $scope.enabledRequests = noOfEnabledReqs > 0;
    };

    $scope.showDeleteEnvironmentModal = function (row) {
        $scope.setEnvironmentData(row);
        $scope.showModal('#delete-environment-modal');
    };

    $scope.confirmEnvironmentDeletion = function () {
        postFormGrid($scope, $http, 'devtool/functionalTesting/environments/delete/' + $scope.environmentData.id, null, '#environmentsGrid');
    };

    $scope.checkUsage = function (row) {
        httpAjax($http, 'GET', 'devtool/functionalTesting/environments/checkUsage/' + row.id, null, function (data) {
            $scope.environmentName = row.name;
            $scope.requestNames = data.requestNames;
            if ($scope.requestNames != null) {
                $scope.showModal('#cannot-delete-environment');
            } else {
                $scope.showDeleteEnvironmentModal(row);
            }
        })
    };

    $scope.checkConnection = function (id) {
        $scope.showModal('.ajax-modal');
        postFormGrid($scope, $http, 'devtool/functionalTesting/environments/checkConnection/' + id, null, '#environmentsGrid', function () {
            $timeout(function () {
                $scope.hideModal();
            }, 200);
        });
    };

    $scope.rowCount = function (row) {
        var enabledCount = 0;
        for (var i = 0; i < row.specificRequestForms.length; i++) {
            if (!row.specificRequestForms[i].disabled) {
                enabledCount++;
            }
        }
        return enabledCount + ' / ' + row.specificRequestForms.length;
    };

    $scope.calcOrder = function (index, totalRecords, page, sortOrder) {
        return sortOrder == 'asc'
            ? index + (page - 1) * 10 + 1
            : totalRecords - index - (page - 1) * 10;
    };

    $scope.showSetForAllRequestsModal = function (row) {
        $scope.setEnvironmentData(row);
        $scope.showModal("#set-environment-for-all-requests");
    };

    $scope.confirmSetForAllRequests = function () {
        postFormGrid($scope, $http, 'devtool/functionalTesting/environments/setForAllRequests/' + $scope.environmentData.id, null, '#requestsGrid');
    };

    $scope.setEnvironmentData = function (row) {
        $scope.environmentData = {
            id: row.id,
            name: row.name,
            baseUrl: row.baseUrl,
            dbUrl: row.dbUrl,
            dbPort: row.dbPort,
            dbName: row.dbName
        };
    }

}]);

