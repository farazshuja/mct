var clipboard = null;

MCTApp.controller('DevKitController', ["$scope", "$http", "$location", "$templateCache", "StaticTypes", "Config",function($scope, $http, $location, $templateCache, StaticTypes, Config) {
    $templateCache.removeAll();
    if(clipboard) {
        clipboard.destroy();
    }
    var _path = $location.path();
    if(_path.indexOf('config/create') > 0) {
        var CRMConfigured = $('#devKitConfigured').val();
        if(CRMConfigured != 'false') {
            $location.path("/devKit/track");
        }
    }

    $scope.StaticTypes = StaticTypes;
    $scope.StaticTypes.secureProtocolTypes = keyValToArray($scope.StaticTypes.secureProtocolTypes);
    $scope.MenuService.setActiveMenuMod('mct-mod', 'CRM DevKit');  //set the correct Menu
    $scope.Config = Config;
    $scope.checkFile = true;

    //convert keystore key/value array to json object
    $scope.KeyStores = [];
    for(o in $scope.StaticTypes.keyStoreTypes) {
        $scope.KeyStores.push({
            id: o,
            name: $scope.StaticTypes.keyStoreTypes[o]
        })
    }

    var reqResXMLEditor;
    if(StaticTypes) {
        $scope.trilogyEndPointURL = StaticTypes.trilogyEndPointURL.trilogyEndPointURL;
    }

    if(_path.indexOf('devKit/track') > 0) {
        reqResXMLEditor = CodeMirror.fromTextArea($('#test-response')[0], {
            lineNumbers: true,
            mode:  "text/html",
            readOnly: true
        });
    }

    $scope.submitForm = function(angObj) {
        if(angObj.$valid) {
            $scope.page.isPageLoading = true;
            var form = $('#createForm')[0];
            form.target = 'upload_target';
            $('#createForm input[type=text]').val(function(_, value) {
                return $.trim(value);
            });
            //also convert the response timeout value to proper int, i.e. remove decimal zeros
            var $timeout = $('input[name=responseTime]');
            $timeout.val(parseInt($timeout.val()));
            form.submit();
        }
    }

    $('#upload_target').bind('load', function() {
        var data = $('#upload_target').contents().find('body').text();
        if(data == '') {
            return; //do nothing
        }
        data = JSON.parse(data);
        $scope.page.isPageLoading = false;
        $('#devKitConfigured').val('true');
        if(data.error || data.errors) {
            data.error = data.message;
        }
        else {
            data.success = data.message;
        }
        $location.path("/devKit/track");
        $.growl.message(data);
        $scope.$apply();
    });

    //for test page
    var $reqXML = $('#requestXML');
    $scope.XMLEditorStatus = {
        isEmpty: true,
        isValid: true
    };
    var xmlEditor;
    var responseEditor;
    if($reqXML.size() > 0){
        $scope.StaticTypes.outBoundMessageNames = keyValToArray($scope.StaticTypes.outBoundMessageNames);
        $scope.isXMLEditorEmpty = true;
        xmlEditor = CodeMirror.fromTextArea($('#requestXML')[0],{
            lineNumbers: true,
            mode:  "text/html"
        });
        responseEditor = CodeMirror.fromTextArea($('#test-response-test')[0], {
            lineNumbers: true,
            mode:  "text/html",
            readOnly: true
        });
        xmlEditor.on('change',function(cMirror){
            var v = cMirror.getValue();
            $scope.XMLEditorStatus.isEmpty = $.trim(v) == '';
            $scope.$apply();

        });
        xmlEditor.on('update',function(cMirror){
            $scope.XMLEditorStatus.isValid = $('.cm-error').length < 1;
            $scope.$apply();
        });
        $scope.$watch('messageType', function(v) {
            if(!v) return;
            $scope.requestXML = '';
            $.ajax({
                url: 'devKit/getSampleXML?messageType=' + v,
                type:"GET",
                success:function (data, a, b) {
                    if(data && data.indexOf('login.js') != -1) {
                        handleSessionTimeout(data);
                    }
                    else {
                        xmlEditor.setValue(data);
                    }
                },
                error: function(error) {
                    handleSessionTimeout(error.responseText);
                }
            });

        });
    }
    if($('.copy-to-clip').size() > 0) {
        $('.copy-to-clip').tooltip({placement: 'bottom',trigger: 'manual'});
        clipboard = new Clipboard('.copy-to-clip', {
            text: function(trigger) {
                $('.copy-to-clip').tooltip('show');
                if($('#requestXML').size() > 0) {   //test page
                    return responseEditor.getValue();
                }
                else    // track page
                    return reqResXMLEditor.getValue();
            }
        });
        $('.copy-to-clip').hover(null, function() {
            $(this).tooltip('hide');
        });
    }

    $scope.operation1 = {msg: null};
    $scope.runTest = function(angObj) {
        if(angObj.$valid) {
            var d = {
              messageType: StaticTypes.outBoundMessageTypes[$('input[name=messageType]').val()],
                requestXML: xmlEditor.getValue()
            };
            $scope.showIndicator();
            $.ajax({
                url: 'devKit/test',
                type:"POST",
                data: d,
                dataType: 'json',
                success:function (data) {
                    $scope.hideIndicator();
                    var res = data.responseContent ? data.responseContent : '';
                    if(data.success) {
                        $scope.operation1 = {
                            msg: 'Status: Successful',
                            success: true
                        }
                    }
                    else {
                        $scope.operation1 = {
                            msg: 'Status: Failed',
                            success: false
                        }
                    }
                    $scope.$apply();
                    $scope.showModal('#response-modal');
                    res = vkbeautify.xml(res);
                    //if its broken then remove undefined
                    res = res.replace(/>undefined</g, '><');    //this is required to patch vkbeautify plugin
                    responseEditor.setValue(res);
                    setTimeout(function() {
                        responseEditor.refresh();
                    }, 700);
                },
                error: function(error) {
                    $scope.hideIndicator();
                    handleSessionTimeout(error.responseText);
                }
            });
        }
    }

    $scope.showRequestResponseModal = function(row, isReq) {
        var url = isReq ? 'devKit/trackingInfo/request' : 'devKit/trackingInfo/response';
        url += '?entityId=' + row.entityId + '&requestSentByDevKit=' + row.requestSentByDevKit + '&ts=' + new Date().getTime();
        $scope.showIndicator();
        $.ajax({
            url: url,
            type:"GET",
            dataType: 'json',
            success:function (data) {
                $scope.hideIndicator();
                var res;
                if(!isReq) {
                    res = data.responseContent ? data.responseContent : '';
                    $('#response-modal .modal-title').text('Response');
                }
                else {
                    res = data.requestContent ? data.requestContent : '';
                    $('#response-modal .modal-title').text('Request');
                }

                $scope.showModal('#response-modal');
                res = vkbeautify.xml(res);
                //if its broken then remove undefined
                res = res.replace(/>undefined</g, '><');    //this is required to patch vkbeautify plugin
                reqResXMLEditor.setValue(res);
                setTimeout(function() {
                    reqResXMLEditor.refresh();
                }, 700);
            },
            error: function(error) {
                $scope.hideIndicator();
                handleSessionTimeout(error.responseText);
            }
        });
    };

    if (!isClipBoardSupported()) {
        $('.copy-to-clip').hide();
    }

    function handleSessionTimeout(template) {
        if(template.indexOf('login.js') != -1) {
            $scope.showModal('.session-modal');
        }
    }
}]);
