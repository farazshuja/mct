angular.module('MCTRoutes', []).config(['$routeProvider', function($routeProvider){
    $routeProvider.when('/', {
            templateUrl : 'home/home',
            controller : [ '$scope', '$location', function($scope, $location) {
                $scope.MenuService.setActiveMenuMod('mct-mod');
                //in case of CRM user redirect user to
                var CRMConfigured = $('#devKitConfigured').val();
                var isCrmUser = $('#isCrmUser').val();
                if(isCrmUser == 'true') {
                    if(CRMConfigured == 'false') {
                        $location.path('devKit/config/create');
                    }
                    else {
                        $location.path('devKit/track');
                    }
                }
            }]
        })
        .when('/devKit/config/create', getRoute('devKit/config/create', 'DevKitController', {
            Config: null,
            StaticTypes: 'devKit/types',
            Scripts: ['codemirror/lib/codemirror.js','codemirror/mode/sql.js','codemirror/mode/xml.js']
        }))
        .when('/devKit/config/modify', getRoute('devKit/config/modify', 'DevKitController', {
            Config: 'devKit/config/getCRMDevKitConfig',
            StaticTypes: 'devKit/types',
            Scripts: ['codemirror/lib/codemirror.js','codemirror/mode/sql.js','codemirror/mode/xml.js']
        }))
        .when('/devKit/track', getRoute('devKit/track', 'DevKitController', {
            Config: null,
            StaticTypes: 'devKit/types',
            Scripts: ['codemirror/lib/codemirror.js','codemirror/mode/sql.js','codemirror/mode/xml.js']
        }))
        .when('/devKit/test', getRoute('devKit/test', 'DevKitController', {
            Config: null,
            StaticTypes: 'devKit/types',
            Scripts: ['codemirror/lib/codemirror.js','codemirror/mode/sql.js','codemirror/mode/xml.js']
        }))
        .otherwise({
            redirectTo : '/'
        });
}]);