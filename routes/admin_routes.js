angular.module('MCTRoutes', []).config(['$routeProvider', function($routeProvider){
    $routeProvider.when('/', {
            templateUrl : 'home/home',
            controller : [ '$scope', '$location', function($scope, $location) {
                $scope.MenuService.setActiveMenuMod('mct-mod');
            }]
        })
        .when('/users/list', getRoute('system/users/list', 'UsersController', null, 'vm'))
        .when('/users/create', getRoute('system/users/create', 'UserEditController', {
                grpList: 'system/getGroupList',
                User: null
            }, 'vm'
        ))
        .when('/users/modify/:id', getRoute('system/users/modify/:id', 'UserEditController', {
                grpList : 'system/getGroupList',
                User : 'system/getUserList/:id'
            }, 'vm'
        ))
        .when('/groups/list', getRoute('system/groups/list', "GroupsController", null, 'vm'))
        .when('/groups/create', getRoute('system/groups/create', "GroupEditController", {
                Group: null,
                SelectedUsers: null,
                Users : 'system/getUserList/:id'
            }, 'vm'
        ))
        .when('/groups/modify/:id', getRoute('system/groups/modify/:id', "GroupEditController", {
                Group : 'system/getGroupList/:id',
                SelectedUsers : 'system/groups/getUsersForGroup/:id',
                Users : 'system/getUserList'
            }, 'vm'
        ))
        .when('/jobs/alews', getRoute('jobs/alews', 'ALEWSController', {
            jsExpression : 'jobs/alews/jsExpression'
        }))
        .when('/jobs/IAStartSingleProcessing', getRoute('jobs/IAStartSingleProcessing', 'IAStartSingleProcessingController'))
        .when('/jobs/IAExecutionControl', getRoute('jobs/IAExecutionControl', 'IAExecutionControlController'))
        .when('/jobs/commonJobsSingleRun', getRoute('jobs/commonJobsSingleRun', 'CommonJobsSingleRunController'))
        .when('/jobs/commonJobsControlRun', getRoute('jobs/commonJobsControlRun', 'CommonJobsControlRunController'))
        .when('/jobs/LPSingleFeedGeneration', getRoute('jobs/LPSingleFeedGeneration', 'LPSingleFeedGenerationController'))
        .when('/jobs/LPFeedGenerationControl', getRoute('jobs/LPFeedGenerationControl', 'LPFeedGenerationControlController'))
        .when('/scriptExecution/sales', getRoute('scriptExecution/sales', 'ScriptExecutionController'))
        .when('/scriptExecution/sales/create', getRoute('scriptExecution/sales/create', 'ScriptExecutionCreateEditController', {
                script: null,
                Scripts: ['codemirror/lib/codemirror.js','codemirror/mode/sql.js','codemirror/mode/xml.js']
            }
        ))
        .when('/scriptExecution/sales/edit/:id', getRoute('scriptExecution/sales/edit/:id', 'ScriptExecutionCreateEditController', {
                script : 'scriptExecution/sales/getScript/:id',
                Scripts: ['codemirror/lib/codemirror.js','codemirror/mode/sql.js','codemirror/mode/xml.js']
            }
        ))
        .when('/scriptExecution/sales/execute', getRoute('scriptExecution/sales/execute', 'ScriptExecutionController'))
        .when('/devtool', getRoute('devtool', 'DevToolController'))
        .when('/devtool/files', getRoute('devtool/files', 'IAFillFilesController'))
        .when('/devtool/hhid', getRoute('devtool/hhid', 'DevToolController'))
        .when('/devtool/pom', getRoute('devtool/pom', 'DevToolController'))
        .when('/devtool/manifest', getRoute('devtool/manifest', 'DevToolController'))
        .when('/devtool/updateGeo', getRoute('devtool/updateGeo', 'UpdateGeoController', {
            status: 'devtool/updateGeo/status'
        }))
        .when('/devtool/inventories', getRoute('devtool/inventories', 'DevToolController'))
        .when('/devtool/dealer/updateDealerGeo', getRoute('devtool/dealer/updateDealerGeo', 'DevToolController'))
        .when('/devtool/dealer/:id/updateGeo', { templateUrl : function(param) {
            return 'devtool/dealer/'+param.id+'/updateGeo';
        }})
        .when('/devtool/restartInventoryJob/:id', getRoute('devtool/restartInventoryJob/:id', 'DevToolController'))
        .when('/devtool/callRecords', getRoute('devtool/callRecords', 'DownloadCallRecordController'))
        .when('/devtool/mapCallToLead', getRoute('devtool/mapCallToLead', 'MapCallToLeadController'))
        .when('/devtool/cacheUtil', getRoute('devtool/cacheUtil', 'CacheController', {
            CacheEntries: 'devtool/getAllHazelCacheEntries'
        }))
        .when('/devtool/pullProspectsFromZoho', getRoute('devtool/pullProspectsFromZoho', 'DevToolController'))
        .when('/devtool/remoteFileHistory', getRoute('devtool/remoteFileHistory', 'UpdateRemoteFileHistory'))
        .when('/devtool/leadNurturing', getRoute('devtool/leadNurturing', 'LeadNurturingController',{
            NurturingPrograms: 'metaData/nurturing/getNurturingProgramsList',
            DataConsumers: 'metaData/dataConsumers/getDataConsumers'
        }))
        .when('/devtool/syncDealer', getRoute('devtool/syncDealer', 'DevToolController'))
        .when('/devtool/syncDealer/:id', { templateUrl : function(param) {
            return 'devtool/syncDealer/'+param.id;
        }})
        .when('/devtool/query/lead', getRoute('devtool/query/lead', 'CheckQueryTimeController'))
        .when('/devtool/functionalTesting', getRoute('devtool/functionalTesting', 'FunctionalTestingController'))
        .when('/devtool/functionalTesting/requests/create', getRoute('devtool/functionalTesting/requests/create', 'TestingRequestCreateModifyController', {
            CommonRequestForm: null,
            Environments: 'devtool/functionalTesting/environments/list',
            ContentTypes: 'devtool/functionalTesting/requests/types'
        }))
        .when('/devtool/functionalTesting/requests/modify/:id', getRoute('devtool/functionalTesting/requests/modify/:id', 'TestingRequestCreateModifyController', {
            CommonRequestForm: 'devtool/functionalTesting/requests/getRequest/:id',
            Environments: 'devtool/functionalTesting/environments/list',
            ContentTypes: 'devtool/functionalTesting/requests/types'
        }))
        .when('/devtool/functionalTesting/environments/create', getRoute('devtool/functionalTesting/environments/create', 'EnvironmentConfigController', {
            Environment: null
        }))
        .when('/devtool/functionalTesting/environments/modify/:id', getRoute('devtool/functionalTesting/environments/modify/:id', 'EnvironmentConfigController', {
            Environment: 'devtool/functionalTesting/environments/getEnvironment/:id'
        }))
        .when('/devtool/functionalTesting/results/:id', getRoute('devtool/functionalTesting/results/:id', 'TestingResultController', {
            TestingResult: 'devtool/functionalTesting/results/getResult/:id',
            ContentTypes: 'devtool/functionalTesting/requests/types',
            Scripts: ['codemirror/lib/codemirror.js','diff_match_patch.js','codemirror/lib/addon/merge/merge.js', 'codemirror/mode/xml.js', 'codemirror/mode/javascript.js']
        }))
        .when('/devtool/functionalTesting/results/forRequest/:id', getRoute('devtool/functionalTesting/results/forRequest/:id', 'ResultsForRequestController', {
            TestingResults: 'devtool/functionalTesting/results/forRequest/list/:id'
        }))
        .when('/devtool/addNewCRMsFromDM', getRoute('devtool/addNewCRMsFromDM', 'DevToolController'))
        .when('/devtool/processingFailedLeads', getRoute('devtool/processingFailedLeads', 'DevToolController'))
        .when('/devtool/sendMktTemplates', getRoute('devtool/sendMktTemplates', 'MktTemplateController',{
            OEMs: 'metaData/OEM/getOEMsLite'
        }))
        .when('/devtool/reloadColumnMetadata', getRoute('devtool/reloadColumnMetadata', 'DevToolController'))
        .when('/devtool/cronExecution', getRoute('devtool/cronExecution', 'CronExecutionController', {
            CronList: 'devtool/cronList'
        }))
        .when('/devtool/configKeyValueProps', getRoute('devtool/configKeyValueProps', 'ConfigKeyValuePropsController', {
            ConfigKeyValuePropsList: 'devtool/configKeyValuePropsList'
        }))
        .when('/devtool/leadCallRecords/migrate', getRoute('devtool/leadCallRecords/migrate', 'MigrateCallRecordsController'))
        .when('/devtool/apiHealthCheck', getRoute('devtool/apiHealthCheck', 'ApiHealthCheckController', {
            HealthCheckProperties: 'devtool/getHealthCheckProperties'
        }))
        .when('/devtool/rawXmlLookUp', getRoute('devtool/rawXmlLookUp', 'RawXmlLookUpController', {
        	Scripts: ['codemirror/lib/codemirror.js','codemirror/mode/sql.js','codemirror/mode/xml.js']	
        	}
        ))
        .when('/devtool/rawMessageLookUp', getRoute('devtool/rawMessageLookUp', 'RawMessageLookUpController', {
        	Scripts: ['codemirror/lib/codemirror.js','codemirror/mode/sql.js','codemirror/mode/xml.js']	
        	}
        ))
        .when('/devtool/suspendedOmh', getRoute('devtool/suspendedOmh', 'SuspendedOmhController', {
            OutDataConnectors: 'bidirectional/odc/getODCList',
            DataSenders: 'bidirectional/dataSender/getDataSenders'
        }))
        .when('/metaData/OEM/list', getRoute('metaData/OEM/list', 'OEMController', null, 'vm'))
        .when('/metaData/OEM/create', getRoute('metaData/OEM/create', 'OEMEditController', {
                StaticTypes: 'metaData/OEM/types',
                OemData: null
            }, 'vm'
        ))
        .when('/metaData/OEM/modify/:id',
            getRoute('metaData/OEM/modify/:id', 'OEMEditController', {
                    StaticTypes: 'metaData/OEM/types',
                    OemData: 'metaData/OEM/getOEM/:id'
                }, 'vm'
            ))
        .when('/metaData/dataSources/metaDataConfig', getRoute('metaData/dataSources/metaDataConfig', 'MetaDataController'))
        .when('/metaData/dataProviders/list', getRoute('metaData/dataProviders/list', 'DataProvidersController'))
        .when('/metaData/dataProviders/create',
            getRoute('metaData/dataProviders/create', 'DataProvidersEditController', {
                    StaticTypes: 'metaData/dataProviders/types',
                    OEMLeadCategoryMappingList: 'metaData/dataProviders/getOemLeadCategories',
                    DataProvider: null,
                    CrmCodes : 'metaData/crmCodes/getCrmCodes',
                    eventCategories: 'metaData/crmCodes/eventCategories',
                    CRMUsers: 'system/groups/getUsersForCRMUserGroup',
                    MVMTypes: 'metaData/MVM/getTypes'
                }
            ))
        .when('/metaData/dataProviders/modify/:id',
            getRoute('metaData/dataProviders/modify/:id', 'DataProvidersEditController', {
                StaticTypes: 'metaData/dataProviders/types',
                OEMLeadCategoryMappingList: 'metaData/dataProviders/getOemLeadCategoriesMerged/:id',
                DataProvider: 'metaData/dataProviders/getDataProvider/:id',
                CrmCodes : 'metaData/crmCodes/getCrmCodes',
                eventCategories: 'metaData/crmCodes/eventCategories',
                CRMUsers: 'system/groups/getUsersForCRMUserGroup',
                MVMTypes: 'metaData/MVM/getTypes'
            }))
        .when('/metaData/dataConsumers/list', getRoute('metaData/dataConsumers/list', 'DataConsumerController'))
        .when('/metaData/dataConsumers/create', getRoute('metaData/dataConsumers/create', 'DataConsumerCreateModifyController', {
                DataConsumer : null,
                OEMList: 'metaData/OEM/getOemList',
                DataProviders: 'metaData/dataProviders/getDataPrvidersList',
                Types: 'metaData/dataConsumers/types',
                lcList: 'metaData/dataSources/metaDataConfig/allLeadCategories',
                ltList: 'metaData/dataSources/metaDataConfig/allLeadTypes',
                NurturingPrograms: 'metaData/nurturing/getNurturingProgramsList'
            }
        ))
        .when('/metaData/dataConsumers/modify/:id', getRoute('metaData/dataConsumers/modify/:id', 'DataConsumerCreateModifyController', {
                DataConsumer : 'metaData/dataConsumers/getDataConsumer/:id',
                OEMList: 'metaData/OEM/getOemList',
                DataProviders: 'metaData/dataProviders/getDataPrvidersList',
                Types: 'metaData/dataConsumers/types',
                lcList: 'metaData/dataSources/metaDataConfig/allLeadCategories',
                ltList: 'metaData/dataSources/metaDataConfig/allLeadTypes',
                NurturingPrograms: 'metaData/nurturing/getNurturingProgramsList'
            }
        ))
        .when('/metaData/processes/jobScheduling/list', getRoute('metaData/processes/jobScheduling/list', 'JobSchedulingController', {
            DataConsumers: 'metaData/processes/jobScheduling/getDataConsumers'
        }))
        .when('/metaData/processes/jobScheduling/create', getRoute('metaData/processes/jobScheduling/create', 'JobSchedulingCreateController', {
            DataConsumers: 'metaData/processes/jobScheduling/getDataConsumers'
        }))
        .when('/metaData/processes/jobScheduling/modify/:id', getRoute('metaData/processes/jobScheduling/modify/:id', 'JobSchedulingModifyController', {
            DataConsumers: 'metaData/processes/jobScheduling/getDataConsumers/:id',
            ScheduledJob: 'metaData/processes/jobScheduling/getDataConsumerJob/:id'
        }))
        .when('/metaData/processes/dataConsumerHistory/list', getRoute('metaData/processes/dataConsumerHistory/list', 'JobConsumptionHistoryController', {
            StaticTypes: 'metaData/dataProviders/types'
        }))
        .when('/metaData/processes/dataConsumerHistory/list/:id', getRoute('metaData/processes/dataConsumerHistory/list/:id', 'JobConsumptionHistoryDetailedController'))
        .when('/metaData/nurturing/list', getRoute('metaData/nurturing/list', 'NurturingController'))
        .when('/metaData/nurturing/create', getRoute('metaData/nurturing/create', 'NurturingCreateController', {
            StaticTypes: 'metaData/nurturing/types',
            NurturingProgram: null
        }))
        .when('/metaData/nurturing/modify/:id', getRoute('metaData/nurturing/modify/:id', 'NurturingCreateController', {
            StaticTypes: 'metaData/nurturing/types',
            NurturingProgram: 'metaData/nurturing/getNurturingProgram/:id'
        }))
        .when('/metaData/crmCodes/list', getRoute('metaData/crmCodes/list', 'CRMCodesController',{
            eventCategories: 'metaData/crmCodes/eventCategories'
        }))
        .when('/metaData/crmCodes/create',
            getRoute('metaData/crmCodes/create', 'CRMCodesCreateController', {
                    eventCategories: 'metaData/crmCodes/eventCategories'
                }
            ))
        .when('/metaData/crmCodes/modify/:id',
            getRoute('metaData/crmCodes/modify/:id', 'CRMCodesModifyController', {
                eventCategories: 'metaData/crmCodes/eventCategories',
                crmCode : 'metaData/crmCodes/getCrmCode/:id'
            }))
        .when('/bidirectional/messageTypes/list', getRoute('bidirectional/messageTypes/list', 'MessageTypeController'))
        .when('/bidirectional/messageTypes/create', getRoute('bidirectional/messageTypes/create', 'MessageTypeCreateController', {
            MessageType: null
        }))
        .when('/bidirectional/messageTypes/modify/:id', getRoute('bidirectional/messageTypes/modify/:id', 'MessageTypeCreateController', {
            MessageType: 'bidirectional/messageTypes/getMessageType/:id'
        }))
        .when('/bidirectional/idc/list', getRoute('bidirectional/idc/list', 'InDataConnectorController'))
        .when('/bidirectional/idc/create', getRoute('bidirectional/idc/create', 'InDataConnectorCreateModifyController', {
            InDataConnector: null,
            InMessageTypes: 'bidirectional/messageTypes/getInMsgTypes',
            StaticTypes: 'bidirectional/idc/types',
            DataConsumers: 'metaData/dataConsumers/getDataConsumersLite',
            OutDataConnectors: 'bidirectional/odc/getODCList'
        }))
        .when('/bidirectional/idc/modify/:id', getRoute('bidirectional/idc/modify/:id', 'InDataConnectorCreateModifyController', {
            InDataConnector: 'bidirectional/idc/getIDC/:id',
            InMessageTypes: 'bidirectional/messageTypes/getInMsgTypes',
            StaticTypes: 'bidirectional/idc/types',
            DataConsumers: 'metaData/dataConsumers/getDataConsumersLite',
            OutDataConnectors: 'bidirectional/odc/getODCList'
        }))
        .when('/bidirectional/dataSender/list', getRoute('bidirectional/dataSender/list', 'DataSenderController'))
        .when('/bidirectional/dataSender/create', getRoute('bidirectional/dataSender/create', 'DataSenderCreateController', {
            DataSender: null,
            StaticTypes: 'bidirectional/dataSender/types',
            MVMTypes: 'metaData/MVM/getTypes'
        }))
        .when('/bidirectional/dataSender/modify/:id', getRoute('bidirectional/dataSender/modify/:id', 'DataSenderCreateController', {
            DataSender: 'bidirectional/dataSender/getDataSender/:id',
            StaticTypes: 'bidirectional/dataSender/types',
            MVMTypes: 'metaData/MVM/getTypes'
        }))
        .when('/bidirectional/odc/list', getRoute('bidirectional/odc/list', 'OutDataConnectorController'))
        .when('/bidirectional/odc/create', getRoute('bidirectional/odc/create', 'OutDataConnectorCreateModifyController', {
            OutDataConnector: null,
            OutMessageTypes: 'bidirectional/messageTypes/getOutMsgTypes',
            DataSenders: 'bidirectional/dataSender/getDataSenders',
            StaticTypes: 'bidirectional/odc/types'
        }))
        .when('/bidirectional/odc/modify/:id', getRoute('bidirectional/odc/modify/:id', 'OutDataConnectorCreateModifyController', {
            OutDataConnector: 'bidirectional/odc/getODC/:id',
            OutMessageTypes: 'bidirectional/messageTypes/getOutMsgTypes',
            DataSenders: 'bidirectional/dataSender/getDataSenders',
            StaticTypes: 'bidirectional/odc/types'
        }))
        .when('/bidirectional/crmDsMapping/list', getRoute('bidirectional/crmDsMapping/list', 'CrmDsMappingController', {
            OEMsLite: 'metaData/OEM/getOEMsLite',
            DataSendersLite: 'bidirectional/dataSender/getDataSendersLite'
        }))
        .when('/bidirectional/crmDsMapping/create', {
            templateUrl: 'bidirectional/crmDsMapping/create',
            controller: 'CrmDsMappingEditController',
            resolve: {
                CRM: ['CRMDTO', function(CRMDTO) {
                    return CRMDTO.loadAll();
                }]
            }
        })
        .when('/bidirectional/crmDsMapping/modify/:id', {
            templateUrl: function(param) {
                return 'bidirectional/crmDsMapping/modify/' + param.id;
            },
            controller: 'CrmDsMappingEditController',
            resolve: {
                CRM: ['CRMDTO', '$route', function(CRMDTO, $route) {
                    var id = $route.current.params.id;
                    return CRMDTO.loadAll(id);
                }]
            }
        })
        .otherwise({
            redirectTo : '/'
        });
}]);