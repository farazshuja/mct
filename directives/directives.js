/*
* Use this directive to validate a unique value from server
* Usage: check-unique="/url/to/check/unique?name={{model}}" ignore-unique-value="{{model}}"
* Error: <label ng-show="createForm.name.$error.unique && !createForm.name.$error.required" class="error">Program name already exists, choose a different name</label>
* Note: use ignore attribute on modify page to ignore the current valid value
 */
MCTApp.directive('checkUnique', ['$http', function($http) {
    return {
        require: 'ngModel',
        link: function(scope, ele, attrs, c) {
            var origonalVal = attrs.ignoreUniqueValue;
            
            attrs.$observe('checkUnique', function(postToUrl) {

                var aEle = angular.element(ele);
                var org = aEle.attr('unique-original');
                if(org && org == ele.val()) {
                    c.$setValidity('unique', true);
                    return;
                }

                if($.trim(ele.val()) == $.trim(origonalVal) && origonalVal != ''){
                    c.$setValidity('unique', true);
                    return;
                }

                $http({
                    method: 'GET',
                    url: postToUrl
                }).then(function(response) {
                    c.$setValidity('unique', response.data);
                }, function() {
                    c.$setValidity('unique', false);
                });
            });
        }
    }
}])
/*
    Simpler version of check-unique
    No need to set any particular attribute value to ignore, it just ignores ngModel value
 */
.directive('isUnique', ['$http', function($http) {
    return {
        require: 'ngModel',
        scope: {
            ngModel: '='
        },
        link: function(scope, ele, attrs, c) {
            var valToIgnore = scope.ngModel;

            attrs.$observe('isUnique', function(postToUrl) {

                //do not send ajax request if mode is empty or undefined
                if(!scope.ngModel) {
                    c.$setValidity('unique', true);
                    return;
                }

                //if model value is same as of value-to-ignore i.e original value
                if(scope.ngModel == valToIgnore) {
                    c.$setValidity('unique', true);
                    return;
                }

                $http({
                    method: 'GET',
                    url: postToUrl
                }).then(function(response) {
                    c.$setValidity('unique', response.data);
                }, function() {
                    c.$setValidity('unique', false);
                });
            });
        }
    }
}])
.directive('validFile',function(){
    return {
        require:'ngModel',
        link:function(scope,el,attrs,ctrl){
            var doValidation = attrs.validFile == 'true';
            //change event is fired when file is selected
            el.bind('change',function(){
                var isValid = false;
                if(!doValidation && el.val() == '') {
                    isValid = true;
                }
                else {
                    var filename = el.val().replace(/^.*[\\\/]/, '');
                    isValid = el.val() != '' && filename.length <= 64;
                }

                ctrl.$setValidity('validFile', isValid);
                scope.$apply(function(){
                    ctrl.$setViewValue(el.val());
                    ctrl.$render();
                });
            });

            if(!doValidation)
                return;

            ctrl.$setValidity('validFile', el.val() != '');
        }
    }
})
.directive('checkJavaScriptExpression', function() {
	return {
        require: 'ngModel',
        link: function(scope, ele, attrs, c) {
            scope.$watch(attrs.ngModel, function() {
                var isValid = false;
                try{
                    var v = ele.val();
                    v = v.replace(/(['"])({{)(.*?)(}})(['"])/g, "''");
                    v = v.replace(/({{)(.*?)(}})/g, 0);
                    eval(v);
                    isValid = true;
                }
                catch(e){
                    isValid = false;
                }
                c.$setValidity('validExpression', isValid);
            });

        }
    }
})
.directive('warnUnique', ['$http', function($http) {
    return {
        require: 'ngModel',
        scope: {
          ngModel: '='
        },
        link: function(scope, ele, attrs, c) {
            attrs.$observe('warnUnique', function(getToUrl) {
                if(!scope.ngModel) {
                    c.isUnique = true;
                    return;
                }

                $http({
                    method: 'GET',
                    url: getToUrl
                }).then(function success(response) {
                    c.isUnique = response.data;
                }, function error() {
                    c.isUnique = false;
                });
            });
        }
    }
}])
.directive('equalsTo', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            var sc = scope;
            scope.$watch(attrs.ngModel, function() {
                var eqCtrl = scope.$eval(attrs.equalsTo);
                if (ctrl.$viewValue===eqCtrl.$viewValue || (!!!ctrl.$viewValue && !!!eqCtrl.$viewValue)) {
                    ctrl.$setValidity('equalsTo', true);
                    eqCtrl.$setValidity('equalsTo', true);
                } else {
                    ctrl.$setValidity('equalsTo', false);
                    eqCtrl.$setValidity('equalsTo', false);
                }
            });
        }
    };
})
.directive('clickToList', function() {
    return {
        link: function(scope, elm, attrs, ctrl) {
            elm.bind('click', function(){
                //create a select box and replace it with this label
                var $elem = $(this);
                $elem.hide();
                var html = '<select class="form-control clickToListSelect">';
                angular.forEach(scope[attrs.clickToList], function(v,k){
                    if(attrs.clickToList == 'transList'){
                        var selected = v.inventoryTransformation.transformationRepresentation == $elem.text() ? 'selected="selected"' : '';
                        html += '<option ' + selected + ' value="' + v.inventoryTransformation.value + '">' + v.inventoryTransformation.transformationRepresentation + '</option>'
                    } else {
                        var selected = v.name == $elem.text() ? 'selected="selected"' : '';
                        html += '<option ' + selected + ' value="' + v.id + '">' + v.name + '</option>'
                    }
                });
                html += '</select>';
                $(html).insertAfter(this);
            });
        }
    };
})
    //obsolete/ remove this directive
.directive('requiredLeadPrice', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            var sc = scope;
            scope.$watch(attrs.ngModel, function(newVal) {
                if(!angular.isDefined(newVal) || newVal === null || newVal === ""){
                    ctrl.$setValidity('requiredLeadPrice', false);
                    return;
                }
                newVal = newVal.toString();
                var isLengthValid = newVal.indexOf('.') != -1 ? 7 : 6;
                isLengthValid = newVal.length <= isLengthValid;
                if (isNaN(Number(newVal)) || Number(newVal) < 0 || !isLengthValid) {
                    ctrl.$setValidity('requiredLeadPrice', false);
                } else {
                    ctrl.$setValidity('requiredLeadPrice', true);
                }
            });
        }
    };
})
.directive('requiredPositiveNo', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            var sc = scope;
            scope.$watch(attrs.ngModel, function(newVal) {
                if(!angular.isDefined(newVal) || newVal === null || newVal === ""){
                    ctrl.$setValidity('requiredPositiveNo', false);
                    return;
                }
                newVal = newVal.toString();
                if (isNaN(Number(newVal)) || Number(newVal) < 0) {
                    ctrl.$setValidity('requiredPositiveNo', false);
                } else {
                    ctrl.$setValidity('requiredPositiveNo', true);
                }
            });
        }
    };
})
.directive('requiredPositiveInt', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            scope.$watch(attrs.ngModel, function(newVal) {
                if(!angular.isDefined(newVal) || newVal === null || newVal === ""){
                    ctrl.$setValidity('requiredPositiveInt', false);
                    return;
                }
                newVal = newVal.toString();
                var isInt = newVal % 1 === 0;
                var len = newVal.length;
                var maxInt = '2147483647';
                var i = 0;
                if (len < 10) {
                    for (i = 0; i < 10 - len; i++) {
                        newVal = '0' + newVal;
                    }
                } else if (len > 10) {
                    for (i = 0; i < len - 10; i++) {
                        maxInt = '0' + maxInt;
                    }
                }
                ctrl.$setValidity('requiredPositiveInt', isInt && parseInt(newVal) >= 0 && newVal <= maxInt);
            });
        }
    };
})
.directive('maximumLength', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            var len = attrs.maximumLength;
            var $ele = $(elm);
            scope.$watch(attrs.ngModel, function(newVal) {
                if (newVal && newVal.length > len) {
                    ctrl.$setValidity('maximumLength', false);
                    $ele.parent().children('.max-len-error').remove();
                    $('<label class="error max-len-error">Max ' + len + 'characters allowed</label>').insertAfter($ele);
                } else {
                    $ele.parent().children('.max-len-error').remove();
                    ctrl.$setValidity('maximumLength', true);
                }
            });
        }
    };
})
.directive('allowDisabledOptions',['$timeout', function($timeout) {
    return function(scope, element, attrs) {
        var ele = element;
        var scopeObj = attrs.allowDisabledOptions;
        $timeout(function(){
            var DS = ele.scope()[scopeObj];
            var options = ele.children();
            if(DS) {
                for(var i=0;i<DS.length;i++) {
                    if(!DS[i].enabled) {
                        options.eq(i).attr('disabled', 'disabled');
                    }
                }
            }
        });
    }
}])
.directive('chosen',['$timeout', function($timeout) {
    return {
        scope: {
            selected: '=chosen',    //array of object from which needs to get the selected elements
            selectedProp: '@',      //property based on which need to find which option should be selected
            ngModel: '='
        },
        link: function (scope, element, attrs, ngModel) {
            $timeout(function() {
                var ids = [];
                if(scope.selected) {
                    for(var i=0;i<scope.selected.length;i++) {
                        ids.push(scope.selected[i][scope.selectedProp]);
                    }
                }
                scope.ngModel = ids;
                scope.$apply();
                $(element).chosen();
            });
        }
    }
}])
.directive('bindOnce', function() {
    return {
        scope: true,
        link: function(scope, ele ) {
            setTimeout(function() {
                scope.$destroy();
                ele.removeClass('ng-binding ng-scope');
            }, 0);
        }
    }
})
.directive('mctEmail', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            var allowMultiple = attrs.mctEmail == 'multiple';
            function validate(value) {
                var emailReg = new RegExp("^(\\s*|^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\\-+)|([A-Za-z0-9]+\\.+)|([A-Za-z0-9]+\\++))*[A-Za-z0-9]+@((\\w+\\-+)|(\\w+\\.))*\\w{1,63}\\.[a-zA-Z]{2,6}$)$");
                var emailArray = [];
                var isValid = true;
                if(allowMultiple){
                    emailArray = value ? value.split(',') : [''];
                }
                else {
                    emailArray.push(value);
                }
                for(var i=0;i<emailArray.length;i++) {
                    var val = $.trim(emailArray[i]);
                    if(emailReg.test(val)){
                        isValid = true;
                    }
                    else {
                        isValid = false;
                        break;
                    }
                }
                ngModel.$setValidity('mctEmail', isValid);
            }

            scope.$watch( function() {
                return ngModel.$viewValue;
            }, validate);


        }
    };
})
.directive('lpsModal', function() {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            lpsModalId: '@',
            lpsModalTitle: '@',
            onOkClick: '&',
            onCancelClick: '&'
        },
        compile: function(tEle, tAttrs, transcludeFn) {

            var $oEle = $(tEle.context);
            var $body = $(tEle.context).find('.modal-body');

            //remove close button if backdrop is disabled
            if(tAttrs.backdrop && tAttrs.backdrop == "static") {
                tEle.find('.modal-header .close').remove();
            }
            tEle.find('.modal-body').html($body.html());
            tEle.removeClass('hide');

            var $footer = tEle.find('.modal-footer');
            var $btns = $oEle.find('button');
            $btns.each(function(i,o){
                var $o = $(o);
                var c = $o.attr('class');
                var t = $o.html();
                $o.html('');
                $o.removeClass().addClass('btn btn-primary ok-btn');
                $o.append('<span class="mct-icons ' + c + '"></span>');
                $o.append('<span class="btn-title">' + t + '</span>');
                $footer.append($o);
            });
        },
        template: '<div id="{{lpsModalId}}" class="modal">' +
            '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                    '<div class="modal-header">' +
                        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>' +
                        '<h1 class="modal-title">{{lpsModalTitle}}</h1>' +
                    '</div>' +
                    '<div class="modal-body">' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'
    }
})
.directive('repeatDone', function() {
    return function(scope, element, attrs) {
        if (scope.$last) { // all are rendered
            scope.$eval(attrs.repeatDone);
        }
    }
})
.directive('gridHeader', function() {
    return {
        restrict: 'A',
        replace: true,
        scope: false,
        compile: function(tEle, tAttrs, transcludeFn) {
            var h3 = tEle.find('h3');
            var temp = h3.html();
            temp = temp.replace('xxxx', tAttrs.gridHeader);
            h3.html(temp);

            //do not generate op messge
            if(tAttrs.disableOpMsg) {
                tEle.find('.opMsg').remove();
            }
        },
        template: '<div class="grid-header">' +
            '<h3>Showing {{grid.data.records}} xxxx</h3>' +
            '<div class="pull-right">' +
            '<div ng-show="operation.msg" class="opMsg" ng-class="operation.success ? \'success-msg\' : \'error-msg\'">{{operation.msg}}</div>' +
            '<ul ng-show="grid.pager.pagerLabel" class="pagination">' +
            '<li><a ng-click="grid.pager.hasPrevPages && prevPage()" class="arrow" ng-class="{disabled: !grid.pager.hasPrevPages}">&laquo;</a></li>' +
            '<li class="desc" ng-bind="grid.pager.pagerLabel"></li>' +
            '<li><a ng-click="!grid.pager.hasNextPages || nextPage()" class="arrow" ng-class="{disabled: !grid.pager.hasNextPages}">&raquo;</a></li>' +
            '</ul>' +
            '</div>' +
            '<div class="clearfix"></div>' +
            '</div>'
    }
})
.directive('gridHeaderText', function() {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            text: '@gridHeaderText',
            Grid: '=gridObj'
        },
        template: '<div class="grid-header">' +
        '<h3>Showing {{Grid.data.records}} {{text}}</h3>' +
        '<div class="pull-right" ng-hide="Grid.data.total == 0">' +
            '<ul class="pagination">' +
                '<li><a ng-click="Grid.prevPage()" class="arrow" ng-class="{disabled: !Grid.hasPrevPages}">&laquo;</a></li>' +
                '<li class="desc">{{Grid.pagerText()}}</li>' +
                '<li><a ng-click="Grid.nextPage()" class="arrow" ng-class="{disabled: !Grid.hasNextPages}">&raquo;</a></li>' +
            '</ul>' +
        '</div>' +
        '<div class="clearfix"></div>' +
        '</div>'
    }
});