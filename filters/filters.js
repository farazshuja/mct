MCTApp.filter('unixToDateFormatter', function() {
    return function(v) {
        if(!v){
            return 'Never executed';
        }
        var date = new Date(v);
        var month = date.getMonth() + 1;
        month = month < 10 ? '0' + month : month;
        var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
        var hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
        var mins = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
        var secs = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
        return date.getFullYear() + '-' + month + '-' + day + ' ' + hours + ':' + mins + ':' + secs;
    };
})
.filter('searchInObject', function() {
    return function(input, key, value, returnIndex) {
        if(!input)
            return null;    //input undefined/null so return not found

        for (var i=0; i<input.length; i++) {
            if(key.indexOf('.') != -1) {
                var keyParts = key.split('.');
                if (input[i][keyParts[0]][keyParts[1]] == value) {
                    return returnIndex ? i : input[i];
                }
            }
            else if (input[i][key] == value) {
                return returnIndex ? i : input[i];
            }
        }
        return null;
    }
})
.filter('parseParam', function() {
    return function(input, key, value, returnIndex) {
        if(!input)
            return null;    //input undefined/null so return not found

        return encodeURIComponent(input);
    }
})
.filter('joinOnProperty', function() {
    return function(input, property, delimiter) {
        var output = '';
        if (input && property) {
            for (var i = 0; i < input.length; i++) {
                var inObj = input[i];
                try {
                    output += Object.resolve(property, inObj);
                } catch (e) {
                    return '';
                }

                if (i != input.length - 1) {
                    output += delimiter || ', ';
                }
            }
        }
        return output;
    }
});

