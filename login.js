// create angular app
var MCTApp = angular.module('mctLogin', []);

MCTApp.controller('loginController', function($scope) {
    $scope.submitResetPW = function(angObj) {
        if(angObj.$valid){
            showActivityInModal();
            $('.modal:visible .msg').remove();  //reset modal
            $.ajax({
                url: "resetPassword",
                type: "POST",
                data: $.param({'userEmail': $('#userEmail').val()}),
                success: function (data) {
                    hideActivityInModal();
                    if (data) {
                        $('#resetPW').append('<div class="msg success-msg">Email sent successfully, please check your email and follow the instructions to reset your password</div>');
                    } else {
                        $('#resetPW').append('<div class="msg error-msg">Email does not exists in our system, please enter a valid email!</div>');
                    }
                },
                error: function (data) {
                    hideActivityInModal();
                    $('#resetPW').append('<div class="msg error-msg">Server got some problem while sending the instruction to reset password to your email address. Please try later!</div>');
                }
            });
        }
    };

    $scope.submitClicked = false;
    $scope.submitForm = function($event, form){
        //in case of autofill
        $scope.j_username = $('#j_username').val();
        $scope.j_password = $('#j_password').val();

        //$scope.$apply();
        setTimeout(function(){
            if(form.$invalid){
                $event.preventDefault();
                $scope.submitClicked = true;
            }
        },400);

    }
});
$(document).ready(function() {
    if(!isBrowserSupported()) {
        $('.browser-un-supported').removeClass('hide');
    }
});


function showActivityInModal() {
    var $modalContent = $('.modal:visible .modal-content');
    $('.modal:visible button.close').hide();
    $modalContent.append('<div class="ajax-indicator-modal"></div>');
    $modalContent.append('<div class="ajax-indicator"></div>');
}
function hideActivityInModal() {
    var $modalContent = $('.modal:visible .modal-content');
    $('.modal:visible button.close').show();
    $modalContent.find('.ajax-indicator-modal, .ajax-indicator').remove();
}

//expire the session cookie so that user can be logged out from all other tabs
Cookies.expire('MCTSessionCookie');



