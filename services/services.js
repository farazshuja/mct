angular.module('mctAppServices', ['ngResource'])
    .factory('MCTAPI', ['$resource', function($resource) {
        return {
            IAStartSingleProcessing: $resource('jobs/IAStartSingleProcessing/grid'),
            IAExecutionControl: $resource('jobs/IAExecutionControl/grid'),
            CommonJobsSingleRun: $resource('jobs/commonJobsSingleRun/grid'),
            CommonJobsControlRun: $resource('jobs/commonJobsControlRun/grid'),
            LPSingleFeedGeneration: $resource('jobs/LPSingleFeedGeneration/grid'),
            LPFeedGenerationControl: $resource('jobs/LPFeedGenerationControl/grid'),
            SalesMatchingSetupGrid: $resource('scriptExecution/sales/grid'),
            SalesExecutionGrid: $resource('scriptExecution/sales/execution/grid'),
            GroupList: $resource('system/getGroupList'),
            GetGroup: $resource('system/getGroupList/:id'),
            GetUser: $resource('system/getUserList/:id'),
            GetUsersForGroup: $resource('system/groups/getUsersForGroup/:id'),
            GetScriptById: $resource('scriptExecution/sales/getScript/:id'),
            LeadCategories: $resource('metaData/dataSources/metaDataConfig/leadCategoryGrid'),
            GetLeadCategory: $resource('metaData/dataSources/metaDataConfig/getLeadCategory/:id'),
            LeadTypes: $resource('metaData/dataSources/metaDataConfig/leadTypeGrid'),
            GetLeadType: $resource('metaData/dataSources/metaDataConfig/getLeadType/:id'),
            OEMList: $resource('metaData/OEM/getOemList'),
            MissedCallRecords: $resource('devtool/callRecords/grid'),
            MapCallToLead: $resource('devtool/mapCallToLead/grid'),
            FTPDataProviders: $resource('devtool/remoteFileHistory/grid'),
            DataConsumers: $resource('metaData/dataConsumers/grid'),
            JobSchedulesGrid: $resource('metaData/processes/jobScheduling/grid'),
            DataConsumptionHistoryGrid: $resource('metaData/processes/dataConsumerHistory/grid'),
            NurturingGrid: $resource('metaData/nurturing/grid'),
            DataConsumptionHistoryDetailedGrid: $resource('metaData/processes/dataConsumerHistory/grid/:id', {id: '@id'}),
            CRMCodesGrid: $resource('metaData/crmCodes/grid'),
            MessageTypesGrid: $resource('bidirectional/messageTypes/grid'),
            IDCGrid: $resource('bidirectional/idc/grid'),
            RequestFormGrid: $resource('devtool/functionalTesting/requests/grid'),
            TestingResultGrid: $resource('devtool/functionalTesting/results/grid'),
            EnvironmentGrid: $resource('devtool/functionalTesting/environments/grid'),
            DevKitTrackGrid: $resource('devKit/trackingInfo')
        };
    }]);

