MCTApp.factory("MenuService", function() {
    var masterMenu = {
        crmMenu : [{
                "title": "CRM DevKit",
                "menu-mod": "mct-mod",
                "className": "system-icon",
                "href": "#/devKit/track"
        }],
        adminMenu: [
            {
                "title": "Home",
                "submenu": [
                    {
                        "title": "MCT",
                        "desc": "Lorem ipsum is simple the dummy text",
                        "className": "mct-mod"
                    },
                    {
                        "title": "Data Sources",
                        "desc": "Lorem ipsum is simple the dummy text",
                        "className": "ds-mod"
                    },
                    {
                        "title": "Data Exports",
                        "desc": "Lorem ipsum is simple the dummy text",
                        "className": "de-mod"
                    },
                    {
                        "title": "Processes",
                        "desc": "Lorem ipsum is simple the dummy text",
                        "className": "proc-mod"
                    }
                ]
            },
            {
                "title": "ACL",
                "menu-mod": "mct-mod",
                "className": "system-icon",
                "submenu": [
                    {
                        "title": "Users",
                        "href": "#/users/list"
                    },
                    {
                        "title": "Groups",
                        "href": "#/groups/list"
                    }
                ]
            },
            {
                "title": "OEMs",
                "menu-mod": "mct-mod",
                "className": "oem-icon",
                "submenu": [
                    {
                        "title": "Setup OEMs",
                        "href": "#/metaData/OEM/list"
                    }
                ]
            },
            {
                "title": "Meta Data",
                "menu-mod": "ds-mod",
                "className": "meta-icon",
                "href": "#/metaData/dataSources/metaDataConfig"
            },
            {
                "title": "Data Providers",
                "menu-mod": "ds-mod",
                "className": "cloud-icon",
                "href": "#/metaData/dataProviders/list"
            },
            {
                "title": "Data Consumers",
                "menu-mod": "ds-mod",
                "className": "cons-icon",
                "href": "#/metaData/dataConsumers/list"
            },
            {
                "title": "Nurturing Programs",
                "menu-mod": "ds-mod",
                "className": "nurtur-icon",
                "href": "#/metaData/nurturing/list"
            },
            {
                "title": "CRM Codes",
                "menu-mod": "ds-mod",
                "className": "crm-icon",
                "href": "#/metaData/crmCodes/list"
            },
            {
                "title": "Bi-directional",
                "menu-mod": "ds-mod",
                "className": "bi-icon",
                "submenu": [
                    {
                        "title": "Message Types",
                        "href": "#/bidirectional/messageTypes/list"
                    },
                    {
                        "title": "Incoming Data Connectors",
                        "href": "#/bidirectional/idc/list"
                    },
                    {
                        "title": "Data Senders",
                        "href": "#/bidirectional/dataSender/list"
                    },
                    {
                        "title": "CRMs",
                        "href": "#/bidirectional/crmDsMapping/list"
                    },
                    {
                        "title": "Outgoing Data Connectors",
                        "href": "#/bidirectional/odc/list"
                    }
                ]
            },
            {
                "title": "Data Consumption History",
                "menu-mod": "proc-mod",
                "className": "history-icon",
                "href": "#/metaData/processes/dataConsumerHistory/list"
            },
            {
                "title": "Stored Scripts",
                "menu-mod": "proc-mod",
                "className": "script-icon",
                "href": "#/"
            },
            {
                "title": "Job Scheduling",
                "menu-mod": "proc-mod",
                "className": "cal-icon",
                "href": "#metaData/processes/jobScheduling/list"
            },
            {
                "title": "Status & Execution Control",
                "menu-mod": "proc-mod",
                "className": "status-icon",
                "submenu": [
                    {
                        "title": "Sales Matching",
                        "submenu": [
                            {
                                "title": "Sales Matching Setup",
                                "href": "#/scriptExecution/sales"
                            },
                            {
                                "title": "Execute Process",
                                "href": "#/scriptExecution/sales/execute"
                            }
                        ]
                    },
                    {
                        "title": "ETL",
                        "submenu": [
                            {
                                "title": "ALEWS Single Run",
                                "href": "#/jobs/alews"
                            },
                            {
                                "title": "IA Start Single Processing",
                                "href": "#/jobs/IAStartSingleProcessing"
                            },
                            {
                                "title": "IA Execution Control",
                                "href": "#/jobs/IAExecutionControl"
                            },
                            {
                                "title": "Common Jobs Single Run",
                                "href": "#/jobs/commonJobsSingleRun"
                            },
                            {
                                "title": "Common Execution Control",
                                "href": "#/jobs/commonJobsControlRun"
                            },
                            {
                                "title": "LP Feed Generation Single Run",
                                "href": "#/jobs/LPSingleFeedGeneration"
                            },
                            {
                                "title": "LP Execution Control",
                                "href": "#/jobs/LPFeedGenerationControl"
                            }
                        ]
                    }
                ]
            }
        ]
    };

    var isCrmUser = $('#isCrmUser').val();
    var menu = isCrmUser == 'true' ? masterMenu.crmMenu : masterMenu.adminMenu;
    var homeMenu = menu[0].submenu;
    return {
        activeMod: 'mct-mod',
        activeTab: '',
        menu: menu,
        homeMenu: homeMenu,
        setActiveMenuMod: function(cname, tab) {
            if(cname != '') {
                this.activeMod = cname;
            }
            this.activeTab = tab;
        }
    };
});